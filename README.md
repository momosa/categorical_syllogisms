# Categorical Syllogisms

<categorical.syllogisms@gmail.com>

- - -

## Contents

1. [Version changes](#version_changes)
1. [Logician's overview](#logic_overview)

    2.1. [Proposition generation](#proposition_generation)

    2.2. [Pretty form versus real natural English](#pretty_form_limits)

    2.3. [Syllogism generation](#syllogism_generation)

1. [Server and API](#server)

    3.1. [Running the server](#running_server)

    3.2. [API documentation](#api_documentation)

1. [Development](#development)

    4.1. [Dev environment setup](#dev_environment_setup)

    4.2. [Docker images and containers](#docker)

    4.2. [Container logs](#docker_logs)

1. [Custom generation](#custom_generation)

    5.1. [Modifying terms](#modifying_terms)

- - -

<a name="version_changes"></a>

## 1\. Version changes

See the `CHANGELOG.md`

- - -

<a name="logic_overview"></a>

## 2\. Overview

This is an open source project
intended to support the educational community
by providing resources for students and teachers of introductory logic.
Currently, we're wrapping up an interface (API)
for categorical logic.
The interface can communicate with other educational resources,
like Moodle or a website.
Our future plans include building a website
where students can practice logic problems
and instructors can create printable worksheets.

See what we've done so far:

- [Generate a random categorical syllogism](http://logicforall.org/api/categorical/syllogism/natural/).
There's a lot of other functionality.
See the API documentation for details.
- [Documentation for the API](http://logicforall.org/documentation/api/)
Documentation on our machine-friendly interface for generating
categorical syllogisms, propositions, exercises, and more.

What users can do through the API:

1. Generate random propositions, syllogisms, and exercises.
1. Get propositions, and syllogisms in canonical form.
1. Get propositions, and syllogisms in natural English.
1. Assess the validity and soundness of a categorical syllogism.

Additional functionality in the backend:

1. Create custom topics.
1. Customize the way sentences are combined using
conjunctions and premise/conclusion indicators.

To see our next steps, check out our
[milestones on gitlab](https://gitlab.com/momosa/categorical_syllogisms/milestones).

-------------------------------------------------------------------------------

<a name="proposition_generation"></a>

### 2.1\. Sentence generation

Categorical propositions can yield sentences of
canonical form or in natural English.
Natural English sentence have more variation
based on the following characteristics.

- Variable quantifiers
  - "All" may be implicit
  - Terms like "only" can switch the order of the subject and predicate
  - _Et cetera_

We've added truth-value semantics,
making it possible to get, for instance,
the contradictory or a true proposition in natural English.

-------------------------------------------------------------------------------

<a name="pretty_form_limits"></a>

### 2.2\. "Natural English" form versus real natural English

The natural English form has its limitations.
The primary limitation of the current system
is the lack of adjectives and non-copula verbs.
The introduction of truth-value semantics made it infeasible
to continue supporting adjectives and non-copula verbs.
The reason is that those extra terms effectively add categories
while enabling combinations of categories.
Consider "Happy pandas love ferocious kittens".
There are four predicates and a binary relation in that sentence.
We could form new sentences expressing different propositions
by combining those predicates and relation in all sorts of ways.
This is how adjectives and non-copula verbs lead to a
combinatorial explosion of semantic complexity.
To keep the system scalable with a straightforward, topic-based semantics,
we moved to simple categories.

-------------------------------------------------------------------------------

<a name="syllogism_generation"></a>

### 2.2\. Syllogism generation

The program can produce random categorical syllogisms in canonical and
natural English.
The program can also create syllogisms that are valid/invalid
or sound/unsound upon request.

The natural English form of a syllogism is composed of natural English propositions.
Additionally, the premise/conclusion indicator is randomized instead of just "therefore".
Finally, shorter sentences may be combined using a conjunction or premise/conclusion indicator.

- - -

<a name="server"></a>

## 3\. Server and API

<a name="running_server"></a>

### 3.1\. Running the server

Prerequisites:

- [Docker](https://www.docker.com/).
- Run on a \*nix host

The following is about the production server.

**NOTE**:
The script `bin/deploy.sh` was designed to
make it easy to deploy to production
on an AWS EC2 instance running ubuntu.
If the `deploy` script gives you trouble on your environment,
use the local deployment technique.

1. Remote host deployment:
`bin/deploy.sh [host_name]` (does not require the repo on the remote host).
1. Local deployment from latest image:
`bin/deploy.sh local`
1. Local deployment from local branch:
You'll need to build artifacts to get the API spec,
if you want it.
See below.

If you want to run the server from a local branch with API documentation,
do the following.
(You can skip to the build step to
run the server without API documentation.)
I recommend making a backup copy of
`logic_server/api_docs/templates/api_docs/index.html`
and replace it after building the `prod` image.

```shell
# Run from the project base directory
bin/build.sh apispec
bin/run.sh apispec_generate
mv ~/.categorical_syllogisms/apispec/index.html logic_server/api_docs/templates/api_docs/index.html
```

Build the production image:

```shell
bin/build.sh prod
```

Now you can run the production server locally
using your local branch:

```shell
bin/run.sh prod_server [port]
```

Without `port`, the production server will attempt to use port 80 for http.
Ensure that port is free before starting the server
or specify another port.

You should be able to see the server containers running

```shell
docker ps
```

There should be two containers,
`backend.logicforall.org` and `nginx`.
Nginx is a reverse proxy server that handles
things like load balancing and rate throttling.
The backend server serves the API and pages.

You can check out your local deployment through your browser at `localhost` or
if you specified a custom port, add `:your_port` after `localhost`.

You can stop the server as follows, replacing `prod_server`
with `dev_server` if necessary:

```shell
docker stop nginx
docker stop backend.logicforall.org
```

-------------------------------------------------------------------------------

<a name="api_documentation"></a>

### 3.2\. API documentation

The API documentation is accessible at
[http://logicforall.org/documentation/api/](http://logicforall.org/documentation/api/)

-------------------------------------------------------------------------------

<a name="development"></a>

## 4\. Development

<a name="dev_environment_setup"></a>

### 4.1\. Dev environment setup

You'll want to set up a virtual environment for dev work.
Here's how to do that using `pyenv`.
Before proceeding, install `pyenv` using the
[installation instructions](https://github.com/pyenv/pyenv#installation).

```shell
# From the project dir
pyenv install 3.7.4
pyenv virtualenv 3.7.4 categorical-syllogisms
pyenv activate categorical-syllogisms
```

Use `requirements-dev.txt` to set up your requirements.

```shell
pip install -r requirements-dev.txt
```

Set up client-side development environment:

```shell
# From the ui/ dir
npm install
```

-------------------------------------------------------------------------------

<a name="docker"></a>

### 4.2\. Docker images and containers

It's easiest to interact with images/containers using the scripts in `bin/`.
You will often have to build images and run containers.
There is a convenient script for each task.
Use each script's `-h` option for more info.

##### Docker image builder

```shell
bin/build.sh -h
```

##### Docker container runner

```shell
bin/run.sh -h
```

#### Building the dev image

```shell
bin/build.sh dev -h
```

#### Running tests and containers for development

Many of these tests are built into the CI,
but during development it's faster to
run relevant tests yourself
instead of waiting for the CI to run _everything_.

##### Docker tests

```shell
bin/build.sh all
bin/test.sh docker
```

Docker tests ensure that each container's requirements are satisfied.
Docker tests help avoid breakage caused by sneaky version changes.
They also make it a lot easier to debug while working on dockerfiles.

##### Unit tests (with coverage analysis)

In a development environment:

```shell
bin/test.sh unit
```

Ouside of a development environment:

```shell
bin/build.sh dev
bin/run.sh unit_tests
```

##### Lint tests

In a development environment:

```shell
bin/test.sh lint
```

Ouside of a development environment:

```shell
bin/build.sh dev
bin/run.sh lint_tests
```

##### Javascript unit tests

In a development environment:

```shell
bin/test.sh ui_unit
```

Ouside of a development environment:

```shell
bin/build.sh dev
bin/run.sh ui_unit_tests
```

##### Javascript lint tests

In a development environment:

```shell
bin/test.sh ui_lint
```

Ouside of a development environment:

```shell
bin/build.sh dev
bin/run.sh ui_lint_tests
```

<a name="topic_validation"></a>

##### Topic validation

In a development environment:

```shell
bin/test.sh topic
```

Ouside of a development environment:

```shell
bin/build.sh dev
bin/run.sh topic_tests
```

##### API tests

API tests can be run using containers or in a local development environment.
Containerized tests use a production server behind `nginx`.
This method can be slow because you need to rebuild images
every time you make a change.
So while you're making a lot of changes,
you can run the tests against a server running in a local development environment.
The CI system uses containerized tests to better ensure that the server is production-ready.

###### API tests in containers

API tests use three containers,
a `test.server` container serving the website
(it's just an nginx container),
a `backend.logicforall.org` container running gunicorn/django,
and an `api_tests` container that runs tests.
The three containers interact on a docker network.
You can run them all together as follows:

```shell
bin/build.sh prod dev
bin/run.sh api_tests
```

Note that API tests require up-to-date production and development images.
That's because the backend container instantiates the production image
while the tests are run from a development container.

##### API tests in a local development environment

It's also possible to run API tests within a local development environment.
To do so,
start the server locally at port 8888 and
run the tests with the environment variable `CS_LOCAL=true`.

```shell
# Start the server in a screen, another terminal, or the background
DJANGO_SETTINGS_MODULE=logic_server.settings_dev python logic_server/manage.py runserver 8888

# Run the tests with CS_LOCAL=true
CS_LOCAL=true pytest -v tests/api/
```

##### API spec validation

```shell
bin/build.sh apispec
bin/run.sh apispec_validate
```

##### API documentation

The openapi spec is in `apispec/`.

Some helpful commands:

```shell
# Build the apispec image
bin/build.sh apispec

# Validate the apispec (requires image)
bin/run.sh apispec_validate

# Generate the apispec (requires image)
bin/run.sh apispec_generate
```

The output will be placed in `$HOME/.categorical_syllogisms/apispec/`

-------------------------------------------------------------------------------

<a name="docker_logs"></a>

### Container logs

By default,
container logs can be found in `$HOME/.categorical_syllogisms`
on the host machine.
`run.sh` has a options to specify a different log dir.

- - -

<a name="custom_generation"></a>

## 5\. Custom Generation

<a name="modifying_terms"></a>

### 5.1\. Modifying topics/terms

A list of categories is called a *topic*.
Topics determine what a proposition, or syllogism is about.

Topics can be customized.
We may eventually build this into the API,
but for now topic customization is done on the server side.
Topics can be found in:

```shell
logic/databases/category_topics/
```

Using `fruits_and_vegetables.json` as a model,
create a new `your_topic.json`.
To help ensure that propositions and syllogisms
are sane, limit the content of the topic so that any possible
category both makes sense and can relate sensibly to other categories.
See the topic creation gitlab issue template for a
full set of guidelines.
Don't forget to ensure the json is free of syntax errors.
Rebuild the dev image and restart the server (see below).
Now, pass the topic into API calls:

```shell
curl --request GET "$HOST/categorical/syllogism/natural" \
    --data '{"topic_name": "your_topic"}'
```

The automated topic validation performs some basic checks like
ensuring that each topic has the correct number of terms.
See [Topic validation](#topic_validation) for how to run these checks locally.

-------------------------------------------------------------------------------

- - -

