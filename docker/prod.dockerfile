FROM python:3.7

ENV DEBIAN_FRONTEND noninteractive

RUN pip install --upgrade pip

# Copy the project
RUN mkdir -p /categorical_syllogisms
COPY $CATEGORICAL_SYLLOGISMS /categorical_syllogisms

# Install external python libs
RUN cd /categorical_syllogisms && \
  pip install --requirement /categorical_syllogisms/requirements-prod.txt

# Set env vars
ENV DJANGO_SETTINGS_MODULE logic_server.settings
