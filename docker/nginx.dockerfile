FROM nginx:stable-alpine

COPY $CATEGORICAL_SYLLOGISMS/misc/nginx.conf /etc/nginx
COPY $CATEGORICAL_SYLLOGISMS/ui/dist /www/data/assets
COPY $CATEGORICAL_SYLLOGISMS/error_pages /www/data/error_pages
