FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive

# Dependencies for swagger tools
RUN apt-get update \
  &&  apt-get install -y apt-utils \
  && apt-get install -y openjdk-8-jre-headless \
  && apt-get install -y wget \
  && apt-get install -y npm
RUN npm install --global swagger-merger
RUN mkdir -p /swagger-codegen \
  && wget https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar \
    --no-check-certificate \
    --output-document /swagger-codegen/swagger-codegen-cli.jar

# Copy the project
RUN mkdir -p /categorical_syllogisms
COPY $CATEGORICAL_SYLLOGISMS /categorical_syllogisms

# Merge the swagger yamls
RUN mkdir -p /swagger-codegen/in \
  && mkdir -p /swagger-codegen/out
RUN swagger-merger \
  --input /categorical_syllogisms/apispec/index.yaml \
  --output /swagger-codegen/in/index.yaml
