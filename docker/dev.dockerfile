FROM python:3.7

ENV DEBIAN_FRONTEND noninteractive

# Required for UI unit tests
RUN apt-get update && \
  apt-get install -y npm

RUN pip install --upgrade pip

# Copy the project
RUN mkdir -p /categorical_syllogisms
COPY $CATEGORICAL_SYLLOGISMS /categorical_syllogisms

# Install external python libs
RUN cd /categorical_syllogisms && \
  pip install --requirement /categorical_syllogisms/requirements-dev.txt

# Set up UI stuff
RUN cd /categorical_syllogisms/ui && npm install
RUN cd /categorical_syllogisms/ui && npm run build

# Set env vars
ENV DJANGO_SETTINGS_MODULE logic_server.settings_dev
