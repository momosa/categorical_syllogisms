# Misc files for categorical syllogisms

## Nginx config

`nginx.conf` is the config file mounted into
nginx containers.
It is designed use the docker network to
communicate with the backend django server.

For tests, the nginx container can have a special name.
The config file must also be set up to handle such requests.
The test server name must be a valid `server_name`
like `test.server` (no underscores).
Test runner containers can then communicate
with the nginx container unambiguously,
without accidentally hitting the remote real prod server.

## Logrotation setup

For host setup on the prod server.

Files (make sure not to overwrite anything):

- `logrotate`: put in `/etc/cron.hourly/`
- `logrotate.hourly.conf`: put it in `/etc/`
