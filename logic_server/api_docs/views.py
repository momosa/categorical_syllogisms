# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Create your views here.


def api_docs(request):
    api_docs_template = loader.get_template('api_docs/index.html')
    context = {}
    return HttpResponse(api_docs_template.render(context, request))
