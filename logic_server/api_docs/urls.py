from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^$',
        views.api_docs,
        name='api_docs',
    ),
]
