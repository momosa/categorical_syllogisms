"""
topic_views.py

TopicListView:
    Provides the views for /api/topic/list

TopicDetailViwe
    Provides the views for /api/topic/<topic_name>
"""


from django.views import View
from django.http import JsonResponse

from logic.categorical.topic import Topic


class TopicListView(View):
    """ View for the topic list endpoint """

    def get(self, request):
        """
        GET /api/categorical/topic/list
        """
        topics = {'topic_list': Topic.get_available()}
        return JsonResponse(topics, content_type='application/json')


class TopicDetailView(View):
    """ View for the topic detail endpoint """

    def get(self, request, topic_name):
        """
        GET /api/categorical/topic/<topic_name>
        """
        topic = Topic(topic_name=topic_name)
        topic_dict = topic.as_dict()
        return JsonResponse(topic_dict, content_type='application/json')
