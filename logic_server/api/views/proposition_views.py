"""
proposition_views.py

PropView:
    Provides the views for the /api/proposition/ endpoint

PropCanonicalView
    Provides the views for the /api/proposition/canonical/ endpoint

PropNaturalView
    Provides the views for the /api/proposition/natural/ endpoint

PropContradictoryView
    Provides the views for the /api/proposition/contradictory/ endpoint
"""


from dataclasses import asdict

from api.views.base_view import BaseView

from logic.categorical.proposition import CategoricalProposition
from logic.categorical.english.proposition_to_str import (
    canonical_categorical_proposition,
    natural_categorical_proposition,
)
from logic.categorical.util.proposition_transformer import contradictory


class PropView(BaseView):
    """ Base for proposition endpoint views """

    def get_obj_type(self):
        """
        Defines CategoricalProposition as the relevant object type
        """
        return CategoricalProposition

    def as_dict(self, obj):
        """
        Gets a `CategoricalProposition` as a dictionary
        """
        return asdict(obj)


class PropCanonicalView(PropView):
    """ Canonical proposition endpoint view """

    def as_dict(self, obj):
        """
        Defines how to get the canonical dictionary
        """
        return {'canonical_sentence': canonical_categorical_proposition(obj)}


class PropNaturalView(PropView):
    """ Natural English proposition endpoint view """

    def as_dict(self, obj):
        """
        Defines how to get the natual dictionary
        """
        return {'natural_sentence': natural_categorical_proposition(obj)}


class PropContradictoryView(PropView):
    """ Contradictory proposition endpoint view """

    @staticmethod
    def get_object(obj_type, request_body=None):
        proposition = PropView.get_object(obj_type, request_body)
        return contradictory(proposition)
