"""
syllogism_views.py

SylView:
    Provides the views for the /api/syllogism/ endpoint

SylCanonicalView
    Provides the views for the /api/syllogism/canonical/ endpoint

SylNaturalView
    Provides the views for the /api/syllogism/natural/ endpoint
"""


from dataclasses import asdict

from api.views.base_view import BaseView

from logic.categorical.syllogism import CategoricalSyllogism
from logic.categorical.english.syllogism_to_str import (
    canonical_categorical_syllogism,
    natural_categorical_syllogism,
)


class SylView(BaseView):
    """
    Base view for syllogism endpoints
    """

    def get_obj_type(self):
        """
        Defines CategoricalSyllogism as the relevant object type
        """
        return CategoricalSyllogism


    def as_dict(self, obj):
        """
        Gets a `CategoricalSyllogism` as a dictionary
        """
        return asdict(obj)


class SylCanonicalView(SylView):
    """ Canonical syllogism endpoint view """

    def as_dict(self, obj):
        """
        Defines how to get the canonical dictionary
        """
        return {'canonical_syllogism': canonical_categorical_syllogism(obj)}


class SylNaturalView(SylView):
    """ Natural English syllogism endpoint view """

    def as_dict(self, obj):
        """
        Defines how to get the natual dictionary
        """
        return {'natural_syllogism': natural_categorical_syllogism(obj)}
