"""
base_view.py

BaseView provides basic API functionality for the categorical logic endpoints.

In most cases, you should only have to implement `get_obj_type`.
If the type doesn't have a `as_dict` method,
you should implement the `as_dict` static method
to create the dict using the appropriate mechanism.
"""

import json

from abc import ABCMeta, abstractmethod

from django.views import View
from django.http import JsonResponse


class BaseView(View):
    """ Base class for API views """


    __metaclass__ = ABCMeta


    @abstractmethod
    def get_obj_type(self):
        """
        Get the object type that the view is about

        Sample implementation: return Category
        """


    def as_dict(self, obj):
        """
        Gets a dictionary of the object

        Override if there's no as_dict method

        :param obj: The object to get a dictionary of
        :return: a dictionary representing the object
        :rtype: dict
        """
        return obj.as_dict()


    def get(self, request):
        """
        GET a json of a random object of the designated type
        """
        obj = self.get_object(self.get_obj_type())
        return JsonResponse(
            self.as_dict(obj),
            content_type='application/json')


    def post(self, request):
        """
        GET a json of a specified object of the designated type
        """
        obj = self.get_object(self.get_obj_type(), request.body)
        return JsonResponse(
            self.as_dict(obj),
            content_type='application/json')


    @staticmethod
    def get_object(obj_type, request_body=None):
        """
        Get an object from the request body

        :param class obj_type: the class to get
        :param str request_body: the query request body (json)
        :return: object of the requested type
        """
        if request_body:
            body_unicode = request_body.decode('utf-8')
            spec = json.loads(body_unicode)
        else:
            spec = {}
        obj = obj_type(**spec)
        return obj
