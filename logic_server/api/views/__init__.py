# -*- coding: utf-8 -*-

from api.views.proposition_views import (
    PropView,
    PropCanonicalView,
    PropNaturalView,
    PropContradictoryView,
)

from api.views.syllogism_views import (
    SylView,
    SylCanonicalView,
    SylNaturalView,
)

from api.views.exercise_views import (
    ExerPropTypeView,
    ExerContradictoryView,
    ExerSyllogismSoundness,
)

from api.views.topic_views import (
    TopicListView,
    TopicDetailView,
)


__all__ = [

    # /api/categorical/proposition/...
    'PropView',
    'PropCanonicalView',
    'PropNaturalView',
    'PropContradictoryView',

    # /api/categorical/syllogism/...
    'SylView',
    'SylCanonicalView',
    'SylNaturalView',

    # /api/categorical/exercise/...
    'ExerPropTypeView',
    'ExerContradictoryView',
    'ExerSyllogismSoundness',

    # /api/topic/...
    'TopicListView',
    'TopicDetailView',

]
