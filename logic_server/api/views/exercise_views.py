"""
exercise_views.py

ExerPropTypeView:
    Provides the views for the /api/categorical/exercise/proposition/type endpoint

ExerContradictoryView
    Provides the views for /api/categorical/exercise/proposition/contradictory
"""


from dataclasses import asdict

from api.views.base_view import BaseView

from logic.categorical import exercise


class ExerciseView(BaseView):
    """
    View for exercises
    """

    def as_dict(self, obj):
        """
        Gets an exercise as a dict
        """
        return asdict(obj)


class ExerPropTypeView(ExerciseView):
    """
    View for the proposition type exercise endpoint
    """

    def get_obj_type(self):
        """
        Defines the relevant object type
        """
        return exercise.PropositionType


class ExerContradictoryView(ExerciseView):
    """
    View for the contradictory proposition exercise endpoint
    """

    def get_obj_type(self):
        """
        Defines the relevant object type
        """
        return exercise.PropositionContradictory


class ExerSyllogismSoundness(ExerciseView):
    """
    View for the syllogism_soundness exercise endpoint
    """

    def get_obj_type(self):
        """
        Defines the relevant object type
        """
        return exercise.SyllogismSoundness
