""" URLs for the API """

from django.conf.urls import url

from . import views

urlpatterns = [

    # Propositions
    url(
        r'^proposition/$',
        views.PropView.as_view(),
        name='proposition',
    ),
    url(
        r'^proposition/canonical/$',
        views.PropCanonicalView.as_view(),
        name='proposition_canonical',
    ),
    url(
        r'^proposition/natural/$',
        views.PropNaturalView.as_view(),
        name='proposition_natural',
    ),
    url(
        r'^proposition/contradictory/$',
        views.PropContradictoryView.as_view(),
        name='proposition_contradictory',
    ),

    # Syllogisms
    url(
        r'^syllogism/$',
        views.SylView.as_view(),
        name='syllogism',
    ),
    url(
        r'^syllogism/canonical/$',
        views.SylCanonicalView.as_view(),
        name='syllogism_canonical',
    ),
    url(
        r'^syllogism/natural/$',
        views.SylNaturalView.as_view(),
        name='syllogism_natural',
    ),

    # Exercises
    url(
        r'^exercise/proposition/type/$',
        views.ExerPropTypeView.as_view(),
        name='exercise_prop_type',
    ),
    url(
        r'^exercise/proposition/contradictory/$',
        views.ExerContradictoryView.as_view(),
        name='exercise_prop_contradictory',
    ),
    url(
        r'^exercise/syllogism/soundness/$',
        views.ExerSyllogismSoundness.as_view(),
        name='exercise_soundness',
    ),

    # Topics
    url(
        r'^topic/list/$',
        views.TopicListView.as_view(),
        name='topic_list',
    ),
    url(
        r'^topic/(?P<topic_name>[\w/]+)/$',
        views.TopicDetailView.as_view(),
        name='topic_detail',
    ),
]
