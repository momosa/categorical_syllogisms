# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.http import Http404
from django.template.response import TemplateResponse
from django.template import loader
from django.core.exceptions import PermissionDenied


def home(request):
    return TemplateResponse(
        request,
        template='home/home.html',
        context={},
    )


# In production, these are served by nginx
def local_assets(request, asset_name=None):
    if settings.DEBUG is not True:
        raise PermissionDenied(
            "Access to local assets is restricted to DEBUG mode"
        )
