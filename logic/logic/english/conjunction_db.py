"""
conjunction_db.py

ConjunctionDB
"""

import random

from logic.util import Database


DB_NAME = 'conjunctions'


class ConjunctionDB:
    """
    A class for interacting with logical conjunctions: 'and' and friends

    Note: We may want to generalize this to a connective DB.

    :Example:

        >>> from conjunction_db import ConjunctionDB
        >>> and_db = ConjunctionDB
        >>> and_db.get_middle_conjunction()
        'yet'
        >>> and_db.get_start_conjunction()
        'additionally,'
    """


    def __init__(self):
        self.database = Database(DB_NAME, read_only=True)


    def get_middle_conjunction(self):
        """
        Gets a conjunction for the middle of a sentence.
        E.g., 'and', 'but'

        :return: String representing the conjunction
        :rtype: str
        """
        print(self.database)
        print(self.database['mid_sentence_conjunction'])
        mid_sentence_conjunctions = self.database['mid_sentence_conjunction']
        return random.choice(mid_sentence_conjunctions)


    def get_start_conjunction(self):
        """
        Gets a conjunction for the beginning of a sentence.
        E.g., 'moreover,', 'however,'

        :return: String representing the conjunction
        :rtype: str
        """
        start_conjunctions = self.database['sentence_start_conjunction']
        return random.choice(start_conjunctions)
