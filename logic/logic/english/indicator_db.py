"""
indicator_db.py

IndicatorDB
"""

import random

from logic.util.database import Database


DB_NAME = 'indicators'


class IndicatorDB:
    """
    A class for interacting with premise and conclusion indicators

    :Example:

        >>> from indicator_db import IndicatorDB
        >>> indi_db = IndicatorDB
        >>> indi_db.get_premise_indicator()
        'since'
        >>> indi_db.get_conclusion_indicator()
        'thus'
    """


    def __init__(self):
        self.database = Database(DB_NAME, read_only=True)


    def get_premise_indicator(self):
        """
        Gets a random premise indicator, like 'because'

        :return: String representing a random premise indicator
        :rtype: str
        """
        premise_indicators = self.database['premise']
        return random.choice(premise_indicators)


    def get_conclusion_indicator(self):
        """
        Gets a random conclusion indicator, like 'therefore'

        :return: String representing a random conclusion indicator
        :rtype: str
        """
        conclusion_indicators = self.database['conclusion']
        return random.choice(conclusion_indicators)
