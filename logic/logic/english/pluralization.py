"""
pluralization.py

Tools for pluralization (singlular, plural)

:Example:

    >>> from pluralization import get_plural_noun
    >>> print get_plural_noun('ant')
    ants
    >>> print get_plural_noun('bush')
    bushes
    >>> print get_plural_noun('monkey')
    monkies
    >>> print get_plural_noun('deer') # Won't work for special cases
    deers
"""

import re


def get_plural_noun(singular_noun):
    """
    Get the plural form noun from the singular form

    Does not work for everything, like 'deer'

    :param string singular_noun: A singular noun
    :return: Plural form of the noun
    :rtype: str
    """

    # E.g., fix, watch
    if re.match(r'.*(ss|x|zz|ch|sh)$', singular_noun):
        plural_noun = singular_noun + 'es'

    # E.g., bus, showbiz
    elif re.match(r'.*[aeiou][sz]$', singular_noun):
        plural_noun = singular_noun + singular_noun[-1] + 'es'

    # E.g., body
    elif re.match(r'.*[^aeiou]y$', singular_noun):
        plural_noun = singular_noun[:-1] + 'ies'

    # E.g., human, face, monkey
    else:
        plural_noun = singular_noun + 's'

    return plural_noun
