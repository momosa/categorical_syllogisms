"""
logic.english

Tools for handling the English representation of logical items
"""
from logic.english.pluralization import get_plural_noun
from logic.english.conjunction_db import ConjunctionDB
from logic.english.indicator_db import IndicatorDB
from logic.english.sentence import Sentence
from logic.english.syllogism import (
    canonical_syllogism,
    natural_syllogism,
)
