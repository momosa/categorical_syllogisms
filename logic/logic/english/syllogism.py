"""
syllogism.py

Syllogism
"""

import random

from logic.util import randbool
from logic.english.sentence import Sentence


def canonical_syllogism(first_premise, second_premise, conclusion):
    """
    Get a string representing the canonical version of the argument.

    :param str first_premise: an unpunctuated string representing the first premise
    :param str second_premise: an unpunctuated string representing the second premise
    :param str conclusion: an unpunctuated string representing the conclusion
    :return: <prem1>. <prem2>. Therefore, <conc>.
    :rtype: str
    """
    return (
        f'{Sentence.punctuate(first_premise)} '
        f'{Sentence.punctuate(second_premise)} '
        f'{Sentence.as_canonical_conclusion(conclusion)}'
    )


def natural_syllogism(first_premise, second_premise, conclusion, random_seed=None):
    """
    Get a plain English version of the argument.

    :param str first_premise: an unpunctuated string representing the first premise
    :param str second_premise: an unpunctuated string representing the second premise
    :param str conclusion: an unpunctuated string representing the conclusion
    :param int random_seed: A random seed used to ensure repeatable results.
    :return: Some plain English rendition.
    :rtype: str
    """
    if random_seed is not None:
        random.seed(random_seed)

    if randbool():
        first_premise, second_premise = second_premise, first_premise

    arg_structure = random.choice([
        'P1. P2. So C.',
        'P1 & P2. So C.',
        'C bc (P1 & P2)',
        '(C bc P1) & P2',
        'P1. So C bc P2',
    ])

    if arg_structure == 'P1. P2. So C.':
        natural = (
            f'{Sentence.punctuate(first_premise)} '
            f'{Sentence.punctuate(second_premise)} '
            f'{Sentence.as_natural_conclusion(conclusion)}'
        )

    elif arg_structure == 'P1 & P2. So C.':
        conjoined_prems = Sentence.conjoin(first_premise, second_premise)
        natural = (
            f'{Sentence.punctuate(conjoined_prems)} '
            f'{Sentence.as_natural_conclusion(conclusion)}'
        )

    elif arg_structure == 'C bc (P1 & P2)':
        conjoined_prems = Sentence.conjoin(first_premise, second_premise)
        natural = Sentence.with_middle_prem_indicator(conclusion, conjoined_prems)
        natural = Sentence.punctuate(natural)

    elif arg_structure == '(C bc P1) & P2':
        c_bc_p1 = Sentence.with_middle_prem_indicator(conclusion, first_premise)
        natural = Sentence.conjoin(c_bc_p1, second_premise)
        natural = Sentence.punctuate(natural)

    elif arg_structure == 'P1. So C bc P2':
        c_bc_p2 = Sentence.with_middle_prem_indicator(conclusion, second_premise)
        c_bc_p2 = Sentence.as_natural_conclusion(c_bc_p2)
        natural = (
            f'{Sentence.punctuate(first_premise)} '
            f'{c_bc_p2}'
        )

    return natural
