"""
sentence.py

Sentence
"""

import random

from logic.english.conjunction_db import ConjunctionDB
from logic.english.indicator_db import IndicatorDB


# Try to break up combinations beyond this length.
UNSAVORY_LENGTH = 80

CONCLUSION_STANDINS = [
    "That's so",
    "That's the case",
    "That's true",
]

class Sentence:
    """
    A class for transforming declarative sentences

    :Example:

        >>> from sentence import Sentence
        >>> Sentence.punctuate("whales are mammals")
        Whales are mammals.
        >>> Sentence.conjoin("whales are mammals", "fish are not")
        whales are mammals and fish are not
        >>> Sentence.as_canonical_conclusion("whales are mammals")
        Therefore, whales are mammals.
        >>> Sentence.as_natural_conclusion("whales are mammals")
        So whales are mammals.
        >>> Sentence.with_middle_prem_indicator("whales are mammals", "my teacher said so")
        whales are mammals because my teacher said so
    """


    _conjunction_db = None
    _indicator_db = None


    def __init__(self):
        raise NotImplementedError("Sentence should not be instantiated")


    @classmethod
    def _initialize(cls):
        """
        This should be run before trying to use the conjunction or indicator databases.
        Not initializing them when the class loads ensures we don't load the databases
        unless they're actually used
        and to keep the unit tests clean.
        """
        cls._conjunction_db = cls._conjunction_db or ConjunctionDB()
        cls._indicator_db = cls._indicator_db or IndicatorDB()


    @staticmethod
    def punctuate(unpunctuated_str):
        """
        Punctuates a sentence by capitalizing the first letter and adding a period.

        Note: str.capitalize() will convert characters after the first to lower-case.
        We only want to capitalize the first letter.

        :param str unpunctuated_str: a string representing an unpunctuated sentence
        :return: a punctuated version of the string
        :rtype: str
        """
        if len(unpunctuated_str) > 1:
            punctuated = f'{unpunctuated_str[0].capitalize()}{unpunctuated_str[1:]}.'
        else:
            punctuated = f'{unpunctuated_str.capitalize()}.'
        return punctuated


    @classmethod
    def conjoin(cls, left_conjunct, right_conjunct):
        """
        Conjoin sentences using a logical conjunction, some English version of the '&' operator.

        If the conjunction is too long,
        it will be split into two sentences
        (although it's still returned as one `str`).

        :param str left_conjunct: string for the left conjunct
        :param str right_conjunct: string for the right conjunct
        :return: string representing the conjunction
        :rtype: str
        """
        cls._initialize()
        if len(left_conjunct) + len(right_conjunct) > UNSAVORY_LENGTH:
            conjunction = cls._conjunction_db.get_start_conjunction()
            conjoined = f"{left_conjunct}. {conjunction.capitalize()} {right_conjunct}"
        else:
            conjunction = cls._conjunction_db.get_middle_conjunction()
            conjoined = f"{left_conjunct} {conjunction} {right_conjunct}"
        return conjoined


    @classmethod
    def as_canonical_conclusion(cls, unpunctuated_str):
        """
        Get the sentence as a conclusion with "therefore" as the indicator.

        Form: Therefore, <sentence>.

        :param str unpunctuated_str: a string representing an unpunctuated sentence
        :return: string representing a conclusion with the canonical indicator
        :rtype: str
        """
        return cls.punctuate(f'therefore, {unpunctuated_str}')


    @classmethod
    def as_natural_conclusion(cls, unpunctuated_str):
        """
        Get the sentence as a conslusion with a random indicator.

        Form: <indicator>, <sentence>.

        :param str unpunctuated_str: a string representing an unpunctuated sentence
        :return: string representing a conclusion with the random indicator
        :rtype: str
        """
        cls._initialize()
        conc_indicator = cls._indicator_db.get_conclusion_indicator()
        return cls.punctuate(f'{conc_indicator} {unpunctuated_str}')


    @classmethod
    def with_middle_prem_indicator(cls, conclusion, premise):
        """
        Combines the sentence with another with a premise indicator in the middle.

        If too long, does something like "self. That's because prem"
        (still returns one `Sentence` object though).

        :param str conclusion: string representing the conclusion.
        :param str premise: string representing the premise(s).
        :return: string representing the arg or sub-arg.
        :rtype: str
        """
        cls._initialize()
        prem_indicator = cls._indicator_db.get_premise_indicator()
        if len(conclusion) + len(premise) > UNSAVORY_LENGTH:
            conc_standin = random.choice(CONCLUSION_STANDINS)
            combined = f"{conclusion}. {conc_standin} {prem_indicator} {premise}"
        else:
            combined = f"{conclusion} {prem_indicator} {premise}"
        return combined
