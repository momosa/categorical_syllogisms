"""
logic.util

A set of handy auxiliary tools
"""

from logic.util.database import Database
from logic.util.randbool import randbool
from logic.util.get_randseed import get_randseed
