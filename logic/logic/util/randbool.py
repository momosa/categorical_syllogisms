"""
randbool.py

randbool
"""

import random

def randbool(random_seed=None):
    """
    A simple function that returns a random boolean value

    :param int random_seed: A seed to determine the result
    :return: a random boolean
    :rtype: bool
    """
    if random_seed is not None:
        random.seed(random_seed)
    return bool(random.getrandbits(1))
