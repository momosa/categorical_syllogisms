"""
database.py

Database
"""

from threading import Lock
import os
import inspect
import json
import copy


# Specific to logic
def _get_logic_db_dir():
    """
    Get the databases location
    """
    db_module_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    logic_db_dir = os.path.join(
        db_module_dir,
        os.pardir,
        os.pardir,
        'databases',
    )
    return logic_db_dir


class Database:
    """
    Allows interaction with a database.

    Database serves as an interface between the database
    and the rest of the program.
    There should not be anything specific to the program here.

    You can usually treat the database like a dict:
    iterate over it, use [], etc.
    But it isn't a dict, so if you really need a dict use `as_dict()`.

    Uses a write lock to avoid conflict.
    Does not use a lock for reads.
    Not save for use across nodes.

    Gotcha: When you modify a db, it modifes all db's with the same db_name.
    So if you want to change a db and save it as something else,
    you should save it first to rename it, make changes, and save it again.

    Gotcha: Like `frozendict`, creating a read-only DB doesn't make values immutable.
    It just makes keys immutable and ensures the DB can't be saved.

    TODO Cannot create a database that doesn't have an existing file

    :param str db_name: the name of the database to initialize
    :param bool read_only: whether or to allow the user to save the DB
        without changing the name
    """

    DB_EXT = '.json'

    _database = {}
    _lock = {}
    _n_using_db = {}
    _read_only_dbs = set()
    _DB_DIR = _get_logic_db_dir()


    def __init__(self, db_name, read_only=False):
        self.db_name = db_name
        self._use_db(db_name)
        if read_only:
            Database._read_only_dbs.add(db_name)


    def __repr__(self):
        return f"Database(db_name={repr(self.db_name)})"


    @staticmethod
    def is_loaded(db_name):
        """
        Determine whether or not a database is loaded

        :param str db_name: The name of the database to check for
        :return: True iff the db has already been loaded
        :rtype: bool
        """
        return db_name in Database._database


    @classmethod
    def get_available(cls, db_type=None):
        """
        Get a list of available databases

        :param str db_type: the type of database to retrieve.
        :return: a list of databases of the given type
        :rtype: list
        """
        if db_type:
            base_dir = os.path.join(cls._DB_DIR, db_type)
        else:
            base_dir = cls._DB_DIR

        walk = _get_saved_dbs(base_dir)
        base_dir = walk[0][0]
        dbs = []
        for subdir in walk:
            sub_db = subdir[0][len(base_dir):].lstrip('/')
            files = subdir[2]
            files = [os.path.join(sub_db, f) for f in files]
            files = [os.path.splitext(f)[0] for f in files]
            dbs += files

        if not db_type:
            dbs = [db for db in dbs if '/' not in db]
        return dbs


    def __iter__(self):
        return self._database.__iter__()


    def __del__(self):
        """
        Unload DB from memory if no instances are using it.
        """
        self._unuse_db()


    def __getitem__(self, key):
        return self._database[key]


    def get(self, key, default=None):
        """
        :param default: the item to return if the key is not in the DB
        :return: the specified object if it is in the DB, default otherwise
        """
        return self._database.get(key, default)


    def __setitem__(self, key, value):
        """
        Set with lock

        :raises AttributeError: if trying to set items in a read-only DB
        """
        if self.db_name in Database._read_only_dbs:
            raise AttributeError(f"{self.db_name} is read-only")
        self._lock.acquire()
        ret_val = self._database[key] = value
        self._lock.release()
        return ret_val


    def __delitem__(self, key):
        """
        Delete with lock

        :raises AttributeError: if trying to delete items in a read-only DB
        """
        if self.db_name in Database._read_only_dbs:
            raise AttributeError("{self.db_name} is read-only")
        self._lock.acquire()
        del self._database[key]
        self._lock.release()


    def __deepcopy__(self, memo):
        """
        Override copy.deepcopy

        This is necessary to ensure DB objects are pickle-able.
        """
        read_only = self.db_name in Database._read_only_dbs
        return Database(self.db_name, read_only)


    def keys(self):
        """
        Get the keys in the database, like a real dict

        :return: the database keys as a set-like dict_keys
        :rtype: dict_keys
        """
        return self._database.keys()


    def items(self):
        """
        Get the key-value pairs in the database, like a real dict

        :return: an iterable of key-value pairs
        :rtype: dict_items
        """
        return self._database.items()


    def values(self):
        """
        Get the values in the database, like a real dict

        :return: an iterable of values
        :rtype: dict_values
        """
        return self._database.values()


    def save(self, new_db_name=None):
        """
        Save the database to a file

        :param str new_db_name: Do not include the extension
        :raises AttributeError: if trying to save over a read-only DB
        """
        db_name = new_db_name if new_db_name else self.db_name

        if db_name in Database._read_only_dbs:
            raise AttributeError(f"{self.db_name} is read-only")
        if db_name != self.db_name:
            db_copy = copy.deepcopy(Database._database[self.db_name])
            Database._database[db_name] = db_copy
            self._unuse_db()
            self._use_db(db_name, load=False)
        db_filename = Database._get_filename(self.db_name)
        self._lock.acquire()
        _write_database(self._database, db_filename)
        self._lock.release()


    def as_dict(self):
        """
        Get the database as a dict

        :return: a copy of the database as a dict
        :rtype: dict
        """
        return copy.deepcopy(self._database)


    @staticmethod
    def _get_filename(db_name):
        """
        Get the full filename of a database

        :param str db_name: The database to get the filename for
        :return: full path of the DB file
        :rtype: str
        """
        basename = db_name + Database.DB_EXT
        db_filename = os.path.join(Database._DB_DIR, basename)
        return db_filename


    def _unuse_db(self):
        """
        Stop using the current DB.

        Unload the DB from memory if no one us using it.

        Locking should not be necessary here because
        unloading only occurs when conflict is impossible.
        """
        try:
            Database._n_using_db[self.db_name] -= 1
            if Database._n_using_db[self.db_name] < 1:
                del Database._database[self.db_name]
                del Database._lock[self.db_name]
                del Database._n_using_db[self.db_name]
                Database._read_only_dbs.discard(self.db_name)
        except AttributeError: # pragma: no cover
            # self.db_name has been removed from memory already
            pass
        except KeyError: # pragma: no cover
            # The database has already been removed from memory
            pass


    def _use_db(self, db_name, load=True):
        """
        Register that we're using the DB.

        Loads the db if necessary

        CAUTION: Only call once when first starting to use the DB.
        Otherwise, it'll look like more instances
        are using the DB than actually are.

        :param str db_name: name of the database, no extension
        :param bool load: iff True, will load the db from file
        """
        if not Database._n_using_db.get(db_name):
            Database._n_using_db[db_name] = 1
            Database._lock[db_name] = Lock()
            if load:
                db_filename = Database._get_filename(db_name)
                Database._lock[db_name].acquire()
                Database._database[db_name] = _get_db_from_file(db_filename)
                Database._lock[db_name].release()
        else:
            Database._n_using_db[db_name] += 1

        self._database = Database._database[db_name]
        self.db_name = db_name
        self._lock = Database._lock[db_name]


def _get_saved_dbs(db_dir): # pragma: no cover
    """
    Get the available dbs

    :param str db_dir: the path to walk searching for dbs
    :return: a walk of the directory, as a tuple
    :rtype: tuple
    """
    return tuple(os.walk(db_dir))


def _get_db_from_file(db_filename): # pragma: no cover
    """
    Get the database as a python dict

    Isolates DB read dependency (json)

    Lock while using this function to keep anyone else from modifying
    the database while we're trying to read it.

    :param str db_filename: the full path to the database file
    :return: data structure representing the database
    :rtype: dict
    """
    with open(db_filename) as json_data:
        data = json.loads(json_data.read())
    return data


def _write_database(database, db_filename): # pragma: no cover
    """
    Write the database to the given file
    Isolates DB write dependency

    :param str filename: full path of the target file
    """
    with open(db_filename, 'w') as outfile:
        json.dump(database, outfile)
