"""
get_randseed.py

randbool
"""

import random


MAX_SEED = 2**16

def get_randseed():
    """
    Get an integer to use as a random seed

    :return: a random integer
    :rtype: int
    """
    return random.randint(1, MAX_SEED)
