"""
proposition_contradictory.py

PropositionContradictory
"""

import random
from typing import Dict, Union
from dataclasses import dataclass

from logic.util import get_randseed
from logic.categorical.util.proposition_transformer import (
    contradictory,
    converse,
    contrary,
    subalternate,
)
from logic.categorical.proposition import CategoricalProposition
from logic.categorical.english.proposition_to_str import natural_categorical_proposition


@dataclass
class PropositionContradictory:
    """
    Exercises for "What is the contradictory of the given proposition?"

    Input is a spec for a categorical proposition.

    :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
    :param str proposition_type: The type of categorical proposition.
        Can be A, E, I, or O.
    :param str subject: The subject. Should be a plural noun.
    :param str predicate: The predicate. Should be a plural noun.
    :param bool truth_value: The truth value of the categorical proposition.
    :param str random_seed: Determines randomization.
        Use to reproduce randomly generated categorical propositions.
    :raises: KeyError on unknown proposition type
    :raises: ValueError on inconsistent input data
    :raises: AssertionError if given a subject or predicate but no topic name
    """
    random_seed: int = None
    question: Dict[str, str] = None
    correct: str = None
    responses: Dict[str, Union[str, bool]] = None
    input_item: CategoricalProposition = None

    def __init__(self, **kwargs):
        self.random_seed = kwargs.setdefault('random_seed', get_randseed())
        self.input_item = self._generate_input_item(**kwargs)
        random.seed(self.random_seed)
        self.correct = self._generate_correct()
        self.question = self._generate_question()
        self.responses = self._generate_responses()


    def is_correct(self, candidate):
        """
        Determine whether or not the candidate is correct

        :param candidate: a candidate response
        :type candidate: CategoricalProposition or kwargs
        :return: True if the candidate is correct, False otherwise
        :rtype: bool
        """
        if hasattr(candidate, 'get'):
            prop_candidate = CategoricalProposition(**candidate)
        else:
            prop_candidate = candidate
        return prop_candidate == self.correct


    def get_hint(self, candidate):
        """
        Get hint about choosing a candidate

        :param candidate: a candidate response
        :type candidate: CategoricalProposition or kwargs
        :return: Hint text
        :rtype: str
        """
        if self.is_correct(candidate):
            hint = "Correct!"
        else:
            hint = (
                "Translate the English sentence into categorical logic. "
                "Then use the square of opposition to find the contradictory. "
                "One at a time, starting with the most promising candidate, "
                "translate the choices into categorical logic "
                "until you find one that matches."
            )

        return hint


    @staticmethod
    def _generate_input_item(**kwargs):
        """
        Create the input item for the contradictory proposition exercise

        :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
        :param str proposition_type: The type of categorical proposition.
            Can be A, E, I, or O.
        :param str subject: The subject. Should be a plural noun.
        :param str predicate: The predicate. Should be a plural noun.
        :param bool truth_value: The truth value of the categorical proposition.
        :param str random_seed: Determines randomization.
            Use to reproduce randomly generated categorical propositions.
        :raises: KeyError on unknown proposition type
        :raises: ValueError on inconsistent input data
        :raises: AssertionError if given a subject or predicate but no topic name
        """
        input_for_contradictory_prop = CategoricalProposition(**kwargs)
        return input_for_contradictory_prop


    def _generate_correct(self):
        """
        Get the correct response to the exercise

        Assumes self.input_item has been set

        :return: the contradictory of the proposition
        :rtype: CategoricalProposition
        """
        return contradictory(self.input_item)


    def _generate_question(self):
        """
        Generate the question

        Assumes self.input_item has been set
        """
        question = {
            'proposition': natural_categorical_proposition(self.input_item),
            'question': (
                "What is the contradictory (opposite) "
                "of that proposition?"
            )
        }
        return question


    def _generate_responses(self):
        """
        Generate a set of 4 responses, exactly one correct

        Assumes self.input_item and self.correct have been set

        :return: list of responses
        :rtype: list
        """
        response_props = [
            self.correct,
            converse(self.input_item),
            contrary(self.input_item),
            subalternate(self.input_item),
        ]
        random.shuffle(response_props)
        responses = [
            {
                'text': natural_categorical_proposition(prop),
                'correct': self.is_correct(prop),
                'hint': self.get_hint(prop),
            }
            for prop in response_props
        ]
        return responses
