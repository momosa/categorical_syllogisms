"""
logic.categorical.exercise

Exercises for categorical logic
"""

from logic.categorical.exercise.proposition_type import PropositionType
from logic.categorical.exercise.proposition_contradictory import PropositionContradictory
from logic.categorical.exercise.syllogism_soundness import SyllogismSoundness
