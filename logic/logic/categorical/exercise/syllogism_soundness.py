"""
syllogism_soundness.py

SyllogismSoundness
"""

import random
from typing import Dict, Union
from dataclasses import dataclass

from logic.util import get_randseed
from logic.categorical.syllogism import CategoricalSyllogism
from logic.categorical.english.syllogism_to_str import natural_categorical_syllogism


RESPONSES = [
    'sound',
    'valid but unsound',
    'sound but invalid',
    'invalid',
]


def _validate_candidate(candidate):
    """
    Determine if a candidate response is legitimate.

    :param str candidate: A candidate response for the argument soundness exercise
    :raises: ValueError if candidate is not a legitimate response
    """
    if candidate not in RESPONSES:
        raise ValueError(
            f"Illegitimate candidate {candidate}. "
            f"Candidates must be one of {RESPONSES}."
        )

@dataclass
class SyllogismSoundness:
    """
    Exercise for determining the validity and soundness of a categorical syllogism

    Input is a spec for a categorical syllogism.

    :param str random_seed: Determines randomization.
        Use to reproduce randomly generated categorical syllogisms.
    :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
    :param dict propositions: Propositions from which to construct the syllogism
    :param dict categories: Categories from which to construct the syllogism
    :param str form: Form of the categorical syllogism
    :param bool is_valid: Whether or not the syllogism is valid
    :param bool is_sound: Whether or not the syllogism is sound
    :return: Kwargs specifying a `CategoricalSyllogism`
    :rtype: dict
    :raises: ValueError on illegitimate input data
    """

    question: Dict[str, str] = None
    correct: str = None
    random_seed: int = None
    responses: Dict[str, Union[str, bool]] = None
    input_item: CategoricalSyllogism = None


    def __init__(self, **kwargs):
        self.random_seed = kwargs.setdefault('random_seed', get_randseed())
        self.input_item = self._generate_input_item(**kwargs)
        random.seed(self.random_seed)

        self.question = self._generate_question()
        self.correct = self._generate_correct()
        self.responses = self._generate_responses()


    def is_correct(self, candidate):
        """
        Determine whether or not the candidate is correct

        :param str candidate: a candidate response describing the syllogism's validity/soundness
        :return: True if the candidate is correct, False otherwise
        :rtype: bool
        :raises: ValueError if candidate is not a legitimate response
        """
        _validate_candidate(candidate)
        return candidate == self.correct


    def get_hint(self, candidate):
        """
        Get hint about choosing a candidate

        Candiates are dictionaries with the following structure:

            {
                'is_valid': True,
                'is_sound': True,
            }

        :param dict candidate: a candidate response describing the syllogism's validity/soundness
        :return: A hint about how to find the solution
        :rtype: str
        :raises: ValueError if candidate is not a legitimate response
        """
        _validate_candidate(candidate)
        if self.is_correct(candidate):
            hint = "Correct!"
        else:
            hint = (
                "Start by representing in standard categorical form. "
                "Assess validity using a Venn diagram "
                "or the mood and figure of the syllogism. "
                "Use the truth value of the premises together with validity "
                "to determine soundness."
            )

        return hint


    @staticmethod
    def _generate_input_item(**kwargs):
        """
        Create the input item for the argument soundness exercise

        CategoricalSyllogism will attempt to create an argument
        with Boolean soundness and validity if possible.
        Validity must be boolean.
        So if soundness isn't Boolean,
        we've been input incompatible with the exercise because
        we the exercise may not have a determinate response.

        :param str random_seed: Determines randomization.
            Use to reproduce randomly generated categorical syllogisms.
        :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
        :param dict propositions: Propositions from which to construct the syllogism
        :param dict categories: Categories from which to construct the syllogism
        :param str form: Form of the categorical syllogism
        :param bool is_valid: Whether or not the syllogism is valid
        :param bool is_sound: Whether or not the syllogism is sound
        :return: Kwargs specifying a `CategoricalSyllogism`
        :rtype: dict
        :raises: ValueError if cannot create a syllogism with a determinate correct answer
        """
        input_item = CategoricalSyllogism(**kwargs)
        if input_item.is_sound not in [True, False]:
            raise ValueError(
                f"Cannot create a categorical syllogism with "
                f"determinate soundness from {kwargs}."
            )
        return input_item


    def _generate_correct(self):
        """
        Get the correct response to the exercise

        Assumes self.input_item has been set

        :return: the correct statement about the argument's validity and soundness
        :rtype: str
        """
        if self.input_item.is_valid is True:
            if self.input_item.is_sound is True:
                correct = 'sound'
            else:
                correct = 'valid but unsound'
        else:
            correct = 'invalid'

        _validate_candidate(correct)
        return correct


    def _generate_question(self):
        """
        Generate the question

        Assumes self.input_item has been set
        """
        question = {
            'syllogism': natural_categorical_syllogism(self.input_item),
            'question': (
                "Assess the validity and soundness of the categorical syllogism."
            )
        }
        return question


    def _generate_responses(self):
        """
        Get a list of 4 responses, exactly one correct

        :return: a list of 4 responses
        :rtype: list
        """
        response_list = [
            {
                'text': response,
                'correct': self.is_correct(response),
                'hint': self.get_hint(response),
            }
            for response in RESPONSES
        ]
        return response_list
