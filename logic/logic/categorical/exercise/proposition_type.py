"""
proposition_type.py

PropositionType
"""

import random
from typing import Dict, Union
from dataclasses import dataclass

from logic.util import get_randseed
from logic.categorical.util.proposition_type import PROPOSITION_TYPES
from logic.categorical.proposition import CategoricalProposition
from logic.categorical.english.proposition_to_str import natural_categorical_proposition


@dataclass
class PropositionType:
    """
    Exercise for "What type is the given proposition?"

    Input is a spec for a categorical proposition.

    :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
    :param str proposition_type: The type of categorical proposition.
        Can be A, E, I, or O.
    :param str subject: The subject. Should be a plural noun.
    :param str predicate: The predicate. Should be a plural noun.
    :param bool truth_value: The truth value of the categorical proposition.
    :param str random_seed: Determines randomization.
        Use to reproduce randomly generated categorical propositions.
    :raises: KeyError on unknown proposition type
    :raises: ValueError on inconsistent input data
    :raises: AssertionError if given a subject or predicate but no topic name
    """
    question: Dict[str, str] = None
    correct: str = None
    responses: Dict[str, Union[str, bool]] = None
    input_item: CategoricalProposition = None
    random_seed: int = None


    def __init__(self, **kwargs):
        self.random_seed = kwargs.setdefault('random_seed', get_randseed())
        self.input_item = self._generate_input_item(**kwargs)
        random.seed(self.random_seed)
        self.question = self._generate_question()
        self.correct = self._generate_correct()
        self.responses = self._generate_responses()

    def is_correct(self, candidate):
        """
        Determine whether or not the candidate is correct

        :param str candidate: a candidate response categorical proposition type
        :return: True if the candidate is correct, False otherwise
        :rtype: bool
        """
        return candidate == self.correct


    def get_hint(self, candidate):
        """
        Get hint about choosing a candidate

        :param str candidate: a candidate response categorical proposition type
        :return: A hint about how to find the solution
        :rtype: str
        """
        universal = ('A', 'E')
        existential = ('I', 'O')

        if self.is_correct(candidate):
            hint = "Correct!"
        elif self.correct in universal:
            if candidate in universal:
                hint = (
                    "Remember that shading indicates that there's nothing "
                    "in the shaded region."
                )
            elif candidate in existential:
                hint = (
                    "Remember that an 'x' only indicates that something "
                    "exists. It doesn't give us information about "
                    "everything in a category."
                )
        elif self.correct in existential:
            if candidate in existential:
                hint = (
                    "Remember what each subregion represents. What kind "
                    "of things are in the region you selected?"
                )
            elif candidate in universal:
                hint = (
                    "Remember that shading gives information about "
                    "everything in a category."
                )

        return hint


    @staticmethod
    def _generate_input_item(**kwargs):
        """
        Create the input item for the proposition type exercise

        :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
        :param str proposition_type: The type of categorical proposition.
            Can be A, E, I, or O.
        :param str subject: The subject. Should be a plural noun.
        :param str predicate: The predicate. Should be a plural noun.
        :param bool truth_value: The truth value of the categorical proposition.
        :param str random_seed: Determines randomization.
            Use to reproduce randomly generated categorical propositions.
        :raises: KeyError on unknown proposition type
        :raises: ValueError on inconsistent input data
        :raises: AssertionError if given a subject or predicate but no topic name
        """
        input_for_prop_type = CategoricalProposition(**kwargs)
        return input_for_prop_type


    def _generate_correct(self):
        """
        Get the correct response to the exercise

        Assumes self.input_item has been set

        :return: the correct type of proposition
        :rtype: str
        """
        return self.input_item.proposition_type


    def _generate_question(self):
        """
        Generate the question

        Assumes self.input_item has been set
        """
        question = {
            'proposition': natural_categorical_proposition(self.input_item),
            'question': (
                "What type of categorical proposition "
                "does that express?"
            )
        }
        return question


    def _generate_responses(self):
        """
        Generate a set of 4 responses, exactly one correct

        :return: list of responses
        :rtype: list
        """
        responses = [
            {
                'text': candidate,
                'correct': self.is_correct(candidate),
                'hint': self.get_hint(candidate),
            }
            for candidate in PROPOSITION_TYPES
        ]
        return responses
