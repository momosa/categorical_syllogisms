"""
quantifier.py

canonical(proposition_type)
natural(proposition_type, order_swapping=False)

We haven't added the following because their
use cases are more nuanced:

    a few
    at least one
    several
    a coulple of
    that
    this
    most
    many
    any
"""


import random
from logic.util import randbool


QUANTIFIERS = {
    'positive universal': ['all', 'every', 'each'],
    'order-swapping positive universal': ['only', 'just'],
    'negative universal': ['no'],
    'existential': ['some'],

    'singular': ['every', 'any', 'each'],
    'plural': ['all', 'only', 'just'],
}


def canonical(proposition_type):
    """
    Get the canonical quantifier corresponding to the given type

    :param str proposition_type: type of categorical proposition
        'A', 'E', 'I', 'O'
    :return: The corresponding quantifier in English, canonical only
    :rtype: str
    :raises: ValueError on unrecognized proposition type
    """
    quantifier_map = {
        'A': 'all',
        'E': 'no',
        'I': 'some',
        'O': 'some',
    }

    try:
        return quantifier_map[proposition_type]
    except KeyError:
        raise ValueError(
            f"Unrecognized proposition_type {repr(proposition_type)}"
        )


def natural(proposition_type, order_swapping=False):
    """
    Get a quantifier for the given proposition type.

    :param str proposition_type: type of categorical proposition
        'A', 'E', 'I', 'O'
    :param bool order_swapping: use an order-swapping quantifier,
        like 'only' vs 'all'
    :return: An English quantifier corresponding to the input,
        may not be canonical
    :rtype: str
    :raises: ValueError on unrecognized proposition type
    """
    # A dict mapping won't work here because 'O' needs to make a recursive call

    if proposition_type == 'A':
        if order_swapping:
            quantifier = random.choice(
                QUANTIFIERS['order-swapping positive universal']
            )
        else:
            quantifier = random.choice(
                QUANTIFIERS['positive universal']
            )

    elif proposition_type == 'E':
        quantifier = random.choice(
            QUANTIFIERS['negative universal']
        )

    elif proposition_type == 'I':
        quantifier = random.choice(
            QUANTIFIERS['existential']
        )

    elif proposition_type == 'O':
        if order_swapping:
            quantifier = f"not {natural('A', order_swapping=True)}"
        else:
            options = QUANTIFIERS['existential'] + [f"not {natural('A', order_swapping=False)}"]
            quantifier = random.choice(options)

    else:
        raise ValueError(
            f"Unrecognized proposition_type {repr(proposition_type)}"
        )

    return quantifier


def is_singular(quantifier_str):
    """
    Determine if a quantifier is singular

    :param str quantifier_str: A categorical quantifier like 'all' or 'some'
    :return: Whether or not to use singular terms with the quantifier.
        If the quantifier can be used either way, returns a random response
    :rtype: bool
    """
    if quantifier_str.startswith('not'):
        quantifier_str = quantifier_str[4:]

    if quantifier_str in QUANTIFIERS['singular']:
        singular = True
    elif quantifier_str in QUANTIFIERS['plural']:
        singular = False
    else:
        singular = randbool()
    return singular
