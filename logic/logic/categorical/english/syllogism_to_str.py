"""
syllogism_to_str.py
"""


from logic.categorical.english.proposition_to_str import (
    canonical_categorical_proposition,
    natural_categorical_proposition,
)
from logic.english.syllogism import (
    canonical_syllogism,
    natural_syllogism,
)


def canonical_categorical_syllogism(categorical_syllogism):
    """
    Get the canonical English form of a categorical syllogism

    :param CategoricalSyllogism categorical_syllogism:
        The categorical syllogism object to render as a string
    :return: A string representing the canonical form of the syllogism
    :rtype: str
    """
    first_premise = canonical_categorical_proposition(
        categorical_syllogism.propositions['major_premise'],
        punctuate=False,
    )
    second_premise = canonical_categorical_proposition(
        categorical_syllogism.propositions['minor_premise'],
        punctuate=False,
    )
    conclusion = canonical_categorical_proposition(
        categorical_syllogism.propositions['conclusion'],
        punctuate=False,
    )
    return canonical_syllogism(first_premise, second_premise, conclusion)


def natural_categorical_syllogism(categorical_syllogism):
    """
    Get the natural English form of a categorical syllogism

    :param CategoricalSyllogism categorical_syllogism:
        The categorical syllogism object to render as a string
    :return: A string representing the natural English form of the syllogism
    :rtype: str
    """
    first_premise = natural_categorical_proposition(
        categorical_syllogism.propositions['major_premise'],
        punctuate=False,
    )
    second_premise = natural_categorical_proposition(
        categorical_syllogism.propositions['minor_premise'],
        punctuate=False,
    )
    conclusion = natural_categorical_proposition(
        categorical_syllogism.propositions['conclusion'],
        punctuate=False,
    )
    return natural_syllogism(first_premise, second_premise, conclusion)
