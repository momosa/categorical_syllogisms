"""
copula.py

Tools for manipulating copula.
"""


def canonical(proposition_type):
    """
    Get the canonical copula for the given proposition type

    :param str proposition_type: the categorical proposition type.
        Can be 'A', 'E', 'I', or 'O'
    :return: The copula corresponding to the proposition type
    :rtype: str
    """
    copula = 'are not' if proposition_type == 'O' else 'are'
    return copula


def natural(proposition_type, pluralization, is_contrary=False):
    """
    Gets the plain English version of the copula.

    The copula is the term joining the subject and predicate.

    :param str proposition_type: the categorical proposition type.
        Can be 'A', 'E', 'I', or 'O'
    :param str pluralization: Indicates whether to use the plural or singular form
    :param bool is_contrary: Indicates whether or not we want negate the copula.
        Negating the copula of a contrary (A <=> E) yields
        a proposition equivalent to the original.
        In spoken English, we sometimes do this with E-type propositions.
        E.g., "No sharks are mammals" => "Sharks aren't mammals"
    :return: The copula
    :rtype: str
    :raises: ValueError if unrecognized pluralization or contrary status
    """
    is_negative = proposition_type == 'O'
    if is_contrary:
        is_negative = not is_negative

    copula_map = {
        # pluralization, is_negative
        ('plural', False): 'are',
        ('singular', False): 'is',
        ('plural', True): "aren't",
        ('singular', True): "isn't",
    }

    try:
        return copula_map[pluralization, is_negative]
    except KeyError:
        raise ValueError(
            f"Unrecognized pluralization {repr(pluralization)} or "
            f"is_contrary {repr(is_contrary)}."
        )
