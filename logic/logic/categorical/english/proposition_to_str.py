"""
proposition_to_str.py

Methods to transform categorical propositions to strings
"""

from logic.util import randbool

from logic.categorical.topic import Topic
from logic.categorical.util import proposition_transformer
from logic.english.sentence import Sentence
from logic.categorical.english import quantifier
from logic.categorical.english import copula


def canonical_categorical_proposition(categorical_proposition, punctuate=True):
    """
    Get a canonical representation of the categorical proposition.

    :param CategoricalProposition categorical_proposition:
        A categorical proposition to convert into a string
    :return: The canonical representation of the categorical proposition.
    :rtype: str
    """
    unpunctuated = _canonical_unpunctuated(categorical_proposition)
    canonical_sentence = Sentence.punctuate(unpunctuated) if punctuate else unpunctuated
    return canonical_sentence


def natural_categorical_proposition(categorical_proposition, punctuate=True):
    """
    Get a natural English representation of the categorical proposition.

    :param CategoricalProposition categorical_proposition:
        A categorical proposition to convert into a string
    :return: The natural English representation of the categorical proposition.
    :rtype: str
    """
    unpunctuated = _natural_unpunctuated(categorical_proposition)
    natural_sentence = Sentence.punctuate(unpunctuated) if punctuate else unpunctuated
    return natural_sentence


def _canonical_unpunctuated(categorical_proposition):
    """
    Get an unpunctuated canonical representation of the categorical proposition.

    Yields a `str` that can be used in the context of an argument.

    :param CategoricalProposition categorical_proposition:
        A categorical proposition to convert into a string
    :return: The canonical representation of the categorical proposition.
    :rtype: str
    """
    topic = Topic(categorical_proposition.topic_name)
    quantifier_str = quantifier.canonical(categorical_proposition.proposition_type)
    subject = topic[categorical_proposition.subject]['plural']
    copula_str = copula.canonical(categorical_proposition.proposition_type)
    predicate = topic[categorical_proposition.predicate]['plural']
    return f"{quantifier_str} {subject} {copula_str} {predicate}"


def _natural_unpunctuated(categorical_proposition):
    """
    Get an unpunctuated natural English representation of the categorical proposition.

    Yields a `str` that can be used in the context of an argument.

    Anything designated randomly could presumably be designated more intelligently.

    :param CategoricalProposition categorical_proposition:
        A categorical proposition to convert into a string
    :return: The canonical representation of the categorical proposition.
    :rtype: str
    """
    order_swapping_quantifier = False
    use_contrary_copula = False
    proposition = categorical_proposition
    topic = Topic(categorical_proposition.topic_name)
    pluralization = None
    quantifier_str = None

    if proposition.proposition_type in ['A', 'O']:
        order_swapping_quantifier = randbool()
    if proposition.proposition_type == 'E':
        use_contrary_copula = randbool()
        # Block negative copula in universal with explicit quantifier
        # Reason: 'all that glitters is not gold' & co. are ambiguous
        if use_contrary_copula:
            # 'all' will be made implicit later
            quantifier_str = 'all'

    if order_swapping_quantifier:
        proposition = proposition_transformer.converse(proposition)
    if use_contrary_copula:
        proposition = proposition_transformer.contrary(proposition)

    quantifier_str = quantifier_str or quantifier.natural(
        proposition.proposition_type,
        order_swapping_quantifier
    )

    if quantifier_str.startswith('not'):
        proposition = proposition_transformer.contradictory(proposition)

    pluralization = 'singular' if quantifier.is_singular(quantifier_str) else 'plural'
    copula_str = copula.natural(
        proposition.proposition_type,
        pluralization,
        use_contrary_copula,
    )

    subject = topic[proposition.subject][pluralization]
    predicate = topic[proposition.predicate][pluralization]

    # Always: "all mammals aren't reptiles" => "mammals aren't reptiles"
    # Randomly: "all mammals are animals" => "mammals are animals"
    if all((
            proposition.proposition_type == 'A',
            not order_swapping_quantifier,
            pluralization == 'plural',
            use_contrary_copula or randbool(),
        )):
        quantifier_str = ''

    if pluralization == 'singular':
        predicate = f"an {predicate}" if predicate[0] in 'aeiou' else f"a {predicate}"

    return f"{quantifier_str} {subject} {copula_str} {predicate}".strip()
