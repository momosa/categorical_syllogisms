"""
proposition.py

CategoricalProposition
"""

from dataclasses import dataclass
from typing import Union

from logic.categorical.util.proposition_input import determine_proposition_kwargs


@dataclass
class CategoricalProposition:
    """
    A class for categorical propositions

    All input is optional:
    full specs are randomly determined from partial or no input.

    A random seed is generated so randomly generated propositions are repeatable.
    Note that the random seed is only guaranteed to generate
    the same proposition given the same input.
    That's relevant if partial specs were given for the original proposition.

    Example:

        >>> from logic.categorical import CategoricalProposition
        >>> prop = CategoricalProposition()
        >>> prop.topic_name
        'fruits_and_vegetables'
        >>> prop.subject
        'oranges'
        >>> prop.random_seed
        46450
        >>> another_prop = CategoricalProposition(random_seed=prop.random_seed)
        >>> prop == another_prop
        True
        >>> from dataclasses import asdict
        >>> prop_from_dict = CategoricalProposition(**asdict(prop))
        >>> prop == prop_from_dict
        True

    :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
    :param str proposition_type: The type of categorical proposition.
        Can be A, E, I, or O.
    :param str subject: The subject. Should be a plural noun.
    :param str predicate: The predicate. Should be a plural noun.
    :param bool truth_value: The truth value of the categorical proposition.
    :param str random_seed: Determines randomization.
        Use to reproduce randomly generated categorical propositions.
    :raises: KeyError on unknown proposition type
    :raises: ValueError on inconsistent input data
    :raises: AssertionError if given a subject or predicate but no topic name
    """

    topic_name: str
    random_seed: int
    proposition_type: str
    subject: str
    predicate: str
    truth_value: Union[bool, None]


    def __init__(self, **kwargs):
        filled_kwargs = determine_proposition_kwargs(**kwargs)
        for attr_name, attr_value in filled_kwargs.items():
            setattr(self, attr_name, attr_value)
