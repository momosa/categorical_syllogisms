"""
topic.py

Topic
"""

import random

from logic.util import Database
from logic.categorical.util.fill_topic import fill_topic_words, fill_topic_truth


DB_SUBDIR = 'category_topics'


class Topic(Database):
    """
    A class representing a group of terms, like fruits and vegetables.

    A topic represents a collection of terms
    corresponding to a topic of conversation.

    A Topic is dict-like, so you can iterate on term keys and so forth.

    :Example:

        >>> from logic.categorical import Topic
        >>> Topic.get_available()
        ['fruits_and_vegetables', 'rocks_and_minerals']
        >>> my_topic = Topic('fruits_and_vegetables')
        >>> 'apples' in my_topic
        True

    :param str topic_name: name of the topic
    :param int random_seed: Determines randomization. Used for testing.
    """


    def __init__(self, topic_name=None, random_seed=None):
        self.topic_name = topic_name or self.random_topic_name(random_seed)

        db_full_name = f"{DB_SUBDIR}/{self.topic_name}"
        is_topic_filled = Database.is_loaded(db_full_name)
        super(Topic, self).__init__(
            db_name=db_full_name,
            read_only=True,
        )

        if not is_topic_filled:
            fill_topic_words(self)
            fill_topic_truth(self)


    def __repr__(self):
        return f"Topic(topic_name={repr(self.topic_name)})"


    @classmethod
    def get_available(cls): #pylint: disable=arguments-differ
        """
        Get a list of available topics

        :return: a list of topic names
        :rtype: list
        """
        return super(Topic, cls).get_available(DB_SUBDIR)


    @classmethod
    def random_topic_name(cls, random_seed=None):
        """
        Get a random topic_name

        :param int random_seed: A seed used to determine random results.
            Used for testing.
        :return: A random topic name
        :rtype: str
        """
        if random_seed is not None:
            random.seed(random_seed)
        available = cls.get_available()
        return random.choice(available)
