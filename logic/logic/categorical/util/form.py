"""
form.py

Form
"""

import re
import random
from textwrap import dedent
from logic.util import get_randseed, randbool
from logic.categorical.util.proposition_type import PROPOSITION_TYPES


VALID_FORMS = (
    "AAA-1",
    "AOO-2",
    "OAO-3",
    "AEE-2",
    "AEE-4",
    "EAE-1",
    "EAE-2",
    "AII-1",
    "AII-3",
    "IAI-3",
    "IAI-4",
    "EIO-1",
    "EIO-2",
    "EIO-3",
    "EIO-4",
)


class Form:
    """
    A class for working with forms of categorical syllogisms
    """


    def __init__(self):
        raise AttributeError(
            "Form class cannot be instantiated. "
            "The class consists of static and class-level items."
        )


    @staticmethod
    def validate(form):
        """
        Validate that a categorical form is legitimate

        Legitimate forms look like AEI-1, OEA-4, etc.

        :param str form: The form to validate
        :raises: ValueError for illegitimate categorical form
        """
        if not re.match(r'^[AEIO]{3}-[1234]$', form):
            raise ValueError(
                f"Unrecognized categorical syllogism form {repr(form)}"
            )


    @staticmethod
    def from_propositions(major_premise, minor_premise, conclusion):
        """
        Get a form from a set of propositions

        :param CategoricalProposition major_premise: The major premise
        :param CategoricalProposition minor_premise: The minor premise
        :param CategoricalProposition conclusion: The conclusion
        :return: The form of the categorical syllogism, like AEI-2
        :rtype: str
        :raises: ValueError for illegitimate categorical form
        """
        mood = (
            f"{major_premise.proposition_type}"
            f"{minor_premise.proposition_type}"
            f"{conclusion.proposition_type}"
        )

        if major_premise.subject == minor_premise.predicate:
            figure = 1
        elif major_premise.predicate == minor_premise.predicate:
            figure = 2
        elif major_premise.subject == minor_premise.subject:
            figure = 3
        elif major_premise.predicate == minor_premise.subject:
            figure = 4
        else:
            raise ValueError(dedent("""
                Cannot determine a legitimate categorical form from the propositions.
                Major premise: {repr(major_premise)}
                Minor premise: {repr(minor_premise)}
                Conclusion: {repr(conclusion)}
            """))

        form = f"{mood}-{figure}"
        Form.validate(form)
        return form


    @staticmethod
    def random(is_valid=None, random_seed=None):
        """
        Get a random categorical form

        :param bool is_valid: A boolean value indicating whether or not the form should be valid
        :param int random_seed: Determines random generation
        :return: A random form given the constraints
        :rtype: str
        """
        random_seed = random_seed if random_seed is not None else get_randseed()
        random.seed(random_seed)

        if is_valid is None:
            is_valid = randbool()

        form = random.choice(VALID_FORMS)
        if not is_valid:
            while Form.is_valid(form):
                form = (
                    f"{random.choice(PROPOSITION_TYPES)}"
                    f"{random.choice(PROPOSITION_TYPES)}"
                    f"{random.choice(PROPOSITION_TYPES)}"
                    "-"
                    f"{random.choice((1, 2, 3, 4))}"
                )

        return form


    @staticmethod
    def is_valid(form):
        """
        Determine whether or not a categorical form is valid

        :param str form: A categorical form like AEI-2
        :return: True if the form is valid, False otherwise
        :rtype: bool
        :raises: ValueError for illegitimate categorical form
        """
        Form.validate(form)
        return form in VALID_FORMS
