"""
fill_topic.py

fill_topic_words(topic)
fill_topic_truth(topic)
"""

from collections import deque

from logic.english import get_plural_noun
from logic.categorical.util.proposition_type import PROPOSITION_TYPES


def fill_topic_words(topic):
    """
    Fill out the topic words from partial info

    We expect the input to have the form:

        {
            'first_term_key': term_info,
            'second_term_key': term_info,
            ...
        }

    Where `term_info` should contain a `'singular'` key
    and possibly also a `'plural'` key.

    :param Database topic: the dict-like topic db to fill out
    """
    for term_key, term_info in topic.items():
        assert 'singular' in term_info, (
            f"{repr(term_key)} needs a singular form in {repr(topic)}"
        )
        if 'plural' not in term_info:
            term_info['plural'] = get_plural_noun(term_info['singular'])

def fill_topic_truth(topic):
    """
    Fill out truth info for the topic

    Given partial truth information, fill out the rest.

    We expect the input to have the form:

        {
            'first_term_key': term_info,
            'second_term_key': term_info,
            ...
        }

    Where `term_info` is a dict that may or may not
    have include (as well as other info) any of the following:

        'truth': {
            'A': [predicate_term_keys...],
            'E': [predicate_term_keys...],
            'I': [predicate_term_keys...],
            'O': [predicate_term_keys...],
        },

    For instance, `topic[S]['truth']['A']` contains
    the predicate keys P for which for which `All S are P` is true.

    This function fills out each term's truth info as well as possible.

    :param Database topic: the dict-like topic db to fill out
    """
    _fill_skeleton(topic)
    _fill_a(topic)
    _fill_e(topic)
    _fill_i(topic)
    _fill_o(topic)


def _fill_skeleton(topic):
    """
    Fill out the truth skeleton

    :param Database topic: the terms to fill out
    """
    for term_key in topic:
        if 'truth' not in topic[term_key]:
            topic[term_key]['truth'] = {}
        for prop_type in PROPOSITION_TYPES:
            if prop_type not in topic[term_key]['truth']:
                topic[term_key]['truth'][prop_type] = []


def _fill_a(topic, to_check=None):
    """
    Fill out the A info for the terms

    Assumes irreflexive partial ordering of terms

    A rules: transitivity, (rAs & sAt) -> rAt

    :param Database topic: the terms to fill out
    """
    if to_check is None:
        to_check = deque(topic.keys())
    while to_check:
        term_key = to_check.popleft()
        supersets = topic[term_key]['truth']['A']
        assert term_key not in supersets, "A-type relations must be irreflexive"

        # We need to update all supersets first
        checked_supersets = not set(supersets).intersection(set(to_check))
        if not checked_supersets:
            to_check.append(term_key)

        else:
            grandsupersets = [
                k for superset in supersets
                for k in topic[superset]['truth']['A']
            ]
            updated = set(topic[term_key]['truth']['A'] + grandsupersets)
            topic[term_key]['truth']['A'] = list(updated)


def _fill_e(terms):
    """
    Fill out the E info for the terms

    Assumes A terms are filled out

    E rules: symmetry, (rAs & sEt) -> rEt

    :param list terms: the terms to fill out
    """
    _fill_symmetric(terms, 'E')

    for key in terms:
        for supset in terms[key]['truth']['A']:
            new = [
                k for k in terms[supset]['truth']['E']
                if k not in terms[key]['truth']['E']
            ]
            terms[key]['truth']['E'] += new
            for other in new:
                terms[other]['truth']['E'].append(key)


def _fill_i(terms):
    """
    Fill out the I info for the terms

    Assumes A terms are filled out

    I rules: symmetry, (sAt & sIr) -> tIr

    :param list terms: the terms to fill out
    """
    _fill_symmetric(terms, 'I')

    for key in terms:
        for supset in terms[key]['truth']['A']:
            new = [
                k for k in terms[key]['truth']['I']
                if k not in terms[supset]['truth']['I'] and k != supset
            ]
            terms[supset]['truth']['I'] += new
            for other in new:
                terms[other]['truth']['I'].append(supset)


def _fill_o(terms):
    """
    Fill out the O info for the terms

    Assumes all other terms are filled out

    O rules:
        (rIs & sEt) -> rOt
        (rI/Os & rEt) -> rOt
        (rAs & tOs) -> tOr

    :param list terms: the terms to fill out
    """
    for key in terms:
        new_ohs = list({
            new_oh for mid in terms[key]['truth']['I']
            for new_oh in terms[mid]['truth']['E']
            if new_oh not in terms[key]['truth']['O']
        })
        terms[key]['truth']['O'] += new_ohs

    for key in terms:
        if terms[key]['truth']['I'] or terms[key]['truth']['O']:
            new = [
                k for k in terms[key]['truth']['E']
                if k not in terms[key]['truth']['O']
            ]
            terms[key]['truth']['O'] += new

    subsets = [
        term for term in terms
        if terms[term]['truth']['A']
    ]
    for subset in subsets:
        supersets = terms[subset]['truth']['A']
        not_subsets_of_a_superset = []
        for term in terms:
            for superset in supersets:
                if superset in terms[term]['truth']['O']:
                    not_subsets_of_a_superset.append(term)
        for o_subject in not_subsets_of_a_superset:
            if subset not in terms[o_subject]['truth']['O']:
                terms[o_subject]['truth']['O'].append(subset)


def _fill_symmetric(terms, prop_type):
    """
    Fill out the symmetric relations

    :param list terms: the terms to fill out
    :param str prop_type: 'E' or 'I'
    """
    assert prop_type in ['E', 'I'], "Symmetric relation required"
    for key in terms:
        for other in terms[key]['truth'][prop_type]:
            if key not in terms[other]['truth'][prop_type]:
                terms[other]['truth'][prop_type].append(key)
