"""
truth_list.py

truth_list
"""

from logic.categorical.util.proposition_type import PROPOSITION_TYPES

def truth_list(topic, proposition_type=None, subject=None, predicate=None):
    """
    Get a list of true categorical relations from a topic

    Returns a list of the form

        [
            {
                'proposition_type': 'A',
                'subject': 'subject',
                'predicate': 'predicate',
            },
            ...
        ]

    :param str proposition_type: A categorical proposition type to match.
        May be 'A', 'E', 'I', or 'O'
    :param str subject: An optional subject key in the topic.
    :param str predicate: An optional predicate key in the topic.
        Cannot be the same as the subject key if both are provided.
    :return: A list of true categorical relations
    :rtype: list
    :raises: AssertionError for:
        Topic with reflexive categorical relations.
        Unknown proposition types.
        Subject or predicate key not in the topic.
        Non-None and identical subject and predicate keys.
    """
    assert proposition_type is None or proposition_type in PROPOSITION_TYPES, \
        f"Unrecognized proposition type {repr(proposition_type)}"
    assert subject is None or subject in topic, \
        f"Subject key {repr(subject)} not in topic"
    assert predicate is None or predicate in topic, \
        f"Predicate key {repr(predicate)} not in topic"
    assert subject is None or subject != predicate, \
        f"Identical subject and predicate keys {repr(subject)}"

    true_categorical_relations = []
    sub_keys = _match_list(subject, topic.keys())
    prop_types = _match_list(proposition_type, PROPOSITION_TYPES)
    for sub_key in sub_keys:
        sub_info = topic[sub_key]
        for prop_type in prop_types:
            pred_keys = _match_list(predicate, sub_info['truth'][prop_type])
            assert sub_key not in pred_keys, \
                f"Categorical relations should be irreflexive {repr(topic)}"
            true_categorical_relations.extend([
                {
                    'proposition_type': prop_type,
                    'subject': sub_key,
                    'predicate': pred_key
                }
                for pred_key in pred_keys
            ])
    return true_categorical_relations


def _match_list(to_match, the_list):
    """
    Get a subset of a list matching an item

    :param str to_match: The item to match.
        Can be `None`
    :param list the_list: The list to match on
    :return: The list items matching the item.
        If `to_match` is None, returns `the_list`
    :rtype: list
    """
    if to_match:
        match_list = [item for item in the_list if item == to_match]
    else:
        match_list = the_list
    return match_list
