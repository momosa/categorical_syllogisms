"""
proposition_transformer.py

contradictory(proposition)
conrary(proposition)
subalternate(proposition)
converse(proposition)
"""

from logic.categorical.proposition import CategoricalProposition
from logic.categorical.util.proposition_type import negate_proposition_type


def contradictory(proposition):
    """
    Get the contradictory of the proposition

    :param CategoricalProposition proposition:
        A categorical proposition
    :return: opposite proposition
    :rtype: CategoricalProposition
    :raises: Keyerror for unknown proposition type
    """
    contradictory_prop = CategoricalProposition(**{
        'topic_name': proposition.topic_name,
        'proposition_type': negate_proposition_type(proposition.proposition_type),
        'subject': proposition.subject,
        'predicate': proposition.predicate,
    })
    return contradictory_prop


def contrary(proposition):
    """
    Get the contrary/subcontrary of the proposition

    :param CategoricalProposition proposition:
        A categorical proposition
    :return: contrary proposition (A <-> E), (I <-> O)
    :rtype: CategoricalProposition
    :raises: Keyerror for unknown proposition type
    """
    contrary_prop_type = {
        'A': 'E',
        'E': 'A',
        'I': 'O',
        'O': 'I',
    }
    contrary_prop = CategoricalProposition(**{
        'topic_name': proposition.topic_name,
        'proposition_type': contrary_prop_type[proposition.proposition_type],
        'subject': proposition.subject,
        'predicate': proposition.predicate,
    })
    return contrary_prop


def subalternate(proposition):
    """
    Get the subalternate of the proposition

    :param CategoricalProposition proposition:
        A categorical proposition
    :return: subalternate proposition (A <-> I), (E <-> O)
    :rtype: CategoricalProposition
    :raises: Keyerror for unknown proposition type
    """
    subalternate_prop_type = {
        'A': 'I',
        'I': 'A',
        'E': 'O',
        'O': 'E',
    }
    subalternate_prop = CategoricalProposition(**{
        'topic_name': proposition.topic_name,
        'proposition_type': subalternate_prop_type[proposition.proposition_type],
        'subject': proposition.subject,
        'predicate': proposition.predicate,
    })
    return subalternate_prop


def converse(proposition):
    """
    Get the converse of the proposition

    :param CategoricalProposition proposition:
        A categorical proposition
    :return: converse proposition, swapping subject and predicate
    :rtype: CategoricalProposition
    :raises: Keyerror for unknown proposition type
    """
    converse_prop = CategoricalProposition(**{
        'topic_name': proposition.topic_name,
        'proposition_type': proposition.proposition_type,
        'subject': proposition.predicate,
        'predicate': proposition.subject,
    })
    return converse_prop
