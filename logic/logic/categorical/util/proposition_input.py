"""
determine_proposition_kwargs.py

determine_proposition_kwargs
"""

import random
from textwrap import dedent
from typing import Optional

from logic.util import get_randseed, randbool

from logic.categorical.topic import Topic
from logic.categorical.util.proposition_type import (
    negate_proposition_type,
    random_proposition_type,
)
from logic.categorical.util.truth_list import truth_list

def determine_proposition_kwargs( #pylint: disable=too-many-arguments
        topic_name: Optional[str] = None,
        proposition_type: Optional[str] = None,
        subject: Optional[str] = None,
        predicate: Optional[str] = None,
        truth_value: Optional[bool] = None,
        random_seed: Optional[int] = None,
    ):
    """
    Flesh out and validate `CategoricalProposition` input

    :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
    :param str proposition_type: The type of categorical proposition.
        Can be A, E, I, or O.
    :param str subject: The subject. Should be a plural noun.
    :param str predicate: The predicate. Should be a plural noun.
    :param bool truth_value: The truth value of the categorical proposition.
    :param str random_seed: Determines randomization.
        Use to reproduce randomly generated categorical propositions.
    :return: Kwargs specifying a `CategoricalProposition`
    :rtype: dict
    :raises: KeyError on unknown proposition type
    :raises: ValueError on inconsistent input data
    :raises: AssertionError if given a subject or predicate but no topic name
    """
    prop_kwargs = {}
    random_seed = random_seed or get_randseed()
    random.seed(random_seed)

    if topic_name is None:
        assert subject is None and predicate is None, \
            "Topic name required when providing a subject and predicate"

    topic_name = topic_name or Topic.random_topic_name(random_seed)
    topic = Topic(topic_name)

    if all((
            truth_value is None,
            proposition_type is None,
            subject is None,
            predicate is None,
        )):
        truth_value = randbool()
        proposition_type = random_proposition_type()

    if truth_value is not None:
        try:
            proposition_type, subject, predicate = _random_prop_elems_with_truth(
                topic,
                truth_value,
                proposition_type,
                subject,
                predicate,
            )
        except IndexError:
            raise ValueError(dedent(f"""
                CategoricalProposition input is inconsistent:
                topic_name={repr(topic_name)},
                truth_value={repr(truth_value)},
                proposition_type={repr(proposition_type)},
                subject={repr(subject)},
                predicate={repr(predicate)}
            """))

    else:
        proposition_type, subject, predicate = _random_prop_elems(
            topic,
            proposition_type,
            subject,
            predicate,
            random_seed,
        )

        if predicate in topic[subject]['truth'][proposition_type]:
            truth_value = True
        elif predicate in topic[subject]['truth'][negate_proposition_type(proposition_type)]:
            truth_value = False

    if subject == predicate:
        raise ValueError("Subject and predicate cannot be identical")

    prop_kwargs = {
        'topic_name': topic_name,
        'random_seed': random_seed,
        'truth_value': truth_value,
        'proposition_type': proposition_type,
        'subject': subject,
        'predicate': predicate,
    }

    return prop_kwargs


def _random_prop_elems_with_truth(topic, truth_value, proposition_type, subject, predicate):
    """
    Get random proposition elements for a true proposition satisfying the input
    """
    assert truth_value is not None, "Truth value required"

    if proposition_type is not None and truth_value is False:
        desired_prop_type = negate_proposition_type(proposition_type)
    else:
        desired_prop_type = proposition_type

    truth_candidates = truth_list(
        topic,
        desired_prop_type,
        subject,
        predicate,
    )
    truth_info = random.choice(truth_candidates)

    if proposition_type is None:
        proposition_type = truth_info['proposition_type'] \
            if truth_value is True \
            else negate_proposition_type(truth_info['proposition_type'])

    subject = truth_info['subject']
    predicate = truth_info['predicate']

    return proposition_type, subject, predicate


def _random_prop_elems(topic, proposition_type, subject, predicate, random_seed):
    """
    Get random proposition elements satisfying the input
    """
    proposition_type = proposition_type or random_proposition_type(random_seed)

    if not subject and not predicate:
        while subject == predicate:
            subject = random.choice(tuple(topic.keys()))
            predicate = random.choice(tuple(topic.keys()))

    elif predicate and not subject:
        subject = predicate
        while subject == predicate:
            subject = random.choice(tuple(topic.keys()))

    elif subject and not predicate:
        predicate = subject
        while subject == predicate:
            predicate = random.choice(tuple(topic.keys()))

    return proposition_type, subject, predicate
