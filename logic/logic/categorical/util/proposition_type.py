"""
proposition_transformer.py

PROPOSITION_TYPES
negate_proposition_type(proposition_type)
random_proposition_type()
"""

import random


PROPOSITION_TYPES = ('A', 'E', 'I', 'O')


def negate_proposition_type(proposition_type):
    """
    Negate a proposition type

    :param str proposition_type: A categorical proposition type.
        Can be 'A', 'E', 'I', or 'O'
    :return: The contradictory proposition type
    :rtype: str
    :raises: KeyError for unknown proposition type
    """
    negation_map = {
        'A': 'O',
        'E': 'I',
        'I': 'E',
        'O': 'A',
    }
    return negation_map[proposition_type]


def random_proposition_type(random_seed=None):
    """
    Get a random proposition type

    :param int random_seed: Determines randomization. Used for testing.
    :return: A random proposition type.
        Can be A, E, I or O.
    :rtype: str
    """
    if random_seed is not None:
        random.seed(random_seed)
    return random.choice(PROPOSITION_TYPES)
