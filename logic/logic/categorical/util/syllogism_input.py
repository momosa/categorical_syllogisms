"""
determine_proposition_kwargs.py

determine_proposition_kwargs
"""

import random
import itertools
from textwrap import dedent
from typing import Optional, Dict, Union

from logic.util import get_randseed, randbool

from logic.categorical.topic import Topic
from logic.categorical.proposition import CategoricalProposition

from logic.categorical.util.proposition_type import negate_proposition_type
from logic.categorical.util.form import Form
from logic.categorical.util.truth_list import truth_list


SOUNDNESS_PROB = 0.333


def determine_syllogism_kwargs( #pylint: disable=too-many-arguments
        random_seed: Optional[int] = None,
        topic_name: Optional[str] = None,
        propositions: Optional[Dict[str, Dict[str, Union[int, str, bool, None]]]] = None,
        categories: Optional[Dict[str, str]] = None,
        form: Optional[str] = None,
        is_valid: Optional[bool] = None,
        is_sound: Optional[bool] = None,
    ):
    """
    Flesh out and validate `CategoricalSyllogism` input

    If no input is given, a randomly specification will be given.
    For partial input, the remaining items will be determined randomly.
    If propositions are given, all are required.
    If categories are given, all are required.

    There are different ways of specifying a syllogism.
    For instance, one could specify the categories and form,
    which determines propositions and validity.
    Or one could specify propositions alone,
    determining the other attributes.

    Determination order:
    (1) Propositions: determine all else
    (2) Categories and (optionally) soundness/validity/form: determines propositions
    (3) Soundness, validity, and/or form:
        randomly generate categories and propositions within restrictions

    :param str random_seed: Determines randomization.
        Use to reproduce randomly generated categorical syllogisms.
    :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
    :param dict propositions: Propositions from which to construct the syllogism
    :param dict categories: Categories from which to construct the syllogism
    :param str form: Form of the categorical syllogism
    :param bool is_valid: Whether or not the syllogism is valid
    :param bool is_sound: Whether or not the syllogism is sound
    :return: Kwargs specifying a `CategoricalSyllogism`
    :rtype: dict
    :raises: ValueError on illegitimate input data
    """

    syl_kwargs = {}
    random_seed = random_seed or get_randseed()
    random.seed(random_seed)

    if topic_name is None:
        if categories and not propositions:
            raise ValueError(
                "If categories are specified and propositions aren't, topic_name is required."
            )
        props = propositions or {}
        topic_name = next(iter([
            props[prop_name]['topic_name']
            for prop_name in props
            if 'topic_name' in props[prop_name]
        ]), Topic.random_topic_name(random_seed))

    if propositions:
        form, categories, is_valid, is_sound = None, None, None, None

    elif categories:
        if form:
            Form.validate(form)
        else:
            if is_sound is True:
                is_valid = True
            form = Form.random(is_valid=is_valid)
        propositions = propositions_from_categories(
            form,
            categories,
        )

    else:
        form, is_valid, is_sound = get_form_validity_soundness(form, is_valid, is_sound)
        propositions = random_propositions(topic_name, is_sound, form)

    try:
        propositions['major_premise'].setdefault('topic_name', topic_name)
        propositions['minor_premise'].setdefault('topic_name', topic_name)
        propositions['conclusion'].setdefault('topic_name', topic_name)
    except KeyError:
        raise ValueError(dedent(f"""
            If propositions are specified, all are required. Given:
            {repr(propositions)}
        """))

    major_premise = CategoricalProposition(**propositions['major_premise'])
    minor_premise = CategoricalProposition(**propositions['minor_premise'])
    conclusion = CategoricalProposition(**propositions['conclusion'])

    form = form or Form.from_propositions(
        major_premise,
        minor_premise,
        conclusion,
    )

    categories = categories or categories_from_propositions(
        form,
        propositions,
    )

    is_valid = is_valid if is_valid is not None else Form.is_valid(form)

    is_sound = is_sound if is_sound is not None else soundness_from_validity_and_prems(
        is_valid,
        major_premise,
        minor_premise,
    )

    syl_kwargs = {
        'random_seed': random_seed,
        'topic_name': topic_name,
        'propositions': {
            'major_premise': major_premise,
            'minor_premise': minor_premise,
            'conclusion': conclusion,
        },
        'categories': categories,
        'form': form,
        'is_valid': is_valid,
        'is_sound': is_sound,
    }

    return syl_kwargs


def categories_from_propositions(form, propositions):
    """
    Get categories from the syllogism's propositions
    Assume the form has been validated
    """
    major_premise = propositions['major_premise']
    minor_premise = propositions['minor_premise']
    conclusion = propositions['conclusion']
    figure = int(form[-1])
    term_location = get_term_location(figure)

    major_term = major_premise[term_location['major_term']['major_premise']]
    minor_term = minor_premise[term_location['minor_term']['minor_premise']]
    middle_term = major_premise[term_location['middle_term']['major_premise']]
    other_middle_term = minor_premise[term_location['middle_term']['minor_premise']]

    if any((
            minor_term != conclusion['subject'],
            major_term != conclusion['predicate'],
            middle_term != other_middle_term,
            major_term == minor_term,
            major_term == middle_term,
            minor_term == middle_term,
        )):
        raise ValueError(dedent(f"""
            Input does not represent a legitimate categorical syllogism.
            Major premise:
            {repr(major_premise)}
            Minor premise:
            {repr(minor_premise)}
            Conclusion:
            {repr(conclusion)}
        """))

    categories = {
        'major_term': major_term,
        'minor_term': minor_term,
        'middle_term': middle_term,
    }
    return categories


def propositions_from_categories(form, categories):
    """
    Determine the propositions given the form and categories

    :param str form: The form of a categorical syllogism, like 'AEI-1'
    :param
    """
    major_premise_type = form[0]
    minor_premise_type = form[1]
    conclusion_type = form[2]
    figure = int(form[-1])
    term_location = get_term_location(figure)
    propositions = {}

    try:
        propositions['major_premise'] = {
            'proposition_type': major_premise_type,
            term_location['middle_term']['major_premise']: categories['middle_term'],
            term_location['major_term']['major_premise']: categories['major_term'],
        }
        propositions['minor_premise'] = {
            'proposition_type': minor_premise_type,
            term_location['middle_term']['minor_premise']: categories['middle_term'],
            term_location['minor_term']['minor_premise']: categories['minor_term'],
        }

    except KeyError:
        raise ValueError(dedent(f"""
            Missing categories. Provide 'major_term', 'minor_term', and 'middle_term'.
            Given:
            {repr(categories)}
        """))

    propositions['conclusion'] = {
        'proposition_type': conclusion_type,
        'subject': categories['minor_term'],
        'predicate': categories['major_term'],
    }

    return propositions


def soundness_from_validity_and_prems(is_valid, major_premise, minor_premise):
    """
    Determine the soundness of the syllogism

    We can't just take in kwargs for the props here because
    we need the truth value.

    :param bool is_valid: Whether or not the syllogism is valid
    :param CategoricalProposition major_premise: The major premise
    :param CategoricalProposition minor_premise: The minor premise
    :return: Whether or not the syllogism is sound
    :rtype: bool
    """
    is_sound = None

    if all((
            is_valid is True,
            major_premise.truth_value is True,
            minor_premise.truth_value is True,
        )):
        is_sound = True

    elif any((
            is_valid is False,
            major_premise.truth_value is False,
            minor_premise.truth_value is False,
        )):
        is_sound = False

    return is_sound


def random_propositions(topic_name, is_sound, form):
    """
    Get random propositions for a syllogism given soundness and form

    :param str topic_name: The name of the topic
    :param bool is_sound: Whether or not the syllogism is sound
    :bool str form: The form of the categorical syllogism
    :return: kwargs for the propositions in the syllogism
    :rtype: dict
    """
    major_premise, minor_premise, conclusion = None, None, None
    prop_types = {
        'major': form[0],
        'minor': form[1],
        'conc': form[2],
    }
    figure = int(form[-1])
    term_location = get_term_location(figure)
    topic = Topic(topic_name)

    if is_sound:
        possible_truth_values = [(True, True)]
    else:
        possible_truth_values = list(itertools.product(
            (True, False),
            (True, False),
        ))
        random.shuffle(possible_truth_values)
        if Form.is_valid(form):
            possible_truth_values.remove((True, True))

    while possible_truth_values and major_premise is None:
        truth_values = possible_truth_values.pop()
        major_premise, minor_premise = prems_given_truth_and_type(
            topic=topic,
            prop_truth={
                'major': truth_values[0],
                'minor': truth_values[1],
            },
            prop_types=prop_types,
            term_location=term_location,
        )

    if major_premise is not None and minor_premise is not None:
        conclusion = {
            'proposition_type': prop_types['conc'],
            'subject': minor_premise[term_location['minor_term']['minor_premise']],
            'predicate': major_premise[term_location['major_term']['major_premise']],
        }
    else:
        raise ValueError(dedent(f"""
            Unable to designate a syllogism.
            topic_name={repr(topic_name)}
            is_sound={repr(is_sound)}
            form={repr(form)}
        """))

    return {
        'major_premise': major_premise,
        'minor_premise': minor_premise,
        'conclusion': conclusion,
    }


def get_term_location(figure):
    """
    Get the location of each term given the figure

    :param int figure: Figure of the categorical syllogism
    :return: Locations of the middle, major, and minor terms in the premises
    :rtype: dict
    :raises: ValueError for illegitimate figure
    """
    if figure not in (1, 2, 3, 4):
        raise ValueError(f"Illegitimate figure {repr(figure)}")

    term_location = {
        'major_term': {},
        'minor_term': {},
        'middle_term': {},
    }

    if figure in [1, 3]:
        term_location['middle_term']['major_premise'] = 'subject'
        term_location['major_term']['major_premise'] = 'predicate'
    else:
        term_location['major_term']['major_premise'] = 'subject'
        term_location['middle_term']['major_premise'] = 'predicate'

    if figure in [3, 4]:
        term_location['middle_term']['minor_premise'] = 'subject'
        term_location['minor_term']['minor_premise'] = 'predicate'
    else:
        term_location['minor_term']['minor_premise'] = 'subject'
        term_location['middle_term']['minor_premise'] = 'predicate'

    return term_location


def prems_given_truth_and_type(topic, prop_truth, prop_types, term_location):
    """
    Try creating premises with the given specs.

    :param Topic topic: The topic from which to assess truth value
    :param dict prop_truth: Truth values for the premises
    :param dict term_location: Locations of middle, major, and minor terms
    :return: The major and minor premises,
        or None if unable to satisfy constraints
    :rtype: tuple
    """
    major_premise, minor_premise = None, None

    desired_type = lambda prop_name: prop_types[prop_name] \
        if prop_truth[prop_name] is True \
        else negate_proposition_type(prop_types[prop_name])
    desired_types = {
        'major': desired_type('major'),
        'minor': desired_type('minor'),
    }

    major_candidates = truth_list(topic, desired_types['major'])
    random.shuffle(major_candidates)

    for major_candidate in major_candidates:
        minor_candidates = truth_list(**{
            'topic': topic,
            'proposition_type': desired_types['minor'],
            term_location['middle_term']['minor_premise']: \
                major_candidate[term_location['middle_term']['major_premise']],
        })
        major_term_is_minor_term = next(iter([
            item for item in minor_candidates
            if item[term_location['minor_term']['minor_premise']] == \
                major_candidate[term_location['major_term']['major_premise']]
        ]), None)
        if major_term_is_minor_term:
            minor_candidates.remove(major_term_is_minor_term)
        if minor_candidates:
            major_premise = {
                'proposition_type': prop_types['major'],
                'subject': major_candidate['subject'],
                'predicate': major_candidate['predicate'],
            }
            minor_premise = {
                'proposition_type': prop_types['minor'],
                'subject': minor_candidates[0]['subject'],
                'predicate': minor_candidates[0]['predicate'],
            }
            break

    return major_premise, minor_premise


def get_form_validity_soundness(form, is_valid, is_sound):
    """
    Determine the form, validity, and soundness given no propositions or categories

    :param str form: Categorical form, like 'AEI-1'.
        Can be None
    :param bool is_valid: Whether or not the categorical syllogism should be valid.
        Can be None
    :param bool is_sound: Whether or not the categorical syllogism should be sound.
        Can be None
    :return: Categorical form, validity, and soundness
    :rtype: tuple
    :raises: ValueError
    """
    if form is not None:
        is_valid = Form.is_valid(form)
        if is_sound is None:
            is_sound = False if is_valid is False else randbool()
        else:
            if is_sound is True and is_valid is False:
                raise ValueError(dedent(f"""
                    Form {repr(form)} conflicts with soundness {repr(is_sound)}.
                """))
    elif is_valid is True:
        is_sound = is_sound if is_sound is not None else randbool()
    elif is_valid is False:
        is_sound = False
    else:
        is_sound = is_sound if is_sound is not None else random.random() <= SOUNDNESS_PROB
        is_valid = True if is_sound is True else randbool()

    form = form or Form.random(is_valid=is_valid)

    return form, is_valid, is_sound
