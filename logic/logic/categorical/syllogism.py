"""
syllogism.py

CategoricalSyllogism
"""

from dataclasses import dataclass
from typing import Union, Dict

from logic.categorical.util.syllogism_input import determine_syllogism_kwargs
from logic.categorical.proposition import CategoricalProposition


@dataclass
class CategoricalSyllogism:
    """
    Create a categorical syllogism

    If no input is given, a randomly specification will be given.
    For partial input, the remaining items will be determined randomly.
    If propositions are given, all are required.
    If categories are given, all are required.

    There are different ways of specifying a syllogism.
    For instance, one could specify the categories and form,
    which determines propositions and validity.
    Or one could specify propositions alone,
    determining the other attributes.

    Determination order:
    (1) Propositions: determine all else
    (2) Categories and (optionally) soundness/validity/form: determines propositions
    (3) Soundness, validity, and/or form:
        randomly generate categories and propositions within restrictions

    :param str random_seed: Determines randomization.
        Use to reproduce randomly generated categorical syllogisms.
    :param str topic_name: Name of the topic, like `'fruits_and_vegetables'`
    :param dict propositions: Propositions from which to construct the syllogism
    :param dict categories: Categories from which to construct the syllogism
    :param str form: Form of the categorical syllogism
    :param bool is_valid: Whether or not the syllogism is valid
    :param bool is_sound: Whether or not the syllogism is sound
    :return: Kwargs specifying a `CategoricalSyllogism`
    :rtype: dict
    :raises: ValueError on illegitimate input data
    """

    random_seed: int
    topic_name: str
    propositions: Dict[str, CategoricalProposition]
    categories: Dict[str, str]
    form: str
    is_valid: Union[bool, None]
    is_sound: Union[bool, None]


    def __init__(self, **kwargs):
        filled_kwargs = determine_syllogism_kwargs(**kwargs)
        for attr_name, attr_value in filled_kwargs.items():
            setattr(self, attr_name, attr_value)
