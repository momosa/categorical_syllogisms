"""
logic.categorical

Tools for categorical logic
"""

from logic.categorical.proposition import CategoricalProposition
from logic.categorical.topic import Topic
