"""
logic

Python library for logic
"""

from setuptools import setup, find_packages
setup(
    name='logic',
    packages=find_packages(),
    description="Python library for logic",
    install_requires=[
        'enum34==1.1.6',
    ]
)
