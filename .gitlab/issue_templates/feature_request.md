## Overview

As a ROLE, I want to TASK so I can GOAL.

----

## Description and rationale

Describe the feature you'd like implemented.
What does it do?

Why should the feature be implemented?

----

## Implementation

Do you have any ideas about how the feature should be implemented?
Please provide details and/or sketches.
Ideally, make it straightforward for anyone to take on.

----

## Labeling

If you're not a maintainer,
please add the `triage` and `type: feature` labels.

Maintainers will add a priority, a difficulty,
and one or more relevant domains.
This will make it easier for others to pick up issues
matching their interests and skill levels.
Maintainers will also include issues in project milestones.
