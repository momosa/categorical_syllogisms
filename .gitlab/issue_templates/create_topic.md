## Overview

As a user, I want to get a wide range of exercises and other data
so I can practice or create worksheets.

----

## Description and rationale

We want our data to be quite varied.
For example, I want to be able to do a lot of exercises
with a variety of terms and forms.
For that to happen, we'll need a wide range of concise topics.

----

## Implementation

### Criteria for new topics

1. The topic name (the `name` in `name.json` of the topic file)
contains only lower-case letters and `_`,
starts and ends with a letter,
and is no more than 30 characters long.
1. 10 nouns
1. No adjectives or verbs
1. Term keys should be the plural form for nouns,
singular form for verbs, and the word for adjectives.
1. The terms are unified, related to one another in a way that's well-defined.
1. All pairs make sense.
(Using any two terms in a categorical sentence yields a sentence that makes sense,
although it need not be true.)
1. At least one of each proposition type (A, E, I, O) is true.
1. The topic can yield a sound argument of any form.
1. Avoid stuff terms (sugar, rain, shale, ...)
1. Terms should be at the middle school or high school level,
and should not require special knowledge.
1. The topic is appropriate for an educational setting:
no cursing, bigotry, etc.
1. Only the singular form of each noun is specified
unless the plural uses an unusual transformation.
In the latter case, both the singular and plural are specified.
(We can handle things like
bear -> bears, bush -> bushes, and berry -> berries,
but not deer -> deer or octopus -> octopi.)

Some of these criteria are validated automatically.
You can check yourself with:

```shell
bin/run.sh topic_tests
```

Other criteria, like choosing appropriate terms,
have to be checked manually.

### Files to create

Create your new topic here:

```shell
logic/databases/category_topics/<new_topic>.json
```

### Adjusting tests

Add the new topic to the expected response for the topic list API test.
Otherwise, the API tests will fail.

```shell
databases/api_tests/topic/list.json
```

### Manual validation

The CI pipeline with catch these issues automatically,
but you may want to do some manual validation
along the way.

Ensure the json is legit:

```shell
# Run from the project dir
python -m json.tool logic/databases/category_topics/<new_topic>.json
```

Ensure automatic topic validation passes:

```shell
# Run from the project dir
bin/test.sh topic
```

Make sure we can hit the topic in the API:

```shell
# Run from the project dir
bin/build.sh dev
bin/run.sh dev_server

curl \
    --request GET \
    'http://localhost:8888/api/categorical/syllogism/natural' \
    --data '{"topic_name": "<new_topic>"}'

curl \
    --request GET \
    'http://localhost:8888/categorical/topic/<new_topic>' 
```

----

## Labeling

If you're not a maintainer,
please add the `triage` and `type: feature` labels.

Maintainers will add a priority, a difficulty,
and one or more relevant domains.
This will make it easier for others to pick up issues
matching their interests and skill levels.
Maintainers will also include issues in project milestones.
