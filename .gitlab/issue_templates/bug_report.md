## Overview

Please include a brief (up to 20-word) description of the bug.

----

## Description

Describe the bug in more detail.
Where relevant, please include screen shots,
tracebacks, error codes, etc.

----

## Reproducing the bug

1. Do this thing
1. Do the other thing
1. Observe this failure

----

## Fixing the bug

Do you have any ideas about what's causing the bug or
how it should be fixed?
Please provide details and/or sketches.
Ideally, make it straightforward for anyone to take on.

----

## Labeling

If you're not a maintainer,
please add the `triage` and `type: bug` labels.

Maintainers will add a priority, a difficulty,
and one or more relevant domains.
This will make it easier for others to pick up issues
matching their interests and skill levels.
Maintainers will also include issues in project milestones.
