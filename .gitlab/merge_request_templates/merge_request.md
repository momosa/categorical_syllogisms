## Overview

As a ROLE I want to TASK so I can GOAL.

----

## Summary of changes

- Did the thing.
- Did another thing.

Include screenshots or videos if appropriate.

----

## Discussion points

For such-and-such,
we could do this or that.
I went with this because reasons.
Feedback welcome.

----

## Testing

Tests will be run as part of the CI for each commit,
although it's faster to run them locally.
To run tests locally, set up your dev environment
as described in the `README.md`.
Then you can run individual test suites as follows:

```shell
$CATEGORICAL_SYLLOGISMS/bin/test.sh docker

$CATEGORICAL_SYLLOGISMS/bin/test.sh unit_tests

$CATEGORICAL_SYLLOGISMS/bin/test.sh lint_tests

$CATEGORICAL_SYLLOGISMS/bin/run.sh api_tests

$CATEGORICAL_SYLLOGISMS/bin/run.sh apispec_validate
```
