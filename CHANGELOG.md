# Changelog

* **0.9** Basic UI (in progress)
  - Add static assets: testing, building, serving
* **0.8** Semantics in the API
  - Fix a bug causing API test data not to be checked properly
  - Remove the category API
  - Simplify categories as single strings
  - Add truth value semantics to categorical propositions and syllogisms
  - Add topic validation to the test script and CI
  - Add full spec API test coverage for all endpoints
  - Streamline class params to match the API
  - Add an exercise for assessing validity and soundness
* **0.7** Security, delivery, and risk minimization upgrades
  - Impose rate-limiting in production (blocks simple DOS attacks)
  - Use nginx in front of gunicorn/django in production
  - Use gunicorn for the production server
  - Run API tests against the production server
  - Enforce 100% unit test coverage
  - Eliminate environment variable requirements
  - Remove the generated apispec from the repo
  - Use GitLab CI pipeline for all tests
  - Add smoke tests for docker images
  - Upgrade to Python 3.7
* **0.6** More API
  - API improvements
  - Exercises API
  - API documentation
* **0.5** API first pass
  - API specs
  - GET endpoints for syllogism components
  - Server containers
* **0.4** Testing
  - Unit tests
  - Lint tests
* **0.3** Moved to Python
* **0.2** Added basic functionality for categorical syllogisms
(canonical and pretty), including validity.
