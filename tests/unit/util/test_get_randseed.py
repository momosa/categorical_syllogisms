"""
test_get_randseed.py
"""

from test_logic.util import TestUnit

from logic.util.get_randseed import get_randseed, MAX_SEED


class TestGetRandseed(TestUnit):
    """
    Tests for get_randseed
    """


    def test_get_randseed(self):
        """
        Test that get_randseed returns a value within the designated range
        """
        random_seed = get_randseed()
        self.assertGreaterEqual(random_seed, 1)
        self.assertLessEqual(random_seed, MAX_SEED)
