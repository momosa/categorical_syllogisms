"""
test_database.py

Test database/db.py
"""

import copy
from textwrap import dedent
from test_logic.util import TestUnit

from logic.util.database import Database


def mock_get_saved_dbs(db_dir):
    """
    Mocks walking the db dir
    """
    root = [
        '/root/',
        ['sub_db'],
        ['root_db1.json', 'root_db2.json'],
    ]
    sub_db = [
        '/root/sub_db/',
        ['subsub_db'],
        ['sub_db1.json', 'sub_db2.json'],
    ]
    subsub_db = [
        '/root/sub_db/subsub_db/',
        [],
        ['subsub_db1.json', 'subsub_db2.json'],
    ]
    if db_dir.endswith('subsub_db'):
        walk = [subsub_db]
    elif db_dir.endswith('sub_db'):
        walk = [sub_db, subsub_db]
    else:
        walk = [root, sub_db, subsub_db]
    return walk


class TestDatabase(TestUnit):
    """
    Tests for Database
    """

    def setUp(self):
        self.setup_mocks(
            namespace='logic.util.database',
            to_mock=(
                '_get_db_from_file',
                '_get_saved_dbs',
                '_write_database',
            ),
        )

        self.mock['_get_db_from_file'].return_value = {'foo': 'bar'}
        self.mock['_get_saved_dbs'].side_effect = mock_get_saved_dbs


class TestRepr(TestDatabase):
    """
    Test the representation
    """


    def setUp(self):
        super(TestRepr, self).setUp()
        self.test_db = Database(db_name='test_db')


    def tearDown(self):
        super(TestRepr, self).tearDown()
        del self.test_db


    def test_repr(self):
        """
        Test that the repr is as expected
        """
        actual_repr = repr(self.test_db)
        expected_repr = "Database(db_name='test_db')"
        self.assertEqual(actual_repr, expected_repr)


class TestIsLoaded(TestDatabase):
    """
    Test Database.is_loaded
    """


    def setUp(self):
        super(TestIsLoaded, self).setUp()
        self.test_db = Database(db_name='test_db')


    def tearDown(self):
        super(TestIsLoaded, self).tearDown()
        del self.test_db


    def test_is_loaded(self):
        """
        Test that a loaded database is recognized as loaded
        """
        self.assertTrue(
            Database.is_loaded('test_db'),
            "Expected 'test_db' to loaded, but it was not.",
        )


    def test_is_not_loaded(self):
        """
        Test that a non-loaded database is not recognized as loaded
        """
        self.assertFalse(
            Database.is_loaded('not_there'),
            "Expected 'not_there' to not be loaded, but it was.",
        )


class TestIsAvailable(TestDatabase):
    """
    Test Database.get_available
    """


    def test_root_available(self):
        """
        Test databases available in the root dir
        """
        root_available = Database.get_available()
        expected = ['root_db1', 'root_db2']
        self.assertEqual(
            root_available,
            expected,
            dedent(f"""
                Root available databases were {root_available},
                but should have been {expected}.
            """),
        )


    def test_subdir_available(self):
        """
        Test databases available in subdirs
        """
        sub_available = Database.get_available('sub_db')
        expected = [
            'sub_db1',
            'sub_db2',
            'subsub_db/subsub_db1',
            'subsub_db/subsub_db2',
        ]
        self.assertEqual(
            sub_available,
            expected,
            dedent(f"""
                Subdir available databases were {sub_available},
                but should have beend {expected}
            """),
        )


class TestDBNameAttribute(TestDatabase):
    """
    Test setting and accessing the database name
    """


    def test_db_name_attr(self):
        """
        Test getting the name of a database
        """
        test_db = Database(db_name='test_db')
        actual_name = test_db.db_name
        expected_name = 'test_db'
        self.assertEqual(
            actual_name,
            expected_name,
            dedent(f"""
                Database(db_name='test_db').db_name
                yields '{actual_name}'
                but should yield '{expected_name}'
            """),
        )


class TestDictOps(TestDatabase):
    """
    Test database dict operations
    """


    def setUp(self):
        super(TestDictOps, self).setUp()
        self.test_db = Database(db_name='test_db')


    def tearDown(self):
        super(TestDictOps, self).tearDown()
        del self.test_db


    def test_keys(self):
        """
        Test getting the list of database keys
        """
        actual = self.test_db.keys()
        expected = {'foo': 'bar'}.keys()
        self.assertEqual(
            actual,
            expected,
            f"Keys were '{actual}' but should have been '{expected}'",
        )


    def test_items(self):
        """
        Test getting the database's items
        """
        actual_items = self.test_db.items()
        expected_items = {'foo': 'bar'}.items()
        self.assertEqual(actual_items, expected_items)


    def test_values(self):
        """
        Test getting the database's values
        """
        actual_values = list(self.test_db.values())
        expected_values = list({'foo': 'bar'}.values())
        self.assertEqual(actual_values, expected_values)


    def test_iter(self):
        """
        Test iterating over the database
        """
        self.assertIn('foo', self.test_db)
        actual_keys = [key for key in self.test_db]
        expected_keys = ['foo']
        self.assertEqual(
            actual_keys,
            expected_keys,
            f"Failed to iterate over the database like a dict",
        )


    def test_get_item(self):
        """
        Testing getting a database item with []
        """
        actual = self.test_db['foo']
        expected = 'bar'
        self.assertEqual(
            actual,
            expected,
            f"db['foo'] is '{actual}', but should have been '{expected}'",
        )


    def test_get_item_key_error(self):
        """
        Test trying to get a non-existent DB item with [] yields KeyError
        """
        with self.assertRaises(KeyError):
            dummy = self.test_db['not_there']


    def test_get(self):
        """
        Test Database with .get
        """

        self.assertEqual(
            self.test_db.get('foo'),
            'bar',
            "Expected db.get('foo') == 'bar'",
        )

        self.assertIsNone(
            self.test_db.get('not_there'),
            "Expected db.get('foo') == None",
        )

        self.assertEqual(
            self.test_db.get('not_there', 'default_str'),
            'default_str',
            "Expected db.get('foo', default_str) == 'default_str'",
        )


    def test_set_item(self):
        """
        Test setting a database item with []
        """

        self.test_db['new_key'] = 'new_value'
        self.assertEqual(
            self.test_db['new_key'],
            'new_value',
            "Failed to set a new value to a new key",
        )

        self.test_db['foo'] = 'new_bar'
        self.assertEqual(
            self.test_db['foo'],
            'new_bar',
            "Failed to set a new value to an existing key",
        )


    def test_del(self):
        """
        Test deleting an item from a database
        """
        del self.test_db['foo']
        self.assertIsNone(
            self.test_db.get('foo'),
            "Failed to delete key 'foo' from the database",
        )


class TestReadOnly(TestDatabase):
    """
    Test read-only property of databases
    """


    def setUp(self):
        super(TestReadOnly, self).setUp()
        self.read_only_db = Database(db_name='read_only_db', read_only=True)


    def tearDown(self):
        super(TestReadOnly, self).tearDown()
        del self.read_only_db


    def test_del_raises_err(self):
        """
        Test that deleting an item from a read-only DB raises an error
        """
        with self.assertRaises(AttributeError):
            del self.read_only_db['foo']


    def test_reassignment_raises_err(self):
        """
        Test that reassigning an item from a read-only DB raises an error
        """
        with self.assertRaises(AttributeError):
            self.read_only_db['foo'] = 'New value for foo'


    def test_assignment_raises_err(self):
        """
        Test that assigning a new item to a read-only DB raises an error
        """
        with self.assertRaises(AttributeError):
            self.read_only_db['not_there'] = 'Learn stuff and be awesome'


class TestSave(TestDatabase):
    """
    Test db.save
    """


    def setUp(self):
        super(TestSave, self).setUp()
        self.test_db = Database(db_name='test_db')


    def tearDown(self):
        super(TestSave, self).tearDown()
        del self.test_db


    def test_save_db_multiplicity(self):
        """
        Test that save retains DB multiplicity
        """

        self.test_db.save()
        actual_n_using_db = Database._n_using_db.get('test_db')
        expected_n_using_db = 1
        self.assertEqual(
            actual_n_using_db,
            expected_n_using_db,
            dedent(f"""
                Saving made it look like {actual_n_using_db}
                instances are using the 'test_db' database
                instead of {expected_n_using_db}
            """),
        )

        self.test_db.save('new_test_db')
        actual_n_using_db = Database._n_using_db.get('new_test_db')
        expected_n_using_db = 1
        self.assertEqual(
            actual_n_using_db,
            expected_n_using_db,
            dedent(f"""
                Saving made it look like {actual_n_using_db}
                instances are using the 'new_test_db' database
                instead of {expected_n_using_db}
            """),
        )


    def test_save_should_adjust_name(self):
        """
        Test that saving changes the active database name
        """
        self.test_db.save('new_test_db')
        actual_name = self.test_db.db_name
        expected_name = 'new_test_db'
        self.assertEqual(
            actual_name,
            expected_name,
            dedent(f"""
                Saving failed to set new DB name.
                Got '{actual_name}' but expected '{expected_name}'
            """),
        )


    def test_save_data_sharing(self):
        """
        Test that save respects data sharing
        """
        original_db = Database(db_name='test_db')
        new_test_db = Database(db_name='test_db')
        new_test_db.save('new_test_db')
        another_new_test_db = Database(db_name='new_test_db')
        new_test_db['new_key'] = 'new_val'

        actual_inserted_in_original = original_db.get('new_key')
        self.assertIsNone(
            actual_inserted_in_original,
            dedent(f"""
                The original database should not have a
                value for 'new_key', but it has '{actual_inserted_in_original}'
            """),
        )

        actual_inserted_in_another = another_new_test_db.get('new_key')
        expected_inserted_in_another = 'new_val'
        self.assertEqual(
            actual_inserted_in_another,
            expected_inserted_in_another,
            dedent(f"""
                The other 'new_test_db' has '{actual_inserted_in_another}'
                for 'new_key' but it should have '{expected_inserted_in_another}'
            """),
        )


    def test_error_saving_read_only(self):
        """
        Test that trying to save a read-only database yields an AttributeError
        """
        read_only_db = Database(db_name='test_db', read_only=True)
        with self.assertRaises(AttributeError):
            read_only_db.save()


class TestAsDict(TestDatabase):
    """
    Test db.as_dict
    """

    def setUp(self):
        super(TestAsDict, self).setUp()
        self.test_db = Database(db_name='test_db')


    def tearDown(self):
        super(TestAsDict, self).tearDown()
        del self.test_db


    def test_as_dict(self):
        """
        Test getting a dictionary of a database
        """
        actual = self.test_db.as_dict()
        expected = {'foo': 'bar'}
        self.assertDictEqual(
            actual,
            expected,
            "Failed to get the dictionary of a database",
        )

class TestDeepCopy(TestDatabase):
    """
    Test deepcopy database
    """

    def test_deepcopy(self):
        """
        Test deepcopy database
        """
        test_db = Database(db_name='test_db')
        self.assertEqual(Database._n_using_db['test_db'], 1)
        dummy = copy.deepcopy(test_db)
        self.assertEqual(Database._n_using_db['test_db'], 2)
