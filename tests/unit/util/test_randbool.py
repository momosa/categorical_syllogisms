"""
test_randbool.py
"""

from test_logic.util import TestUnit

from logic.util.randbool import randbool


class TestRandbool(TestUnit):
    """
    Tests for randbool
    """


    def test_true(self):
        """
        Test that randbool returns true with the right seed
        """
        self.assertTrue(randbool(random_seed=2))


    def test_false(self):
        """
        Test that randbool returns false with the right seed
        """
        self.assertFalse(randbool(random_seed=1))
