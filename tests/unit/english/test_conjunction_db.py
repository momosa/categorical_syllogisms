"""
test_conjunction_db.py

Unit tests for sentence/conjunction_db.py
"""

from test_logic.util import TestUnit

from logic.english.conjunction_db import ConjunctionDB


FAKE_DATABASE = {
    'mid_sentence_conjunction': ['and'],
    'sentence_start_conjunction': ['moreover'],
}


class TestConjunctionDB(TestUnit):
    """
    Tests for the ConjunctionDB
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.english.conjunction_db',
            to_mock=(
                'Database.is_loaded',
                'Database',
                'random.choice',
            ),
        )

        self.mock['random.choice'].side_effect = lambda array: array[0]
        self.mock['Database'].return_value = FAKE_DATABASE


    def test_get_middle_conjunction(self):
        """
        Test getting a mid-sentence conjunction
        """
        actual = ConjunctionDB().get_middle_conjunction()
        expected = 'and'
        self.assertEqual(
            actual,
            expected,
            f"get_middle_conjunction yields '{actual}' but should yield '{expected}'",
        )


    def test_get_start_conjunction(self):
        """
        Test getting a conjunction to start a sentence
        """
        actual = ConjunctionDB().get_start_conjunction()
        expected = 'moreover'
        self.assertEqual(
            actual,
            expected,
            f"get_start_conjunction yields '{actual}' but should yield '{expected}'",
        )
