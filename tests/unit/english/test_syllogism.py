"""
test_syllogism.py

Unit tests for logic.english.syllogism
"""

from test_logic.util import TestUnit

from logic.english.syllogism import (
    canonical_syllogism,
    natural_syllogism,
)


class TestSyllogism(TestUnit):
    """
    Base class for testing English syllogisms
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.english.syllogism',
            to_mock=(
                'randbool',
                'random.choice',
                'random.seed',
                'Sentence.punctuate',
                'Sentence.as_canonical_conclusion',
                'Sentence.as_natural_conclusion',
                'Sentence.conjoin',
                'Sentence.with_middle_prem_indicator',
            ),
        )
        self.mock['random.choice'].side_effect = lambda arr: arr[0]
        self.mock['Sentence.punctuate'].side_effect = \
            lambda string: f'Punctuated {string}.'
        self.mock['Sentence.as_canonical_conclusion'].side_effect = \
            lambda string: f'Canonical conclusion: {string}.'
        self.mock['Sentence.as_natural_conclusion'].side_effect = \
            lambda string: f'Natural conclusion: {string}.'
        self.mock['Sentence.conjoin'].side_effect = \
            lambda lhs, rhs: f'{lhs} and {rhs}'
        self.mock['Sentence.with_middle_prem_indicator'].side_effect = \
            lambda conc, prem: f'{conc} because {prem}'


class TestCanonicalSyllogism(TestSyllogism):
    """
    Test getting the canonical form of a syllogism
    """

    def test_canonical_syllogism(self):
        """
        Test getting the canonical form: <prem1>. <prem2>. Therefore, <conc>.
        """
        actual = canonical_syllogism('prem1', 'prem2', 'conc')
        expected = (
            f'Punctuated prem1. '
            f'Punctuated prem2. '
            f'Canonical conclusion: conc.'
        )
        self.assertEqual(actual, expected)


class TestNaturalSyllogism(TestSyllogism):
    """
    Test getting the natural form of a syllogism
    """

    def test_separated_form(self):
        """
        Test getting the natural form: <prem1>. <prem2>. So, <conc>.
        """
        self.mock['randbool'].return_value = False
        self.mock['random.choice'].side_effect = lambda arr: arr[0]
        actual = natural_syllogism('prem1', 'prem2', 'conc')
        expected = (
            'Punctuated prem1. '
            'Punctuated prem2. '
            'Natural conclusion: conc.'
        )
        self.assertEqual(actual, expected)


    def test_left_conjoined_prems(self):
        """
        Test getting the natural form: <prem1> & <prem2>. So, <conc>.
        """
        self.mock['randbool'].return_value = False
        self.mock['random.choice'].side_effect = lambda arr: arr[1]
        actual = natural_syllogism('prem1', 'prem2', 'conc')
        expected = (
            'Punctuated prem1 and prem2. '
            'Natural conclusion: conc.'
        )
        self.assertEqual(actual, expected)


    def test_right_conjoined_prems(self):
        """
        Test getting the natural form: <conc> bc (<prem1> & <prem2>).
        """
        self.mock['randbool'].return_value = False
        self.mock['random.choice'].side_effect = lambda arr: arr[2]
        actual = natural_syllogism('prem1', 'prem2', 'conc')
        expected = 'Punctuated conc because prem1 and prem2.'
        self.assertEqual(actual, expected)


    def test_conjoined_left_subarg(self):
        """
        Test getting the natural form: (<conc> bc <prem1>) & <prem2>.
        """
        self.mock['randbool'].return_value = False
        self.mock['random.choice'].side_effect = lambda arr: arr[3]
        actual = natural_syllogism('prem1', 'prem2', 'conc')
        expected = 'Punctuated conc because prem1 and prem2.'
        self.assertEqual(actual, expected)


    def test_left_subarg(self):
        """
        Test getting the natural form: <prem1>. So <conc> bc <prem2>.
        """
        self.mock['randbool'].return_value = False
        self.mock['random.choice'].side_effect = lambda arr: arr[4]
        actual = natural_syllogism('prem1', 'prem2', 'conc')
        expected = 'Punctuated prem1. Natural conclusion: conc because prem2.'
        self.assertEqual(actual, expected)


    def test_switching_prems(self):
        """
        Test switching the premises
        """
        self.mock['randbool'].return_value = True
        self.mock['random.choice'].side_effect = lambda arr: arr[0]
        actual = natural_syllogism('prem1', 'prem2', 'conc')
        expected = (
            'Punctuated prem2. '
            'Punctuated prem1. '
            'Natural conclusion: conc.'
        )
        self.assertEqual(actual, expected)


    def test_setting_randseed(self):
        """
        Test setting a random seed
        """
        _ = natural_syllogism('prem1', 'prem2', 'conc', random_seed=7)
        self.mock['random.seed'].assert_called_with(7)
