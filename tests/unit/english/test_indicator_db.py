"""
test_conjunction_db.py

Unit tests for sentence/conjunction_db.py
"""

from test_logic.util import TestUnit

from logic.english.indicator_db import IndicatorDB


FAKE_DATABASE = {
    'premise': ['because'],
    'conclusion': ['therefore'],
}


class TestIndicatorDB(TestUnit):
    """
    Tests for the IndicatorDB
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.english.indicator_db',
            to_mock=(
                'Database.is_loaded',
                'Database',
                'random.choice',
            ),
        )

        self.mock['random.choice'].side_effect = lambda array: array[0]
        self.mock['Database'].return_value = FAKE_DATABASE


    def test_get_premise_indicator(self):
        """
        Test getting a premise indicator
        """
        actual = IndicatorDB().get_premise_indicator()
        expected = 'because'
        self.assertEqual(
            actual,
            expected,
            f"get_premise_indicator yields '{actual}' but should yield '{expected}'",
        )


    def test_get_conclusion_indicator(self):
        """
        Test getting a conclusion indicator
        """
        actual = IndicatorDB().get_conclusion_indicator()
        expected = 'therefore'
        self.assertEqual(
            actual,
            expected,
            f"get_conclusion_indicator yields '{actual}' but should yield '{expected}'",
        )
