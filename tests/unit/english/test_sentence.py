"""
test_sentence.py

Unit tests for logic.english.sentence
"""

from test_logic.util import TestUnit

from logic.english.sentence import Sentence


class FakeConjunctionDB:
    """ Fake the conjunction DB """
    get_middle_conjunction = lambda self: 'and'
    get_start_conjunction = lambda self: 'Moreover,'


class FakeIndicatorDB:
    """ Fake the indicator DB """
    get_premise_indicator = lambda self: 'because'
    get_conclusion_indicator = lambda self: 'so'


class TestSentence(TestUnit):
    """
    Base class for testing `Sentence`
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.english.sentence',
            to_mock=('random.choice'),
        )
        self.mock['random.choice'].side_effect = lambda array: array[0]
        Sentence._conjunction_db = FakeConjunctionDB()
        Sentence._indicator_db = FakeIndicatorDB()


class TestNoInit(TestSentence):
    """
    Test that Sentence can't be instantiated
    """


    def test_no_init(self):
        """
        Test that Sentence can't be instantiated
        """
        with self.assertRaises(NotImplementedError):
            _ = Sentence()


class TestPunctuate(TestSentence):
    """
    Test punctuating a sentence
    """


    def test_short(self):
        """
        Test punctuating a short string
        """
        unpunctuated_str = "b"
        actual = Sentence.punctuate(unpunctuated_str)
        expected = "B."
        self.assertEqual(actual, expected)


    def test_with_interior_capital(self):
        """
        Test punctuating a sentence with an interior capital letter
        """
        unpunctuated_str = "test Punctuate"
        actual = Sentence.punctuate(unpunctuated_str)
        expected = "Test Punctuate."
        self.assertEqual(actual, expected)


class TestConjoin(TestSentence):
    """
    Test conjoining two sentences
    """


    def _test_case(self, left_conjunct, right_conjunct, expected):
        actual = Sentence.conjoin(left_conjunct, right_conjunct)
        self.assertEqual(actual, expected)


    def test_conjoin_short(self):
        """
        Test conjoining two short sentences
        """
        self._test_case(
            left_conjunct="here is a short sentence",
            right_conjunct="this is another short sentence",
            expected=(
                "here is a short sentence and "
                "this is another short sentence"
            ),
        )


    def test_conjoin_long(self):
        """
        Test conjoining two sentences with a long result
        """
        self._test_case(
            left_conjunct="here is a short sentence",
            right_conjunct=(
                "this is a longer sentence used for testing "
                "sentence-splitting, which should happen at "
                "this length"
            ),
            expected=(
                "here is a short sentence. Moreover, this is a "
                "longer sentence used for testing sentence-splitting, "
                "which should happen at this length"
            ),
        )


class TestAsCanonicalConclusion(TestSentence):
    """
    Test transforming a sentence into a canonical conclusion
    """


    def test_as_canonical_conclusion(self):
        """
        Test transforming a sentence into a canonical conclusion
        """
        unpunctuated_str = "kittens are cute"
        actual_conc = Sentence.as_canonical_conclusion(unpunctuated_str)
        expected_conc = "Therefore, kittens are cute."
        self.assertEqual(actual_conc, expected_conc)


class TestAsNaturalConclusion(TestSentence):
    """
    Test transforming a sentence into a natural English conclusion
    """


    def test_as_natural_conclusion(self):
        """
        Test transforming a sentence into a natural English conclusion
        """
        unpunctuated_str = "kittens are cute"
        actual_conc = Sentence.as_natural_conclusion(unpunctuated_str)
        expected_conc = "So kittens are cute."
        self.assertEqual(actual_conc, expected_conc)


class TestWithMiddlePrem(TestSentence):
    """
    Test combining two sentences with a premise indicator in the middle
    """


    def test_short_input(self):
        """
        Test combining two short sentences with a premise indicator in the middle
        """
        conc = "Trump is a chump"
        prems = "he's a bully, and bullies are chumps"
        actual_arg = Sentence.with_middle_prem_indicator(conc, prems)
        expected_arg = (
            "Trump is a chump because he's a bully, and bullies are chumps"
        )
        self.assertEqual(actual_arg, expected_arg)

    def test_chop_long_input(self):
        """
        Test combining two sentences with a long result with a premise indicator in the middle
        """
        conc = "this sentence should be chopped up"
        prems = "we need to chop up long sentences and this sentence is quite long"
        actual_arg = Sentence.with_middle_prem_indicator(conc, prems)
        expected_arg = (
            "this sentence should be chopped up. "
            "That's so because we need to chop up long sentences and "
            "this sentence is quite long"
        )
        self.assertEqual(actual_arg, expected_arg)
