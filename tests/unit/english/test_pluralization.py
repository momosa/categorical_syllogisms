"""
test_pluralization.py

Tests for logic.english.pluralization
"""

from textwrap import dedent

from test_logic.util import TestUnit

from logic.english.pluralization import get_plural_noun

class TestGetPluralNoun(TestUnit):
    """
    Test get_plural_noun
    """


    def _test_case(self, singular_noun, expected):
        actual = get_plural_noun(singular_noun)
        self.assertEqual(
            actual,
            expected,
            dedent(f"""
                Noun pluralization failed.
                Singular noun: {repr(singular_noun)}
                Attempted pluralization: {repr(actual)}
                Expected pluralization: {repr(expected)}
            """),
        )


    def test_appending_s(self):
        """
        Test pluralizing a noun by appending an 's'

        The singular noun should not end with 's', 'ch', 'sh', 'x', or 'z'.
        If the noun ends with 'y', the 'y' should be preceded by a vowel.
        """

        self._test_case(
            singular_noun='human',
            expected='humans',
        )

        self._test_case(
            singular_noun='face',
            expected='faces',
        )

        self._test_case(
            singular_noun='monkey',
            expected='monkeys',
        )


    def test_appending_es(self):
        """
        Test pluralizing a noun by appending 'es'

        The singular noun should end with 's', 'ch', 'sh', 'x', or 'z'.
        If the noun ends with a vowel followed by an 's', we should append 'ses'.
        """

        self._test_case(
            singular_noun='bus',
            expected='busses',
        )

        self._test_case(
            singular_noun='bush',
            expected='bushes',
        )

        self._test_case(
            singular_noun='watch',
            expected='watches',
        )

        self._test_case(
            singular_noun='fix',
            expected='fixes',
        )

        self._test_case(
            singular_noun='showbiz',
            expected='showbizzes',
        )


    def test_morphing_ies(self):
        """
        Test pluralizing a noun ending with a consonant followed by a 'y'
        """

        self._test_case(
            singular_noun='body',
            expected='bodies',
        )
