"""
test_syllogism_soundness.py

Unit tests for exercise/syllogism_soundness.py
"""

from textwrap import dedent
from dataclasses import dataclass

from test_logic.util import TestUnit
from unit.categorical.exercise.test_exercise import MixinTestExerciseInit

from logic.categorical.exercise.syllogism_soundness import SyllogismSoundness


@dataclass
class FakeSyllogism:
    """
    Class representing a CategoricalSyllogism
    """
    is_valid: bool
    is_sound: bool
    random_seed: int


class TestExerciseSyllogismSoundness(TestUnit):
    """
    Tests for exercise.SyllogismSoundness
    """

    def setUp(self):

        self.setup_mocks(
            namespace='logic.categorical.exercise.syllogism_soundness',
            to_mock=(
                'get_randseed',
                'random.seed',
                'CategoricalSyllogism',
                'natural_categorical_syllogism',
            ),
        )

        self.mock['CategoricalSyllogism'].side_effect = FakeSyllogism
        self.mock['get_randseed'].return_value = 1
        self.mock['natural_categorical_syllogism'].side_effect = \
            lambda syllogism: 'Fake natural syllogism.'


class TestInit(TestExerciseSyllogismSoundness, MixinTestExerciseInit):
    """
    Tests for exercise.SyllogismSoundness ctor and friends
    """

    def _test_case(self, input_kwargs, expected):
        self._test_exercise(
            exercise_class=SyllogismSoundness,
            exercise_target_type_key='CategoricalSyllogism',
            input_kwargs=input_kwargs,
            expected=expected,
        )


    def test_input_item(self):
        """
        Test soundness exercise generating input
        """

        self._test_case(
            input_kwargs={'is_valid': True, 'is_sound': True},
            expected={
                'input_item': FakeSyllogism(is_valid=True, is_sound=True, random_seed=1),
            },
        )


    def test_illegit_input_item(self):
        """
        Test soundness exercise raising ValueError on bad syllogism
        """
        with self.assertRaises(ValueError):
            _ = SyllogismSoundness(is_valid=True, is_sound=None)


    def test_correct_sound(self):
        """
        Test soundness exercise generating correct answer for sound arguments
        """

        self._test_case(
            input_kwargs={'is_valid': True, 'is_sound': True},
            expected={
                'correct': 'sound',
            },
        )


    def test_correct_valid(self):
        """
        Test soundness exercise generating correct answer for merely valid arguments
        """

        self._test_case(
            input_kwargs={'is_valid': True, 'is_sound': False},
            expected={
                'correct': 'valid but unsound',
            },
        )


    def test_correct_invalid(self):
        """
        Test soundness exercise generating correct answer for merely valid arguments
        """

        self._test_case(
            input_kwargs={'is_valid': False, 'is_sound': False},
            expected={
                'correct': 'invalid',
            },
        )


    def test_question(self):
        """
        Test soundness exercise question generation
        """
        self._test_case(
            input_kwargs={'is_valid': True, 'is_sound': True},
            expected={
                'question': {
                    'syllogism': "Fake natural syllogism.",
                    'question': (
                        "Assess the validity and soundness of the categorical syllogism."
                    )
                },
            },
        )


    def test_responses(self):
        """
        Test contradictory prop exercise response generation
        """
        expected_incorrect_hint = (
            "Start by representing in standard categorical form. "
            "Assess validity using a Venn diagram "
            "or the mood and figure of the syllogism. "
            "Use the truth value of the premises together with validity "
            "to determine soundness."
        )
        self._test_case(
            input_kwargs={'is_valid': True, 'is_sound': True},
            expected={
                'responses': [
                    {
                        'text': 'sound',
                        'correct': True,
                        'hint': 'Correct!',
                    },
                    {
                        'text': 'valid but unsound',
                        'correct': False,
                        'hint': expected_incorrect_hint,
                    },
                    {
                        'text': 'sound but invalid',
                        'correct': False,
                        'hint': expected_incorrect_hint,
                    },
                    {
                        'text': 'invalid',
                        'correct': False,
                        'hint': expected_incorrect_hint,
                    },
                ],
            },
        )


class TestIsCorrect(TestExerciseSyllogismSoundness):
    """
    Tests for exercise.SyllogismSoundness is_correct
    """

    def test_correct_sound(self):
        """
        Test the correct response for a sound argument
        """
        exer = SyllogismSoundness(is_valid=True, is_sound=True)
        correct_response = 'sound'
        self.assertTrue(exer.is_correct(correct_response))


    def test_correct_valid(self):
        """
        Test the correct response for a merely valid argument
        """
        exer = SyllogismSoundness(is_valid=True, is_sound=False)
        correct_response = 'valid but unsound'
        self.assertTrue(exer.is_correct(correct_response))


    def test_correct_invalid(self):
        """
        Test the correct response for an invalid argument
        """
        exer = SyllogismSoundness(is_valid=False, is_sound=False)
        correct_response = 'invalid'
        self.assertTrue(exer.is_correct(correct_response))


    def test_incorrect(self):
        """
        Test an incorrect response
        """
        exer = SyllogismSoundness(is_valid=True, is_sound=True)
        incorrect_response = 'invalid'
        self.assertFalse(exer.is_correct(incorrect_response))


    def test_illegit_input(self):
        """
        Test that is_correct raises a ValueError for illegit input
        """
        exer = SyllogismSoundness(is_valid=True, is_sound=True)
        incorrect_response = 'illegit'
        with self.assertRaises(ValueError):
            _ = exer.is_correct(incorrect_response)


class TestGetHint(TestExerciseSyllogismSoundness):
    """
    Tests for PrositionContradictory get_hint
    """

    def _test_case(self, input_kwargs, candidate, expected):
        exer = SyllogismSoundness(**input_kwargs)
        actual = exer.get_hint(candidate)
        self.assertEqual(
            actual,
            expected,
            dedent(f"""
                '{candidate}' yields the wrong hint.
                Actual: "{actual}"
                Expected: "{expected}"
            """),
        )


    def test_correct(self):
        """
        Test contradictory prop exercise get hint for correct response
        """
        self._test_case(
            input_kwargs={'is_valid': True, 'is_sound': True},
            candidate='sound',
            expected='Correct!',
        )


    def test_incorrect(self):
        """
        Test contradictory prop exercise get hint for incorrect response
        """
        self._test_case(
            input_kwargs={'is_valid': True, 'is_sound': True},
            candidate='invalid',
            expected=(
                "Start by representing in standard categorical form. "
                "Assess validity using a Venn diagram "
                "or the mood and figure of the syllogism. "
                "Use the truth value of the premises together with validity "
                "to determine soundness."
            ),
        )
