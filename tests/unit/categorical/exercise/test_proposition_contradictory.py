"""
test_proposition_contradictory.py

Unit tests for exercise/propsition_contradictory.py
"""

from textwrap import dedent

from test_logic.util import TestUnit
from unit.categorical.exercise.test_exercise import MixinTestExerciseInit

from logic.categorical.exercise.proposition_contradictory import PropositionContradictory


class TestExercisePropositionContradictory(TestUnit):
    """
    Tests for exercise.PropositionContradictory
    """

    def setUp(self):

        self.setup_mocks(
            namespace='logic.categorical.exercise.proposition_contradictory',
            to_mock=(
                'get_randseed',
                'random.seed',
                'CategoricalProposition',
                'natural_categorical_proposition',
                'contradictory',
                'converse',
                'contrary',
                'subalternate',
            ),
        )

        self.mock['CategoricalProposition'].return_value = 'proposition'
        self.mock['get_randseed'].return_value = 1
        self.mock['natural_categorical_proposition'].side_effect = \
            lambda prop: f'Fake natural {prop}.'
        self.mock['contradictory'].return_value = 'contradictory proposition'
        self.mock['converse'].return_value = 'converse proposition'
        self.mock['contrary'].return_value = 'contrary proposition'
        self.mock['subalternate'].return_value = 'subalternate proposition'


class TestInit(TestExercisePropositionContradictory, MixinTestExerciseInit):
    """
    Tests for exercise.PropositionContradictory ctor and friends
    """

    def _test_case(self, input_kwargs, expected):
        self._test_exercise(
            exercise_class=PropositionContradictory,
            exercise_target_type_key='proposition',
            input_kwargs=input_kwargs,
            expected=expected,
        )


    def test_contradictory_designation(self):
        """
        Test contradictory prop exercise generating correct answer
        """

        self._test_case(
            input_kwargs={},
            expected={
                'correct': 'contradictory proposition',
            },
        )


    def test_question(self):
        """
        Test contradictory prop exercise question generation
        """
        self._test_case(
            input_kwargs={},
            expected={
                'question': {
                    'proposition': "Fake natural proposition.",
                    'question': (
                        "What is the contradictory (opposite) "
                        "of that proposition?"
                    )
                },
            },
        )


    def test_responses(self):
        """
        Test contradictory prop exercise response generation
        """
        expected_incorrect_hint = (
            "Translate the English sentence into categorical logic. "
            "Then use the square of opposition to find the contradictory. "
            "One at a time, starting with the most promising candidate, "
            "translate the choices into categorical logic "
            "until you find one that matches."
        )
        self._test_case(
            input_kwargs={},
            expected={
                'responses': [
                    {
                        'text': "Fake natural contrary proposition.",
                        'correct': False,
                        'hint': expected_incorrect_hint,
                    },
                    {
                        'text': "Fake natural subalternate proposition.",
                        'correct': False,
                        'hint': expected_incorrect_hint,
                    },
                    {
                        'text': "Fake natural contradictory proposition.",
                        'correct': True,
                        'hint': 'Correct!',
                    },
                    {
                        'text': "Fake natural converse proposition.",
                        'correct': False,
                        'hint': expected_incorrect_hint,
                    },
                ],
            },
        )


class TestIsCorrect(TestExercisePropositionContradictory):
    """
    Tests for exercise.PropositionContradictory is_correct
    """

    def test_correct(self):
        """
        Test the correct response
        """
        exer = PropositionContradictory()
        correct_response = 'contradictory proposition'
        self.assertTrue(exer.is_correct(correct_response))


    def test_incorrect(self):
        """
        Test the incorrect response
        """
        exer = PropositionContradictory()
        incorrect_response = 'incorrect proposition'
        self.assertFalse(exer.is_correct(incorrect_response))


class TestGetHint(TestExercisePropositionContradictory):
    """
    Tests for PrositionContradictory get_hint
    """

    def _test_case(self, candidate, expected):
        exer = PropositionContradictory()
        actual = exer.get_hint(candidate)
        self.assertEqual(
            actual,
            expected,
            dedent(f"""
                '{candidate}' yields the wrong hint.
                Actual: "{actual}"
                Expected: "{expected}"
            """),
        )


    def test_correct(self):
        """
        Test contradictory prop exercise get hint for correct response
        """
        self._test_case(
            candidate='contradictory proposition',
            expected='Correct!',
        )


    def test_incorrect(self):
        """
        Test contradictory prop exercise get hint for incorrect response
        """
        self._test_case(
            candidate='incorrect proposition',
            expected=(
                "Translate the English sentence into categorical logic. "
                "Then use the square of opposition to find the contradictory. "
                "One at a time, starting with the most promising candidate, "
                "translate the choices into categorical logic "
                "until you find one that matches."
            ),
        )

        self._test_case(
            candidate={'incorrect': 'proposition'},
            expected=(
                "Translate the English sentence into categorical logic. "
                "Then use the square of opposition to find the contradictory. "
                "One at a time, starting with the most promising candidate, "
                "translate the choices into categorical logic "
                "until you find one that matches."
            ),
        )
