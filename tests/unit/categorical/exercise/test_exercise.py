"""
base_exercise_test.py

Base test classes for testing exercises
"""

from textwrap import dedent
import unittest


class MixinTestExerciseInit(unittest.TestCase):
    """
    Tests for exercise ctor and friends
    """

    def _test_exercise(
            self,
            exercise_class,
            exercise_target_type_key,
            input_kwargs,
            expected,
        ):
        """
        Basic tests for an exercise

        :param class exercise_class: The class generating the exercise object
        :param str exercise_target_type_key: A hint about the type of input item.
            Used only for feedback on test failure.
            This param should probably be removed.
        :param dict input_kwargs: kwargs used to generate the exercise object
        :param dict expected: attributes of the exercise.
            Only keys given in `expected` will be tested.
        """
        actual = exercise_class(**input_kwargs)
        if exercise_target_type_key in expected:
            self._test_target(
                exercise_target_type_key,
                input_kwargs,
                actual.input_item,
                expected[exercise_target_type_key],
            )
        if 'correct' in expected:
            self._test_correct(
                input_kwargs,
                actual.correct,
                expected['correct'],
            )
        if 'question' in expected:
            self._test_question(
                input_kwargs,
                actual.question,
                expected['question'],
            )
        if 'responses' in expected:
            self._test_responses(
                input_kwargs,
                actual.responses,
                expected['responses'],
            )


    def _test_target(self, exercise_target_type_key, input_kwargs, actual, expected):
        self.assertEqual(
            actual,
            expected,
            dedent(f"""
                The {exercise_target_type_key} was not assigned properly.
                Input:{input_kwargs}
                Actual {exercise_target_type_key}:
                {actual}
                Expected {exercise_target_type_key}:
                {expected}
            """),
        )


    def _test_correct(self, input_kwargs, actual, expected):
        self.assertEqual(
            actual,
            expected,
            dedent(f"""
                The correct value was not assigned properly.
                Input:{input_kwargs}
                Actual correct:
                {str(actual)}
                Expected correct:
                {expected}
            """),
        )


    def _test_question(self, input_kwargs, actual, expected):
        self.assertDictEqual(
            actual,
            expected,
            dedent(f"""
                The question was not assigned properly.
                Input:
                {input_kwargs}
                Actual question:
                {actual}
                Expected question:
                {expected}
            """),
        )


    def _test_responses(self, input_kwargs, actual, expected):
        self.assertListEqual(
            actual,
            expected,
            dedent(f"""
                The responses were not assigned properly.
                Input:{input_kwargs}
                Actual responses:
                {actual}
                Expected responses:
                {expected}
            """),
        )
