"""
test_proposition_type.py

Unit tests for exercise/propsition_type.py
"""

from dataclasses import dataclass
from textwrap import dedent

from test_logic.util import TestUnit
from unit.categorical.exercise.test_exercise import MixinTestExerciseInit

from logic.categorical.exercise.proposition_type import PropositionType


@dataclass
class FakeProposition:
    """
    Fake categorical proposition
    """
    random_seed: int
    proposition_type: str


class TestExercisePropositionType(TestUnit):
    """
    Tests for exercise.PropositionType
    """

    def setUp(self):

        self.setup_mocks(
            namespace='logic.categorical.exercise.proposition_type',
            to_mock=(
                'get_randseed',
                'random.seed',
                'CategoricalProposition',
                'natural_categorical_proposition',
            ),
        )

        self.mock['CategoricalProposition'].side_effect = FakeProposition
        self.mock['get_randseed'].return_value = 1
        self.mock['natural_categorical_proposition'].return_value = 'Fake natural proposition.'


class TestInit(TestExercisePropositionType, MixinTestExerciseInit):
    """
    Tests for exercise.PropositionType ctor and friends
    """

    def _test_case(self, input_kwargs, expected):
        self._test_exercise(
            exercise_class=PropositionType,
            exercise_target_type_key='proposition',
            input_kwargs=input_kwargs,
            expected=expected,
        )


    def test_prop_type_designation(self):
        """
        Test proposition type exercise designating prop type
        """
        self._test_case(
            input_kwargs={'proposition_type': 'A'},
            expected={'correct': 'A'},
        )
        self._test_case(
            input_kwargs={'proposition_type': 'E'},
            expected={'correct': 'E'},
        )
        self._test_case(
            input_kwargs={'proposition_type': 'I'},
            expected={'correct': 'I'},
        )
        self._test_case(
            input_kwargs={'proposition_type': 'O'},
            expected={'correct': 'O'},
        )


    def test_question(self):
        """
        Test proposition type exercise question generation
        """
        self._test_case(
            input_kwargs={'proposition_type': 'A'},
            expected={
                'question': {
                    'proposition': 'Fake natural proposition.',
                    'question': (
                        "What type of categorical proposition "
                        "does that express?"
                    ),
                },
            },
        )


    def test_responses(self):
        """
        Test proposition type exercise response generation
        """
        self._test_case(
            input_kwargs={'proposition_type': 'A'},
            expected={
                'responses': [
                    {
                        'text': 'A',
                        'correct': True,
                        'hint': 'Correct!',
                    },
                    {
                        'text': 'E',
                        'correct': False,
                        'hint': (
                            "Remember that shading indicates that there's "
                            "nothing in the shaded region."
                        ),
                    },
                    {
                        'text': 'I',
                        'correct': False,
                        'hint': (
                            "Remember that an 'x' only indicates that "
                            "something exists. It doesn't give us information "
                            "about everything in a category."
                        ),
                    },
                    {
                        'text': 'O',
                        'correct': False,
                        'hint': (
                            "Remember that an 'x' only indicates that "
                            "something exists. It doesn't give us information "
                            "about everything in a category."
                        ),
                    },
                ],
            },
        )

        self._test_case(
            input_kwargs={'proposition_type': 'I'},
            expected={
                'responses': [
                    {
                        'text': 'A',
                        'correct': False,
                        'hint': (
                            "Remember that shading gives information about "
                            "everything in a category."
                        ),
                    },
                    {
                        'text': 'E',
                        'correct': False,
                        'hint': (
                            "Remember that shading gives information about "
                            "everything in a category."
                        ),
                    },
                    {
                        'text': 'I',
                        'correct': True,
                        'hint': 'Correct!'
                    },
                    {
                        'text': 'O',
                        'correct': False,
                        'hint': (
                            "Remember what each subregion represents. What kind "
                            "of things are in the region you selected?"
                        ),
                    },
                ],
            },
        )


class TestIsCorrect(TestExercisePropositionType):
    """
    Tests for exercise.PropositionType is_correct
    """

    def _test_correct(self, input_kwargs, candidate):
        exer = PropositionType(**input_kwargs)
        actual = exer.is_correct(candidate)
        self.assertTrue(
            actual,
            dedent(f"""
                '{actual}' returned incorrect, but it should be correct.
                Input:
                {input_kwargs}
            """),
        )


    def _test_incorrect(self, input_kwargs, candidate):
        exer = PropositionType(**input_kwargs)
        actual = exer.is_correct(candidate)
        self.assertFalse(
            actual,
            dedent(f"""
                '{actual}' returned correct, but it should be incorrect.
                Input:
                {input_kwargs}
            """),
        )



    def test_prop_type_a(self):
        """
        Test prop type exercise correctness for prop type A
        """
        input_kwargs = {'proposition_type': 'A'}
        self._test_correct(input_kwargs, 'A')
        self._test_incorrect(input_kwargs, 'E')
        self._test_incorrect(input_kwargs, 'I')
        self._test_incorrect(input_kwargs, 'O')


    def test_prop_type_e(self):
        """
        Test prop type exercise correctness for prop type E
        """
        input_kwargs = {'proposition_type': 'E'}
        self._test_incorrect(input_kwargs, 'A')
        self._test_correct(input_kwargs, 'E')
        self._test_incorrect(input_kwargs, 'I')
        self._test_incorrect(input_kwargs, 'O')


    def test_prop_type_i(self):
        """
        Test prop type exercise correctness for prop type I
        """
        input_kwargs = {'proposition_type': 'I'}
        self._test_incorrect(input_kwargs, 'A')
        self._test_incorrect(input_kwargs, 'E')
        self._test_correct(input_kwargs, 'I')
        self._test_incorrect(input_kwargs, 'O')


    def test_prop_type_o(self):
        """
        Test prop type exercise correctness for prop type O
        """
        input_kwargs = {'proposition_type': 'O'}
        self._test_incorrect(input_kwargs, 'A')
        self._test_incorrect(input_kwargs, 'E')
        self._test_incorrect(input_kwargs, 'I')
        self._test_correct(input_kwargs, 'O')


class TestGetHint(TestExercisePropositionType):
    """
    Tests for exercise.PropositionType get_hint
    """

    def _test_case(self, input_kwargs, candidate, expected):
        exer = PropositionType(**input_kwargs)
        actual = exer.get_hint(candidate)
        self.assertEqual(
            actual,
            expected,
            dedent(f"""
                '{candidate}' yields the wrong hint.
                Input:
                {input_kwargs}
                Actual: "{actual}"
                Expected: "{expected}"
            """),
        )


    def test_prop_type_a(self):
        """
        Test prop type exercise get hint for prop type A
        """
        input_kwargs = {'proposition_type': 'A'}
        self._test_case(input_kwargs, 'A', "Correct!")
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='E',
            expected=(
                "Remember that shading indicates that there's "
                "nothing in the shaded region."
            ),
        )
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='I',
            expected=(
                "Remember that an 'x' only indicates that "
                "something exists. It doesn't give us information "
                "about everything in a category."
            ),
        )
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='O',
            expected=(
                "Remember that an 'x' only indicates that "
                "something exists. It doesn't give us information "
                "about everything in a category."
            ),
        )


    def test_prop_type_e(self):
        """
        Test prop type exercise get hint for prop type E
        """
        input_kwargs = {'proposition_type': 'E'}
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='A',
            expected=(
                "Remember that shading indicates that there's "
                "nothing in the shaded region."
            ),
        )
        self._test_case(input_kwargs, 'E', "Correct!")
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='I',
            expected=(
                "Remember that an 'x' only indicates that "
                "something exists. It doesn't give us information "
                "about everything in a category."
            ),
        )
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='O',
            expected=(
                "Remember that an 'x' only indicates that "
                "something exists. It doesn't give us information "
                "about everything in a category."
            ),
        )


    def test_prop_type_i(self):
        """
        Test prop type exercise get hint for prop type I
        """
        input_kwargs = {'proposition_type': 'I'}
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='A',
            expected=(
                "Remember that shading gives information about "
                "everything in a category."
            ),
        )
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='E',
            expected=(
                "Remember that shading gives information about "
                "everything in a category."
            ),
        )
        self._test_case(input_kwargs, 'I', "Correct!")
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='O',
            expected=(
                "Remember what each subregion represents. What kind "
                "of things are in the region you selected?"
            ),
        )


    def test_prop_type_o(self):
        """
        Test prop type exercise get hint for prop type O
        """
        input_kwargs = {'proposition_type': 'O'}
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='A',
            expected=(
                "Remember that shading gives information about "
                "everything in a category."
            ),
        )
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='E',
            expected=(
                "Remember that shading gives information about "
                "everything in a category."
            ),
        )
        self._test_case(
            input_kwargs=input_kwargs,
            candidate='I',
            expected=(
                "Remember what each subregion represents. What kind "
                "of things are in the region you selected?"
            ),
        )
        self._test_case(input_kwargs, 'O', "Correct!")
