"""
test_proposition.py

Tests for logic.categorical.proposition.CategoricalProposition
"""

from textwrap import dedent

from test_logic.util import TestUnit

from logic.categorical.proposition import CategoricalProposition


class TestCategoricalProposition(TestUnit):
    """
    Test creating a categorical proposition.
    Recall that determine_proposition_kwargs standardizes kwargs for us.
    The dataclass gives us lots of tested functionality OOTB
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.proposition',
            to_mock='determine_proposition_kwargs',
        )


    def _test_case(self, input_kwargs, expected):
        prop = CategoricalProposition(**input_kwargs)
        mismatch_errs = []
        for attr in expected:
            if not hasattr(prop, attr) or not getattr(prop, attr) == expected[attr]:
                mismatch_errs.append(attr)
        self.assertEqual(
            mismatch_errs,
            [],
            dedent(f"""
                CategoricalProposition creation failed.
                Mismatched attributes: {mismatch_errs}
                CategoricalProposition created:
                {repr(prop)}
                Input kwargs:
                {repr(input_kwargs)}
                Expected attributes:
                {repr(expected)}
            """),
        )


    def test_empty_input(self):
        """
        Test providing empty input
        """
        self.mock['determine_proposition_kwargs'].return_value = {
            'random_seed': 1,
            'topic_name': 'topic_name',
            'truth_value': True,
            'proposition_type': 'A',
            'subject': 'mammals',
            'predicate': 'animals',
        }
        self._test_case(
            input_kwargs={},
            expected={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_full_spec(self):
        """
        Test providing a full spec
        """
        self.mock['determine_proposition_kwargs'].side_effect = lambda **kwargs: kwargs
        self._test_case(
            input_kwargs={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
            expected={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )
