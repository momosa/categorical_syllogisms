"""
test_syllogism.py

Tests for logic.categorical.syllogism.CategoricalSyllogism
"""

from textwrap import dedent
from dataclasses import dataclass

from test_logic.util import TestUnit

from logic.categorical.syllogism import CategoricalSyllogism


@dataclass
class FakeProposition:
    """
    Class representing a categorical proposition.
    """
    proposition_type: str
    subject: str
    predicate: str
    truth_value: bool = True


class TestCategoricalProposition(TestUnit):
    """
    Test creating a categorical syllogism.
    Recall that determine_syllogism_kwargs standardizes kwargs for us.
    The dataclass gives us lots of tested functionality OOTB
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.syllogism',
            to_mock=(
                'determine_syllogism_kwargs',
                'CategoricalProposition',
            ),
        )

        self.mock['CategoricalProposition'].side_effect = FakeProposition


    def _test_case(self, input_kwargs, expected):
        prop = CategoricalSyllogism(**input_kwargs)
        mismatch_errs = []
        for attr in expected:
            if not hasattr(prop, attr) or not getattr(prop, attr) == expected[attr]:
                mismatch_errs.append(attr)
        self.assertEqual(
            mismatch_errs,
            [],
            dedent(f"""
                Syllogism creation failed.
                Mismatched attributes: {mismatch_errs}
                CategoricalSyllogism created:
                {repr(prop)}
                Input kwargs:
                {repr(input_kwargs)}
                Expected attributes:
                {repr(expected)}
            """),
        )


    def test_empty_input(self):
        """
        Test providing empty input
        """
        self.mock['determine_syllogism_kwargs'].return_value = {
            'random_seed': 1,
            'topic_name': 'topic_name',
            'propositions': {
                'major_premise': FakeProposition(
                    truth_value=True,
                    proposition_type='O',
                    subject='reptiles',
                    predicate='humans',
                ),
                'minor_premise': FakeProposition(
                    truth_value=True,
                    proposition_type='A',
                    subject='reptiles',
                    predicate='animals',
                ),
                'conclusion': FakeProposition(
                    truth_value=True,
                    proposition_type='O',
                    subject='animals',
                    predicate='humans',
                ),
            },
            'categories': {
                'major_term': 'humans',
                'minor_term': 'animals',
                'middle_term': 'reptiles',
            },
            'form': 'OAO-3',
            'is_valid': True,
            'is_sound': True,
        }
        self._test_case(
            input_kwargs={},
            expected={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        truth_value=True,
                        proposition_type='O',
                        subject='reptiles',
                        predicate='humans',
                    ),
                    'minor_premise': FakeProposition(
                        truth_value=True,
                        proposition_type='A',
                        subject='reptiles',
                        predicate='animals',
                    ),
                    'conclusion': FakeProposition(
                        truth_value=True,
                        proposition_type='O',
                        subject='animals',
                        predicate='humans',
                    ),
                },
                'categories': {
                    'major_term': 'humans',
                    'minor_term': 'animals',
                    'middle_term': 'reptiles',
                },
                'form': 'OAO-3',
                'is_valid': True,
                'is_sound': True,
            },
        )


    def test_full_spec(self):
        """
        Test providing a full spec
        """
        self.mock['determine_syllogism_kwargs'].side_effect = lambda **kwargs: kwargs
        self._test_case(
            input_kwargs={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        truth_value=True,
                        proposition_type='O',
                        subject='reptiles',
                        predicate='humans',
                    ),
                    'minor_premise': FakeProposition(
                        truth_value=True,
                        proposition_type='A',
                        subject='reptiles',
                        predicate='animals',
                    ),
                    'conclusion': FakeProposition(
                        truth_value=True,
                        proposition_type='O',
                        subject='animals',
                        predicate='humans',
                    ),
                },
                'categories': {
                    'major_term': 'humans',
                    'minor_term': 'animals',
                    'middle_term': 'reptiles',
                },
                'form': 'OAO-3',
                'is_valid': True,
                'is_sound': True,
            },
            expected={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        truth_value=True,
                        proposition_type='O',
                        subject='reptiles',
                        predicate='humans',
                    ),
                    'minor_premise': FakeProposition(
                        truth_value=True,
                        proposition_type='A',
                        subject='reptiles',
                        predicate='animals',
                    ),
                    'conclusion': FakeProposition(
                        truth_value=True,
                        proposition_type='O',
                        subject='animals',
                        predicate='humans',
                    ),
                },
                'categories': {
                    'major_term': 'humans',
                    'minor_term': 'animals',
                    'middle_term': 'reptiles',
                },
                'form': 'OAO-3',
                'is_valid': True,
                'is_sound': True,
            },
        )
