"""
test_copula.py

Unit tests for logic.categorical.english.copula
"""

from test_logic.util import TestUnit

from logic.categorical.english.copula import (
    canonical,
    natural,
)


class TestCanonical(TestUnit):
    """
    Test getting a canonical copula
    """


    def test_a(self):
        """
        Test getting a canonical copula for proposition type A
        """
        actual = canonical('A')
        expected = 'are'
        self.assertEqual(actual, expected)


    def test_e(self):
        """
        Test getting a canonical copula for proposition type E
        """
        actual = canonical('E')
        expected = 'are'
        self.assertEqual(actual, expected)


    def test_i(self):
        """
        Test getting a canonical copula for proposition type I
        """
        actual = canonical('I')
        expected = 'are'
        self.assertEqual(actual, expected)


    def test_o(self):
        """
        Test getting a canonical copula for proposition type O
        """
        actual = canonical('O')
        expected = 'are not'
        self.assertEqual(actual, expected)


class TestNatural(TestUnit):
    """
    Test getting a natural English copula
    """


    def test_prop_type_a(self):
        """
        Test getting natural English copula for proposition type A
        """
        self.assertEqual(natural('A', 'plural'), "are")
        self.assertEqual(natural('A', 'singular'), "is")
        self.assertEqual(natural('A', 'plural', is_contrary=True), "aren't")
        self.assertEqual(natural('A', 'singular', is_contrary=True), "isn't")


    def test_prop_type_e(self):
        """
        Test getting natural English copula for proposition type E
        """
        self.assertEqual(natural('E', 'plural'), "are")
        self.assertEqual(natural('E', 'singular'), "is")
        self.assertEqual(natural('E', 'plural', is_contrary=True), "aren't")
        self.assertEqual(natural('E', 'singular', is_contrary=True), "isn't")


    def test_prop_type_i(self):
        """
        Test getting natural English copula for proposition type I
        """
        self.assertEqual(natural('I', 'plural'), "are")
        self.assertEqual(natural('I', 'singular'), "is")
        self.assertEqual(natural('I', 'plural', is_contrary=True), "aren't")
        self.assertEqual(natural('I', 'singular', is_contrary=True), "isn't")


    def test_prop_type_o(self):
        """
        Test getting natural English copula for proposition type O
        """
        self.assertEqual(natural('O', 'plural'), "aren't")
        self.assertEqual(natural('O', 'singular'), "isn't")
        self.assertEqual(natural('O', 'plural', is_contrary=True), "are")
        self.assertEqual(natural('O', 'singular', is_contrary=True), "is")


    def test_unrecognized_prop_type(self):
        """
        Test getting error on unrecognized proposition type
        """
        with self.assertRaises(ValueError):
            dummy = natural('A', 'unrecognized')
