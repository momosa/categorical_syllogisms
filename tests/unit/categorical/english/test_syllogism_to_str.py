"""
test_sentence.py

Unit tests for `logic.categorical.english.syllogism_to_str`
"""

from test_logic.util import TestUnit

from logic.categorical.english.syllogism_to_str import (
    canonical_categorical_syllogism,
    natural_categorical_syllogism,
)


class FakeCategoricalSyllogism:
    """ Fake a categorical syllogism with only necessary components """
    propositions = {
        'major_premise': 'major premise',
        'minor_premise': 'minor premise',
        'conclusion': 'conclusion',
    }

class TestSyllogismToString(TestUnit):
    """
    Base class for test categorical sentences
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.english.syllogism_to_str',
            to_mock=(
                'canonical_categorical_proposition',
                'natural_categorical_proposition',
                'canonical_syllogism',
                'natural_syllogism',
            ),
        )

        self.mock['canonical_categorical_proposition'].side_effect = \
            lambda string, punctuate: f'canonical {string}'
        self.mock['natural_categorical_proposition'].side_effect = \
            lambda string, punctuate: f'natural {string}'
        self.mock['canonical_syllogism'].side_effect = \
            lambda prem1, prem2, conc: f'{prem1}. {prem2}. Therefore, {conc}.'
        self.mock['natural_syllogism'].side_effect = \
            lambda prem1, prem2, conc: f'{prem1}. {prem2}. So, {conc}.'


    def test_canonical(self):
        """
        Test getting the canonical English form of a categorical syllogism
        """
        actual = canonical_categorical_syllogism(FakeCategoricalSyllogism())
        expected = (
            'canonical major premise. '
            'canonical minor premise. '
            'Therefore, canonical conclusion.'
        )
        self.assertEqual(actual, expected)


    def test_natural(self):
        """
        Test getting the natural English form of a categorical syllogism
        """
        actual = natural_categorical_syllogism(FakeCategoricalSyllogism())
        expected = (
            'natural major premise. '
            'natural minor premise. '
            'So, natural conclusion.'
        )
        self.assertEqual(actual, expected)
