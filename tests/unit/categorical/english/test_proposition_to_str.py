"""
test_sentence.py

Unit tests for `logic.categorical.english.proposition_to_str`
"""

from dataclasses import dataclass

from test_logic.util import TestUnit

from logic.categorical.english.proposition_to_str import (
    canonical_categorical_proposition,
    natural_categorical_proposition,
)


FAKE_TOPIC = {
    'animals': {
        'plural': 'animals',
        'singular': 'animal',
    },
    'mammals': {
        'plural': 'mammals',
        'singular': 'mammal',
    },
}


@dataclass
class FakeProposition:
    """ Fake a categorical proposition with only necessary components """
    proposition_type: str
    subject: str
    predicate: str
    topic_name: str = 'topic_name'

class TestCategoricalSentence(TestUnit):
    """
    Base class for test categorical sentences
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.english.proposition_to_str',
            to_mock=(
                'randbool',
                'proposition_transformer.converse',
                'proposition_transformer.contrary',
                'proposition_transformer.contradictory',
                'Topic',
                'Sentence.punctuate',
                'quantifier.canonical',
                'quantifier.natural',
                'quantifier.is_singular',
                'copula.canonical',
                'copula.natural',
            ),
        )

        self.mock['Topic'].return_value = FAKE_TOPIC
        self.mock['Sentence.punctuate'].side_effect = lambda string: f'Punctuated {string}.'


class TestCanonical(TestCategoricalSentence):
    """
    Test getting a canonical categorical sentence
    """


    def test_punctuated(self):
        """
        Test getting a canonical categorical sentence
        """
        self.mock['quantifier.canonical'].return_value = 'all'
        self.mock['copula.canonical'].return_value = 'are'
        proposition = FakeProposition('A', 'animals', 'mammals')
        actual = canonical_categorical_proposition(proposition)
        expected = 'Punctuated all animals are mammals.'
        self.assertEqual(actual, expected)


    def test_unpunctuated(self):
        """
        Test getting an unpunctuated canonical sentence
        """
        self.mock['quantifier.canonical'].return_value = 'all'
        self.mock['copula.canonical'].return_value = 'are'
        proposition = FakeProposition('A', 'animals', 'mammals')
        actual = canonical_categorical_proposition(
            proposition,
            punctuate=False,
        )
        expected = 'all animals are mammals'
        self.assertEqual(actual, expected)




class TestNatural(TestCategoricalSentence):
    """
    Test getting a canonical categorical sentence
    """


    def test_punctuated(self):
        """
        Test getting a canonical categorical sentence
        """

        self.mock['quantifier.natural'].return_value = 'all'
        self.mock['quantifier.is_singular'].return_value = False
        self.mock['copula.natural'].return_value = 'are'
        self.mock['randbool'].return_value = False

        proposition = FakeProposition('A', 'animals', 'mammals')
        actual = natural_categorical_proposition(proposition)
        expected = 'Punctuated all animals are mammals.'
        self.assertEqual(actual, expected)


    def test_no_swap_no_contra_plural(self):
        """
        Test without swapping and creating a contrary, and plural
        """
        self.mock['quantifier.natural'].return_value = 'all'
        self.mock['quantifier.is_singular'].return_value = False
        self.mock['copula.natural'].return_value = 'are'
        self.mock['randbool'].return_value = False

        proposition = FakeProposition('A', 'animals', 'mammals')
        actual = natural_categorical_proposition(
            proposition,
            punctuate=False,
        )
        expected = 'all animals are mammals'
        self.assertEqual(actual, expected)


    def test_no_swap_no_contra_singular(self):
        """
        Test without swapping and creating a contrary, and singular
        """
        self.mock['quantifier.natural'].return_value = 'every'
        self.mock['quantifier.is_singular'].return_value = True
        self.mock['copula.natural'].return_value = 'is'
        self.mock['randbool'].return_value = False

        proposition = FakeProposition('A', 'animals', 'mammals')
        actual = natural_categorical_proposition(
            proposition,
            punctuate=False,
        )
        expected = 'every animal is a mammal'
        self.assertEqual(actual, expected)


    def test_swap_plural(self):
        """
        Test with swapping and plural
        """
        self.mock['quantifier.natural'].return_value = 'only'
        self.mock['quantifier.is_singular'].return_value = False
        self.mock['copula.natural'].return_value = 'are'
        self.mock['randbool'].return_value = True
        converse = FakeProposition(
            'A',
            'mammals',
            'animals',
        )
        self.mock['proposition_transformer.converse'].return_value = converse

        proposition = FakeProposition('A', 'animals', 'mammals')
        actual = natural_categorical_proposition(
            proposition,
            punctuate=False,
        )
        expected = 'only mammals are animals'
        self.assertEqual(actual, expected)


    def test_contrary_plural(self):
        """
        Test with using a contrary and plural
        """
        self.mock['quantifier.natural'].return_value = 'all'
        self.mock['quantifier.is_singular'].return_value = False
        self.mock['copula.natural'].return_value = "aren't"
        self.mock['randbool'].return_value = True
        contrary = FakeProposition(
            'A',
            'animals',
            'mammals',
        )
        self.mock['proposition_transformer.contrary'].return_value = contrary

        proposition = FakeProposition('E', 'animals', 'mammals')
        actual = natural_categorical_proposition(
            proposition,
            punctuate=False,
        )
        expected = "animals aren't mammals"
        self.assertEqual(actual, expected)


    def test_no_double_negation(self):
        """
        Test that we don't negate both the quantifier and the copula of an 'O'
        """
        self.mock['quantifier.natural'].return_value = 'not all'
        self.mock['quantifier.is_singular'].return_value = False
        self.mock['copula.natural'].return_value = "are"
        self.mock['randbool'].return_value = False
        contradictory = FakeProposition(
            'A',
            'animals',
            'mammals',
        )
        self.mock['proposition_transformer.contradictory'].return_value = contradictory

        proposition = FakeProposition('E', 'animals', 'mammals')
        actual = natural_categorical_proposition(
            proposition,
            punctuate=False,
        )
        expected = "not all animals are mammals"
        self.assertEqual(actual, expected)
