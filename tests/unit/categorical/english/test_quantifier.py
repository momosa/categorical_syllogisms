"""
test_quantifier.py

Unit tests for logic.categorical.english.quantifier
"""

from test_logic.util import TestUnit

from logic.categorical.english.quantifier import (
    canonical,
    natural,
    is_singular,
)


class TestCanonical(TestUnit):
    """
    Test getting a canonical quantifier
    """


    def test_a(self):
        """
        Test getting a canonical quantifier for proposition type A
        """
        actual = canonical('A')
        expected = 'all'
        self.assertEqual(actual, expected)


    def test_e(self):
        """
        Test getting a canonical quantifier for proposition type E
        """
        actual = canonical('E')
        expected = 'no'
        self.assertEqual(actual, expected)


    def test_i(self):
        """
        Test getting a canonical quantifier for proposition type I
        """
        actual = canonical('I')
        expected = 'some'
        self.assertEqual(actual, expected)


    def test_o(self):
        """
        Test getting a canonical quantifier for proposition type O
        """
        actual = canonical('O')
        expected = 'some'
        self.assertEqual(actual, expected)


    def test_unrecognized(self):
        """
        Test getting error on unrecognized proposition type
        """
        with self.assertRaises(ValueError):
            dummy = canonical('unrecognized')


class TestNatural(TestUnit):
    """
    Test getting a natural English quantifier
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.english.quantifier',
            to_mock='random.choice',
        )

        self.mock['random.choice'].side_effect = lambda array: array[0]


    def test_a_swapped(self):
        """
        Test getting a natural English quantifier for proposition type A and swapped terms
        """
        actual = natural('A', order_swapping=True)
        expected = 'only'
        self.assertEqual(actual, expected)


    def test_a_unswapped(self):
        """
        Test getting a natural English quantifier for proposition type A and uswapped terms
        """
        actual = natural('A', order_swapping=False)
        expected = 'all'
        self.assertEqual(actual, expected)


    def test_e_swapped(self):
        """
        Test getting a natural English quantifier for proposition type E and swapped terms
        """
        actual = natural('E', order_swapping=True)
        expected = 'no'
        self.assertEqual(actual, expected)


    def test_e_unswapped(self):
        """
        Test getting a natural English quantifier for proposition type E and uswapped terms
        """
        actual = natural('E', order_swapping=False)
        expected = 'no'
        self.assertEqual(actual, expected)


    def test_i_swapped(self):
        """
        Test getting a natural English quantifier for proposition type I and swapped terms
        """
        actual = natural('I', order_swapping=True)
        expected = 'some'
        self.assertEqual(actual, expected)


    def test_i_unswapped(self):
        """
        Test getting a natural English quantifier for proposition type I and uswapped terms
        """
        actual = natural('I', order_swapping=False)
        expected = 'some'
        self.assertEqual(actual, expected)


    def test_o_swapped(self):
        """
        Test getting a natural English quantifier for proposition type O and swapped terms
        """
        actual = natural('O', order_swapping=True)
        expected = 'not only'
        self.assertEqual(actual, expected)


    def test_o_unswapped(self):
        """
        Test getting a natural English quantifier for proposition type O and uswapped terms
        """
        actual = natural('O', order_swapping=False)
        expected = 'some'
        self.assertEqual(actual, expected)


    def test_unrecognized(self):
        """
        Test getting error on unrecognized proposition type
        """
        with self.assertRaises(ValueError):
            dummy = natural('unrecognized')


class TestIsSingular(TestUnit):
    """
    Test determining if a quantifier is singular or plural
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.english.quantifier',
            to_mock='randbool',
        )


    def test_singular(self):
        """
        Test identifying singular quantifiers as such
        """
        self.assertEqual(is_singular('every'), True)
        self.assertEqual(is_singular('any'), True)


    def test_plural(self):
        """
        Test identifying singular quantifiers as such
        """
        self.assertEqual(is_singular('all'), False)
        self.assertEqual(is_singular('only'), False)
        self.assertEqual(is_singular('just'), False)


    def test_arbitrary(self):
        """
        Test arbitrarily selecting pluralization
        """
        self.mock['randbool'].return_value = True
        self.assertEqual(is_singular('some'), True)
        self.mock['randbool'].assert_called()


    def test_negated(self):
        """
        Test negated quantifiers
        """
        self.assertEqual(is_singular('not every'), True)
        self.assertEqual(is_singular('not any'), True)
        self.assertEqual(is_singular('not all'), False)
        self.assertEqual(is_singular('not only'), False)
        self.assertEqual(is_singular('not just'), False)
