"""
test_topic.py

Tests for Topic
"""

from test_logic.util import TestUnit

from logic.categorical.topic import Topic


class TestTopic(TestUnit):
    """
    Setup for topic tests
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.topic',
            to_mock=(
                'fill_topic_words',
                'fill_topic_truth',
                # Mock init to handle init call to superclass
                'Database.__init__',
                'Database.is_loaded',
                'Database.get_available',
            ),
        )


class TestInit(TestTopic):
    """
    Test initializing a topic
    """


    def test_setting_topic_name(self):
        """
        Test setting the topic_name
        """
        topic = Topic(topic_name='topic_name')
        self.assertEqual(topic.topic_name, 'topic_name')


    def test_unloaded_topic(self):
        """
        Test normal initialization with a topic that hasn't been loaded before
        """
        self.mock['Database.is_loaded'].return_value = False
        dummy = Topic(topic_name='topic_name')
        self.mock['fill_topic_words'].assert_called()
        self.mock['fill_topic_truth'].assert_called()


    def test_loaded_topic(self):
        """
        Test normal initialization with an already-loaded topic
        """
        self.mock['Database.is_loaded'].return_value = True
        dummy = Topic(topic_name='topic_name')
        self.mock['fill_topic_words'].assert_not_called()
        self.mock['fill_topic_truth'].assert_not_called()


class TestRepr(TestTopic):


    def test_repr(self):
        """
        Test the repr
        """
        actual_repr = repr(Topic(topic_name='topic_name'))
        expected_repr = "Topic(topic_name='topic_name')"
        self.assertEqual(actual_repr, expected_repr)


class TestGetAvailable(TestTopic):
    """
    Test Topic.get_available
    """


    def test_get_available(self):
        """
        Test getting the available topics
        """
        self.mock['Database.get_available'].return_value = [
            'first_topic',
            'second_topic',
        ]
        actual_available = Topic.get_available()
        expected_available = ['first_topic', 'second_topic']
        self.assertEqual(actual_available, expected_available)


class TestRandomTopicName(TestTopic):
    """
    Test Topic.random_topic_name
    """


    def test_random_topic_name(self):
        """
        Test getting a random topic name
        """
        self.mock['Database.get_available'].return_value = [
            'first_topic',
            'second_topic',
        ]
        actual_random_name = Topic.random_topic_name()
        expected_candidates = {'first_topic', 'second_topic'}
        self.assertIn(actual_random_name, expected_candidates)


    def test_determinate_random(self):
        """
        Test getting a random topic name with a random seed
        """
        self.mock['Database.get_available'].return_value = [
            'first_topic',
            'second_topic',
        ]
        actual_random_name = Topic.random_topic_name(random_seed=1)
        expected_random_name = 'first_topic'
        self.assertEqual(actual_random_name, expected_random_name)
