"""
test_truth_list.py
"""

from textwrap import dedent

from test_logic.util import TestUnit

from logic.categorical.util.truth_list import truth_list


FAKE_TOPIC = {
    'animals': {
        'singular': 'animal',
        'plural': 'animals',
        'truth': {
            'A': [],
            'E': [],
            'I': ['reptiles', 'mammals'],
            'O': [],
        },
    },
    'mammals': {
        'singular': 'mammal',
        'plural': 'mammals',
        'truth': {
            'A': ['animals'],
            'E': ['reptiles'],
            'I': ['animals'],
            'O': ['reptiles'],
        },
    },
    'reptiles': {
        'singular': 'reptile',
        'plural': 'reptiles',
        'truth': {
            'A': ['animals'],
            'E': ['mammals'],
            'I': ['animals'],
            'O': ['mammals'],
        },
    },
}

class TestTruthList(TestUnit):
    """
    Tests for logic.categorical.util.truth_list
    """


    def _test_case(self, actual_truth_list, expected_truth_list):
        sort_key = lambda item: (
            f"{item['proposition_type']}"
            f"{item['subject']}"
            f"{item['predicate']}"
        )
        self.assertEqual(
            sorted(actual_truth_list, key=sort_key),
            sorted(expected_truth_list, key=sort_key),
            dedent(f"""
                Failed to determine the correct truth list.
                Actual:
                {actual_truth_list}
                Expected:
                {expected_truth_list}
            """),
        )


    def test_full_list(self):
        """
        Test getting a full truth list from a topic
        """
        self._test_case(
            actual_truth_list=truth_list(FAKE_TOPIC),
            expected_truth_list=[
                {'proposition_type': 'I', 'subject': 'animals', 'predicate': 'reptiles'},
                {'proposition_type': 'I', 'subject': 'animals', 'predicate': 'mammals'},
                {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
                {'proposition_type': 'E', 'subject': 'mammals', 'predicate': 'reptiles'},
                {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
                {'proposition_type': 'O', 'subject': 'mammals', 'predicate': 'reptiles'},
                {'proposition_type': 'A', 'subject': 'reptiles', 'predicate': 'animals'},
                {'proposition_type': 'E', 'subject': 'reptiles', 'predicate': 'mammals'},
                {'proposition_type': 'I', 'subject': 'reptiles', 'predicate': 'animals'},
                {'proposition_type': 'O', 'subject': 'reptiles', 'predicate': 'mammals'},
            ],
        )


    def test_prop_type_a(self):
        """
        Test getting a truth list from a topic specifying proposition type 'A'
        """
        self._test_case(
            actual_truth_list=truth_list(
                FAKE_TOPIC,
                proposition_type='A',
            ),
            expected_truth_list=[
                {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
                {'proposition_type': 'A', 'subject': 'reptiles', 'predicate': 'animals'},
            ],
        )


    def test_prop_type_e(self):
        """
        Test getting a truth list from a topic specifying proposition type 'E'
        """
        self._test_case(
            actual_truth_list=truth_list(
                FAKE_TOPIC,
                proposition_type='E',
            ),
            expected_truth_list=[
                {'proposition_type': 'E', 'subject': 'mammals', 'predicate': 'reptiles'},
                {'proposition_type': 'E', 'subject': 'reptiles', 'predicate': 'mammals'},
            ],
        )


    def test_prop_type_i(self):
        """
        Test getting a truth list from a topic specifying proposition type 'I'
        """
        self._test_case(
            actual_truth_list=truth_list(
                FAKE_TOPIC,
                proposition_type='I',
            ),
            expected_truth_list=[
                {'proposition_type': 'I', 'subject': 'animals', 'predicate': 'reptiles'},
                {'proposition_type': 'I', 'subject': 'animals', 'predicate': 'mammals'},
                {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
                {'proposition_type': 'I', 'subject': 'reptiles', 'predicate': 'animals'},
            ],
        )


    def test_prop_type_o(self):
        """
        Test getting a truth list from a topic specifying proposition type 'O'
        """
        self._test_case(
            actual_truth_list=truth_list(
                FAKE_TOPIC,
                proposition_type='O',
            ),
            expected_truth_list=[
                {'proposition_type': 'O', 'subject': 'mammals', 'predicate': 'reptiles'},
                {'proposition_type': 'O', 'subject': 'reptiles', 'predicate': 'mammals'},
            ],
        )


    def test_subject(self):
        """
        Test getting a truth list from a topic specifying subject
        """
        self._test_case(
            actual_truth_list=truth_list(
                FAKE_TOPIC,
                subject='mammals',
            ),
            expected_truth_list=[
                {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
                {'proposition_type': 'E', 'subject': 'mammals', 'predicate': 'reptiles'},
                {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
                {'proposition_type': 'O', 'subject': 'mammals', 'predicate': 'reptiles'},
            ],
        )


    def test_predicate(self):
        """
        Test getting a truth list from a topic specifying predicate
        """
        self._test_case(
            actual_truth_list=truth_list(
                FAKE_TOPIC,
                predicate='animals',
            ),
            expected_truth_list=[
                {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
                {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
                {'proposition_type': 'A', 'subject': 'reptiles', 'predicate': 'animals'},
                {'proposition_type': 'I', 'subject': 'reptiles', 'predicate': 'animals'},
            ],
        )


    def test_combo_prop_type_subject(self):
        """
        Test getting a truth list from a topic specifying the proposition type and subject
        """
        self._test_case(
            actual_truth_list=truth_list(
                FAKE_TOPIC,
                proposition_type='A',
                subject='mammals',
            ),
            expected_truth_list=[
                {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
            ],
        )


    def test_empty(self):
        """
        Test getting a truth list from a topic specifying an impossible combo
        """
        self._test_case(
            actual_truth_list=truth_list(
                FAKE_TOPIC,
                proposition_type='A',
                subject='animals',
            ),
            expected_truth_list=[],
        )


    def test_bad_prop_type(self):
        """
        Test error for bad proposition type
        """
        with self.assertRaises(AssertionError):
            dummy = truth_list(FAKE_TOPIC, proposition_type='bad')


    def test_bad_subject(self):
        """
        Test error for bad subject key
        """
        with self.assertRaises(AssertionError):
            dummy = truth_list(FAKE_TOPIC, subject='bad')


    def test_bad_predicate(self):
        """
        Test error for bad predicate key
        """
        with self.assertRaises(AssertionError):
            dummy = truth_list(FAKE_TOPIC, predicate='bad')
