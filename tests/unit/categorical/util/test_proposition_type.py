"""
test_proposition_transformer.py
"""

from test_logic.util import TestUnit

from logic.categorical.util.proposition_type import (
    PROPOSITION_TYPES,
    negate_proposition_type,
    random_proposition_type,
)


class TestNegatePropositionType(TestUnit):
    """
    Tests for negate_proposition_type(proposition_type)
    """


    def test_negate_a(self):
        """
        Test negating a the proposition type A
        """
        self.assertEqual(negate_proposition_type('A'), 'O')


    def test_negate_e(self):
        """
        Test negating a the proposition type E
        """
        self.assertEqual(negate_proposition_type('E'), 'I')


    def test_negate_i(self):
        """
        Test negating a the proposition type I
        """
        self.assertEqual(negate_proposition_type('I'), 'E')


    def test_negate_o(self):
        """
        Test negating a the proposition type O
        """
        self.assertEqual(negate_proposition_type('O'), 'A')


    def test_bad_proposition_type(self):
        """
        Test getting an error for a bad proposition type
        """
        with self.assertRaises(KeyError):
            dummy = negate_proposition_type('bad')


class TestRandomPropositionType(TestUnit):
    """
    Tests for random_proposition_type()
    """


    def test_random_proposition_type(self):
        """
        Test getting a random proposition type
        """
        random_prop_type = random_proposition_type()
        self.assertIn(random_prop_type, PROPOSITION_TYPES)


    def test_determinate_random(self):
        """
        Test determining a random proposition type with a random seed
        """
        random_prop_type = random_proposition_type(random_seed=1)
        self.assertEqual(random_prop_type, 'E')
