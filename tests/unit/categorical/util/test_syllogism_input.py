"""
test_proposition_input.py
"""

from textwrap import dedent
from dataclasses import dataclass
from typing import Union

from test_logic.util import TestUnit

from logic.categorical.util.syllogism_input import (
    determine_syllogism_kwargs,
    categories_from_propositions,
    propositions_from_categories,
    soundness_from_validity_and_prems,
    random_propositions,
    get_term_location,
    prems_given_truth_and_type,
    get_form_validity_soundness,
)


@dataclass
class TruthOnlyFakeProposition:
    """
    Represents a categorical proposition where only truth value is relevant
    """
    truth_value: Union[bool, None]


@dataclass
class FakeProposition:
    """
    Fake categorical proposition
    """
    topic_name: str
    proposition_type: str
    subject: str
    predicate: str


class TestDetermineKwargs(TestUnit):
    """
    Test determining kwargs for `CategoricalSyllogism`
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.syllogism_input',
            to_mock=(
                'get_randseed',
                'random.seed',
                'Topic.random_topic_name',
                'propositions_from_categories',
                'categories_from_propositions',
                'get_form_validity_soundness',
                'random_propositions',
                'soundness_from_validity_and_prems',
                'CategoricalProposition',
                'Form.validate',
                'Form.from_propositions',
                'Form.random',
                'Form.is_valid',
            ),
        )
        self.mock['get_randseed'].return_value = 1
        self.mock['Topic.random_topic_name'].return_value = 'topic_name'
        self.mock['CategoricalProposition'].side_effect = FakeProposition


    def _test_case(self, actual_kwargs, expected_kwargs):
        self.assertEqual(
            sorted(list(actual_kwargs.keys())),
            sorted(list(expected_kwargs.keys())),
            dedent(f"""
                The kwargs do not have the correct keys.
                Actual:
                {repr(actual_kwargs.keys())}
                Expected:
                {repr(expected_kwargs.keys())}
            """),
        )
        for kwarg in expected_kwargs:
            self.assertEqual(
                actual_kwargs[kwarg],
                expected_kwargs[kwarg],
                dedent(f"""
                    Kwarg {repr(kwarg)} is incorrect.
                    Actual:
                    {repr(actual_kwargs[kwarg])}
                    Expected:
                    {repr(expected_kwargs[kwarg])}
                    Full actual:
                    {repr(actual_kwargs)}
                    Full expected:
                    {repr(expected_kwargs)}
                """),
            )


    def test_no_arguments(self):
        """
        Test getting syllogism kwargs from nothing
        """
        self.mock['get_form_validity_soundness'].return_value = 'AEI-1', True, True
        self.mock['Form.is_valid'].return_value = True
        self.mock['random_propositions'].return_value = {
            'major_premise': {
                'proposition_type': 'A',
                'subject': 'middle_term',
                'predicate': 'major_term',
            },
            'minor_premise': {
                'proposition_type': 'E',
                'subject': 'minor_term',
                'predicate': 'middle_term',
            },
            'conclusion': {
                'proposition_type': 'I',
                'subject': 'minor_term',
                'predicate': 'major_term',
            },
        }
        self.mock['categories_from_propositions'].return_value = {
            'major_term': 'major_term',
            'minor_term': 'minor_term',
            'middle_term': 'middle_term',
        }

        self._test_case(
            actual_kwargs=determine_syllogism_kwargs(),
            expected_kwargs={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='A',
                        subject='middle_term',
                        predicate='major_term',
                    ),
                    'minor_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='E',
                        subject='minor_term',
                        predicate='middle_term',
                    ),
                    'conclusion': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='I',
                        subject='minor_term',
                        predicate='major_term',
                    ),
                },
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
                'form': 'AEI-1',
                'is_valid': True,
                'is_sound': True,
            },
        )

        self.mock['get_randseed'].assert_called()


    def test_specifying_random_seed(self):
        """
        Test getting syllogism kwargs from with a specified random_seed
        """
        self.mock['get_form_validity_soundness'].return_value = 'AEI-1', True, True
        self.mock['Form.is_valid'].return_value = True
        self.mock['random_propositions'].return_value = {
            'major_premise': {
                'proposition_type': 'A',
                'subject': 'middle_term',
                'predicate': 'major_term',
            },
            'minor_premise': {
                'proposition_type': 'E',
                'subject': 'minor_term',
                'predicate': 'middle_term',
            },
            'conclusion': {
                'proposition_type': 'I',
                'subject': 'minor_term',
                'predicate': 'major_term',
            },
        }
        self.mock['categories_from_propositions'].return_value = {
            'major_term': 'major_term',
            'minor_term': 'minor_term',
            'middle_term': 'middle_term',
        }

        self._test_case(
            actual_kwargs=determine_syllogism_kwargs(**{
                'random_seed': 5,
            }),
            expected_kwargs={
                'random_seed': 5,
                'topic_name': 'topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='A',
                        subject='middle_term',
                        predicate='major_term',
                    ),
                    'minor_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='E',
                        subject='minor_term',
                        predicate='middle_term',
                    ),
                    'conclusion': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='I',
                        subject='minor_term',
                        predicate='major_term',
                    ),
                },
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
                'form': 'AEI-1',
                'is_valid': True,
                'is_sound': True,
            },
        )

        self.mock['get_randseed'].assert_not_called()


    def test_from_propositions(self):
        """
        Test getting syllogism kwargs from propositions
        """
        self.mock['Form.from_propositions'].return_value = 'AEI-1'
        self.mock['Form.is_valid'].return_value = True
        self.mock['soundness_from_validity_and_prems'].return_value = True
        self.mock['categories_from_propositions'].return_value = {
            'major_term': 'major_term',
            'minor_term': 'minor_term',
            'middle_term': 'middle_term',
        }

        self._test_case(
            actual_kwargs=determine_syllogism_kwargs(**{
                'propositions': {
                    'major_premise': {
                        'topic_name': 'prop_topic_name',
                        'proposition_type': 'A',
                        'subject': 'middle_term',
                        'predicate': 'major_term',
                    },
                    'minor_premise': {
                        'topic_name': 'prop_topic_name',
                        'proposition_type': 'E',
                        'subject': 'minor_term',
                        'predicate': 'middle_term',
                    },
                    'conclusion': {
                        'topic_name': 'prop_topic_name',
                        'proposition_type': 'I',
                        'subject': 'minor_term',
                        'predicate': 'major_term',
                    },
                },
            }),
            expected_kwargs={
                'random_seed': 1,
                'topic_name': 'prop_topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        topic_name='prop_topic_name',
                        proposition_type='A',
                        subject='middle_term',
                        predicate='major_term',
                    ),
                    'minor_premise': FakeProposition(
                        topic_name='prop_topic_name',
                        proposition_type='E',
                        subject='minor_term',
                        predicate='middle_term',
                    ),
                    'conclusion': FakeProposition(
                        topic_name='prop_topic_name',
                        proposition_type='I',
                        subject='minor_term',
                        predicate='major_term',
                    ),
                },
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
                'form': 'AEI-1',
                'is_valid': True,
                'is_sound': True,
            },
        )

        self.mock['get_randseed'].assert_called()
        self.mock['random_propositions'].assert_not_called()
        self.mock['Form.is_valid'].assert_called()
        self.mock['soundness_from_validity_and_prems'].assert_called()
        self.mock['categories_from_propositions'].assert_called()


    def test_from_categories(self):
        """
        Test getting syllogism kwargs from categories
        """
        self.mock['Form.random'].return_value = 'AEI-1'
        self.mock['Form.is_valid'].return_value = True
        self.mock['soundness_from_validity_and_prems'].return_value = True
        self.mock['propositions_from_categories'].return_value = {
            'major_premise': {
                'proposition_type': 'A',
                'subject': 'middle_term',
                'predicate': 'major_term',
            },
            'minor_premise': {
                'proposition_type': 'E',
                'subject': 'minor_term',
                'predicate': 'middle_term',
            },
            'conclusion': {
                'proposition_type': 'I',
                'subject': 'minor_term',
                'predicate': 'major_term',
            },
        }

        self._test_case(
            actual_kwargs=determine_syllogism_kwargs(**{
                'topic_name': 'topic_name',
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
            }),
            expected_kwargs={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='A',
                        subject='middle_term',
                        predicate='major_term',
                    ),
                    'minor_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='E',
                        subject='minor_term',
                        predicate='middle_term',
                    ),
                    'conclusion': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='I',
                        subject='minor_term',
                        predicate='major_term',
                    ),
                },
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
                'form': 'AEI-1',
                'is_valid': True,
                'is_sound': True,
            },
        )

        self.mock['get_randseed'].assert_called()
        self.mock['random_propositions'].assert_not_called()
        self.mock['Form.is_valid'].assert_called()
        self.mock['soundness_from_validity_and_prems'].assert_called()
        self.mock['propositions_from_categories'].assert_called()


    def test_from_categories_with_form(self):
        """
        Test getting syllogism kwargs from categories and form
        """
        self.mock['Form.is_valid'].return_value = True
        self.mock['soundness_from_validity_and_prems'].return_value = True
        self.mock['propositions_from_categories'].return_value = {
            'major_premise': {
                'proposition_type': 'A',
                'subject': 'middle_term',
                'predicate': 'major_term',
            },
            'minor_premise': {
                'proposition_type': 'E',
                'subject': 'minor_term',
                'predicate': 'middle_term',
            },
            'conclusion': {
                'proposition_type': 'I',
                'subject': 'minor_term',
                'predicate': 'major_term',
            },
        }

        self._test_case(
            actual_kwargs=determine_syllogism_kwargs(**{
                'topic_name': 'topic_name',
                'form': 'AEI-1',
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
            }),
            expected_kwargs={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='A',
                        subject='middle_term',
                        predicate='major_term',
                    ),
                    'minor_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='E',
                        subject='minor_term',
                        predicate='middle_term',
                    ),
                    'conclusion': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='I',
                        subject='minor_term',
                        predicate='major_term',
                    ),
                },
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
                'form': 'AEI-1',
                'is_valid': True,
                'is_sound': True,
            },
        )

        self.mock['get_randseed'].assert_called()
        self.mock['random_propositions'].assert_not_called()
        self.mock['Form.is_valid'].assert_called()
        self.mock['Form.random'].assert_not_called()
        self.mock['soundness_from_validity_and_prems'].assert_called()
        self.mock['propositions_from_categories'].assert_called()


    def test_from_categories_with_soundness(self):
        """
        Test getting syllogism kwargs from categories and soundness
        """
        self.mock['Form.random'].return_value = 'AEI-1'
        self.mock['Form.is_valid'].return_value = True
        self.mock['soundness_from_validity_and_prems'].return_value = True
        self.mock['propositions_from_categories'].return_value = {
            'major_premise': {
                'proposition_type': 'A',
                'subject': 'middle_term',
                'predicate': 'major_term',
            },
            'minor_premise': {
                'proposition_type': 'E',
                'subject': 'minor_term',
                'predicate': 'middle_term',
            },
            'conclusion': {
                'proposition_type': 'I',
                'subject': 'minor_term',
                'predicate': 'major_term',
            },
        }

        self._test_case(
            actual_kwargs=determine_syllogism_kwargs(**{
                'topic_name': 'topic_name',
                'is_sound': True,
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
            }),
            expected_kwargs={
                'random_seed': 1,
                'topic_name': 'topic_name',
                'propositions': {
                    'major_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='A',
                        subject='middle_term',
                        predicate='major_term',
                    ),
                    'minor_premise': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='E',
                        subject='minor_term',
                        predicate='middle_term',
                    ),
                    'conclusion': FakeProposition(
                        topic_name='topic_name',
                        proposition_type='I',
                        subject='minor_term',
                        predicate='major_term',
                    ),
                },
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
                'form': 'AEI-1',
                'is_valid': True,
                'is_sound': True,
            },
        )

        self.mock['get_randseed'].assert_called()
        self.mock['random_propositions'].assert_not_called()
        self.mock['Form.is_valid'].assert_not_called()
        self.mock['soundness_from_validity_and_prems'].assert_not_called()
        self.mock['propositions_from_categories'].assert_called()


    def test_missing_propositions(self):
        """
        Test error on some missing propositions
        """
        with self.assertRaises(ValueError):
            dummy = determine_syllogism_kwargs(**{
                'propositions': {
                    'minor_premise': {
                        'topic_name': 'prop_topic_name',
                        'proposition_type': 'E',
                        'subject': 'minor_term',
                        'predicate': 'middle_term',
                    },
                    'conclusion': {
                        'topic_name': 'prop_topic_name',
                        'proposition_type': 'I',
                        'subject': 'minor_term',
                        'predicate': 'major_term',
                    },
                },
            })


    def test_categories_missing_topic_name(self):
        """
        Test error on specifying categories without topic name
        """
        with self.assertRaises(ValueError):
            dummy = determine_syllogism_kwargs(**{
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
            })


class TestCategoriesFromPropositions(TestUnit):
    """
    Test determining categories from propositions
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.syllogism_input',
            to_mock='get_term_location',
        )


    def test_get_categories(self):
        """
        Test getting categories from legitimate propositions
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }
        actual_categories = categories_from_propositions(
            form='AEI-1',
            propositions={
                'major_premise': {
                    'proposition_type': 'A',
                    'subject': 'middle_term',
                    'predicate': 'major_term',
                },
                'minor_premise': {
                    'proposition_type': 'E',
                    'subject': 'minor_term',
                    'predicate': 'middle_term',
                },
                'conclusion': {
                    'proposition_type': 'I',
                    'subject': 'minor_term',
                    'predicate': 'major_term',
                },
            },
        )
        expected_categories = {
            'major_term': 'major_term',
            'minor_term': 'minor_term',
            'middle_term': 'middle_term',
        }
        self.assertEqual(
            actual_categories,
            expected_categories,
            dedent(f"""
                Failed to get categories from propositions.
                Actual:
                {repr(actual_categories)}
                Expected:
                {repr(expected_categories)}
            """),
        )


    def test_wrong_conc_subject(self):
        """
        Test error if the conclusion's subject is not the minor term
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }
        with self.assertRaises(ValueError):
            dummy = categories_from_propositions(
                form='AEI-1',
                propositions={
                    'major_premise': {
                        'proposition_type': 'A',
                        'subject': 'middle_term',
                        'predicate': 'major_term',
                    },
                    'minor_premise': {
                        'proposition_type': 'E',
                        'subject': 'minor_term',
                        'predicate': 'middle_term',
                    },
                    'conclusion': {
                        'proposition_type': 'I',
                        'subject': 'something mysterious',
                        'predicate': 'major_term',
                    },
                },
            )


    def test_wrong_conc_predicate(self):
        """
        Test error if the conclusion's predicate is not the major term
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }
        with self.assertRaises(ValueError):
            dummy = categories_from_propositions(
                form='AEI-1',
                propositions={
                    'major_premise': {
                        'proposition_type': 'A',
                        'subject': 'middle_term',
                        'predicate': 'major_term',
                    },
                    'minor_premise': {
                        'proposition_type': 'E',
                        'subject': 'minor_term',
                        'predicate': 'middle_term',
                    },
                    'conclusion': {
                        'proposition_type': 'I',
                        'subject': 'minor_term',
                        'predicate': 'something mysterious',
                    },
                },
            )


    def test_no_middle_term(self):
        """
        Test error if there is no middle term
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }
        with self.assertRaises(ValueError):
            dummy = categories_from_propositions(
                form='AEI-1',
                propositions={
                    'major_premise': {
                        'proposition_type': 'A',
                        'subject': 'middle_term',
                        'predicate': 'major_term',
                    },
                    'minor_premise': {
                        'proposition_type': 'E',
                        'subject': 'minor_term',
                        'predicate': 'something mysterious',
                    },
                    'conclusion': {
                        'proposition_type': 'I',
                        'subject': 'minor_term',
                        'predicate': 'major_term',
                    },
                },
            )


    def test_indistinct_major_minor_terms(self):
        """
        Test error if the major term and minor term are the same
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }
        with self.assertRaises(ValueError):
            dummy = categories_from_propositions(
                form='AEI-1',
                propositions={
                    'major_premise': {
                        'proposition_type': 'A',
                        'subject': 'middle_term',
                        'predicate': 'same term',
                    },
                    'minor_premise': {
                        'proposition_type': 'E',
                        'subject': 'same term',
                        'predicate': 'middle_term',
                    },
                    'conclusion': {
                        'proposition_type': 'I',
                        'subject': 'minor_term',
                        'predicate': 'major_term',
                    },
                },
            )


    def test_indistinct_major_middle_terms(self):
        """
        Test error if the major term and middle term are the same
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }
        with self.assertRaises(ValueError):
            dummy = categories_from_propositions(
                form='AEI-1',
                propositions={
                    'major_premise': {
                        'proposition_type': 'A',
                        'subject': 'middle_term',
                        'predicate': 'middle_term',
                    },
                    'minor_premise': {
                        'proposition_type': 'E',
                        'subject': 'minor_term',
                        'predicate': 'middle_term',
                    },
                    'conclusion': {
                        'proposition_type': 'I',
                        'subject': 'minor_term',
                        'predicate': 'major_term',
                    },
                },
            )


    def test_indistinct_minor_middle_terms(self):
        """
        Test error if the minor term and middle term are the same
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }
        with self.assertRaises(ValueError):
            dummy = categories_from_propositions(
                form='AEI-1',
                propositions={
                    'major_premise': {
                        'proposition_type': 'A',
                        'subject': 'middle_term',
                        'predicate': 'major_term',
                    },
                    'minor_premise': {
                        'proposition_type': 'E',
                        'subject': 'middle_term',
                        'predicate': 'middle_term',
                    },
                    'conclusion': {
                        'proposition_type': 'I',
                        'subject': 'minor_term',
                        'predicate': 'major_term',
                    },
                },
            )


class TestPropositionsFromCategories(TestUnit):
    """
    Test determining propositions from categories and form
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.syllogism_input',
            to_mock='get_term_location',
        )


    def _test_case(self, input_kwargs, expected_props):
        actual_props = propositions_from_categories(**input_kwargs)
        errs = []
        for prop_name in expected_props:
            if actual_props[prop_name] != expected_props[prop_name]:
                errs.append(
                    f"{repr(prop_name)} is incorrect. "
                    f"Actual: {repr(actual_props[prop_name])}. "
                    f"Expected: {repr(expected_props[prop_name])}. "
                    f"Input kwargs: {repr(input_kwargs)}."
                )
        self.assertEqual(errs, [], '\n'.join(errs))


    def test_getting_propositions(self):
        """
        Test getting random propositions given sound
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }
        self._test_case(
            input_kwargs={
                'form': 'AEI-1',
                'categories': {
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
            },
            expected_props={
                'major_premise': {
                    'proposition_type': 'A',
                    'subject': 'middle_term',
                    'predicate': 'major_term',
                },
                'minor_premise': {
                    'proposition_type': 'E',
                    'subject': 'minor_term',
                    'predicate': 'middle_term',
                },
                'conclusion': {
                    'proposition_type': 'I',
                    'subject': 'minor_term',
                    'predicate': 'major_term',
                },
            },
        )


    def test_missing_major_term(self):
        """
        Test that an error is raised when the major_term is missing
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }

        with self.assertRaises(ValueError):
            dummy = propositions_from_categories(
                form='AEI-1',
                categories={
                    'minor_term': 'minor_term',
                    'middle_term': 'middle_term',
                },
            )


    def test_missing_minor_term(self):
        """
        Test that an error is raised when the minor_term is missing
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }

        with self.assertRaises(ValueError):
            dummy = propositions_from_categories(
                form='AEI-1',
                categories={
                    'major_term': 'major_term',
                    'middle_term': 'middle_term',
                },
            )


    def test_missing_middle_term(self):
        """
        Test that an error is raised when the middle_term is missing
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
            'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
        }

        with self.assertRaises(ValueError):
            dummy = propositions_from_categories(
                form='AEI-1',
                categories={
                    'major_term': 'major_term',
                    'minor_term': 'minor_term',
                },
            )


class TestDetermineSoundness(TestUnit):
    """
    Test determining soundess from validity and premises
    """


    def _test_case(self, input_kwargs, expected_sound):
        actual_sound = soundness_from_validity_and_prems(**input_kwargs)
        self.assertEqual(
            actual_sound,
            expected_sound,
            dedent(f"""
                Failed to determine soundness from proposition kwargs.
                Actual: {repr(actual_sound)}
                Expected: {repr(expected_sound)}
                Input: {repr(input_kwargs)}
            """),
        )


    def test_sound(self):
        """
        Test determining soundness from the input
        """
        self._test_case(
            input_kwargs={
                'is_valid': True,
                'major_premise': TruthOnlyFakeProposition(True),
                'minor_premise': TruthOnlyFakeProposition(True),
            },
            expected_sound=True,
        )


    def test_unsound_invalid(self):
        """
        Test determining unsoundness from the input given invalidity
        """
        self._test_case(
            input_kwargs={
                'is_valid': False,
                'major_premise': TruthOnlyFakeProposition(True),
                'minor_premise': TruthOnlyFakeProposition(True),
            },
            expected_sound=False,
        )


    def test_unsound_false_prem(self):
        """
        Test determining unsoundness from the input given a false premise
        """
        self._test_case(
            input_kwargs={
                'is_valid': True,
                'major_premise': TruthOnlyFakeProposition(False),
                'minor_premise': TruthOnlyFakeProposition(None),
            },
            expected_sound=False,
        )


    def test_indeterminate(self):
        """
        Test determining indeterminate soundness from the input
        """
        self._test_case(
            input_kwargs={
                'is_valid': True,
                'major_premise': TruthOnlyFakeProposition(None),
                'minor_premise': TruthOnlyFakeProposition(True),
            },
            expected_sound=None,
        )


class TestRandomPropositions(TestUnit):
    """
    Test determining random propositions given soundness and form
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.syllogism_input',
            to_mock=(
                'get_term_location',
                'prems_given_truth_and_type',
                'itertools.product',
                'Topic',
                'random.shuffle',
                'Form.is_valid',
            ),
        )

        self.mock['itertools.product'].return_value = \
            [(True, True), (True, False), (False, True), (False, False)]


    def _test_case(self, input_kwargs, expected_props):
        actual_props = random_propositions(**input_kwargs)
        errs = []
        for prop_name in expected_props:
            if actual_props[prop_name] != expected_props[prop_name]:
                errs.append(
                    f"{repr(prop_name)} is incorrect. "
                    f"Actual: {repr(actual_props[prop_name])}. "
                    f"Expected: {repr(expected_props[prop_name])}. "
                    f"Input kwargs: {repr(input_kwargs)}."
                )
        self.assertEqual(errs, [], '\n'.join(errs))


    def test_sound(self):
        """
        Test getting random propositions given sound
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
        }
        self.mock['prems_given_truth_and_type'].return_value = [
            {
                'proposition_type': 'A',
                'subject': 'diamonds',
                'predicate': 'minerals',
            },
            {
                'proposition_type': 'E',
                'subject': 'rocks',
                'predicate': 'diamonds',
            },
        ]

        self._test_case(
            input_kwargs={
                'topic_name': 'topic_name',
                'is_sound': True,
                'form': 'AEI-1'
            },
            expected_props={
                'major_premise': {
                    'proposition_type': 'A',
                    'subject': 'diamonds',
                    'predicate': 'minerals',
                },
                'minor_premise': {
                    'proposition_type': 'E',
                    'subject': 'rocks',
                    'predicate': 'diamonds',
                },
                'conclusion': {
                    'proposition_type': 'I',
                    'subject': 'rocks',
                    'predicate': 'minerals',
                },
            },
        )

        self.mock['get_term_location'].assert_called()
        self.mock['prems_given_truth_and_type'].assert_called()
        self.mock['Form.is_valid'].assert_not_called()


    def test_unsound_valid(self):
        """
        Test getting random propositions given unsound and valid form
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
        }
        self.mock['prems_given_truth_and_type'].return_value = [
            {
                'proposition_type': 'A',
                'subject': 'diamonds',
                'predicate': 'minerals',
            },
            {
                'proposition_type': 'E',
                'subject': 'rocks',
                'predicate': 'diamonds',
            },
        ]
        self.mock['Form.is_valid'].return_value = True

        self._test_case(
            input_kwargs={
                'topic_name': 'topic_name',
                'is_sound': False,
                'form': 'AEI-1'
            },
            expected_props={
                'major_premise': {
                    'proposition_type': 'A',
                    'subject': 'diamonds',
                    'predicate': 'minerals',
                },
                'minor_premise': {
                    'proposition_type': 'E',
                    'subject': 'rocks',
                    'predicate': 'diamonds',
                },
                'conclusion': {
                    'proposition_type': 'I',
                    'subject': 'rocks',
                    'predicate': 'minerals',
                },
            },
        )

        self.mock['get_term_location'].assert_called()
        self.mock['prems_given_truth_and_type'].assert_called()
        self.mock['Form.is_valid'].assert_called()

    def test_invalid(self):
        """
        Test getting random propositions given invalid form
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
        }
        self.mock['prems_given_truth_and_type'].return_value = [
            {
                'proposition_type': 'A',
                'subject': 'diamonds',
                'predicate': 'minerals',
            },
            {
                'proposition_type': 'E',
                'subject': 'rocks',
                'predicate': 'diamonds',
            },
        ]
        self.mock['Form.is_valid'].return_value = False

        self._test_case(
            input_kwargs={
                'topic_name': 'topic_name',
                'is_sound': False,
                'form': 'AEI-1'
            },
            expected_props={
                'major_premise': {
                    'proposition_type': 'A',
                    'subject': 'diamonds',
                    'predicate': 'minerals',
                },
                'minor_premise': {
                    'proposition_type': 'E',
                    'subject': 'rocks',
                    'predicate': 'diamonds',
                },
                'conclusion': {
                    'proposition_type': 'I',
                    'subject': 'rocks',
                    'predicate': 'minerals',
                },
            },
        )

        self.mock['get_term_location'].assert_called()
        self.mock['prems_given_truth_and_type'].assert_called()
        self.mock['Form.is_valid'].assert_called()


    def test_not_found(self):
        """
        Test error on not finding propositions
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
        }
        self.mock['prems_given_truth_and_type'].return_value = None, None

        with self.assertRaises(ValueError):
            dummy = random_propositions(
                topic_name='topic_name',
                is_sound=True,
                form='AEI-1',
            )


    def test_no_unsound_valid_all_true(self):
        """
        Test we can't get a sound argument given unsound and valid form
        """
        self.mock['get_term_location'].return_value = {
            'major_term': {'major_premise': 'predicate'},
            'minor_term': {'minor_premise': 'subject'},
        }
        # We limit the truth values just to make sure we remove this one
        self.mock['itertools.product'].return_value = [(True, True)]
        self.mock['Form.is_valid'].return_value = True

        with self.assertRaises(ValueError):
            dummy = random_propositions(
                topic_name='topic_name',
                is_sound=False,
                form='AEI-1',
            )
        self.mock['prems_given_truth_and_type'].assert_not_called()


class TestTermLocation(TestUnit):
    """
    Test determining the location of categories given the figure
    """


    def _test_case(self, actual_term_location, expected_term_location):
        self.assertEqual(
            actual_term_location,
            expected_term_location,
            dedent(f"""
                Failed to determine the correct location of terms.
                Actual:
                {repr(actual_term_location)}
                Expected:
                {repr(expected_term_location)}
            """),
        )


    def test_figure_one(self):
        """
        Test providing the location of terms in premises for figure 1
        """
        self._test_case(
            actual_term_location=get_term_location(figure=1),
            expected_term_location={
                'major_term': {'major_premise': 'predicate'},
                'minor_term': {'minor_premise': 'subject'},
                'middle_term': {'major_premise': 'subject', 'minor_premise': 'predicate'},
            },
        )


    def test_figure_two(self):
        """
        Test providing the location of terms in premises for figure 2
        """
        self._test_case(
            actual_term_location=get_term_location(figure=2),
            expected_term_location={
                'major_term': {'major_premise': 'subject'},
                'minor_term': {'minor_premise': 'subject'},
                'middle_term': {'major_premise': 'predicate', 'minor_premise': 'predicate'},
            },
        )


    def test_figure_three(self):
        """
        Test providing the location of terms in premises for figure 3
        """
        self._test_case(
            actual_term_location=get_term_location(figure=3),
            expected_term_location={
                'major_term': {'major_premise': 'predicate'},
                'minor_term': {'minor_premise': 'predicate'},
                'middle_term': {'major_premise': 'subject', 'minor_premise': 'subject'},
            },
        )


    def test_figure_four(self):
        """
        Test providing the location of terms in premises for figure 4
        """
        self._test_case(
            actual_term_location=get_term_location(figure=4),
            expected_term_location={
                'major_term': {'major_premise': 'subject'},
                'minor_term': {'minor_premise': 'predicate'},
                'middle_term': {'major_premise': 'predicate', 'minor_premise': 'subject'},
            },
        )


    def test_illegitimate_figure(self):
        """
        Test error on illegitimate figure
        """
        with self.assertRaises(ValueError):
            dummy = get_term_location(figure='1')


class TestPremsFromTruthAndType(TestUnit):
    """
    Test determining premises from truth values and proposition types
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.syllogism_input',
            to_mock=(
                'random.shuffle',
                'negate_proposition_type',
                'truth_list',
            ),
        )

        self.mock['negate_proposition_type'].side_effect = \
            lambda prop_type: {'A': 'O', 'E': 'I', 'I': 'E', 'O': 'A'}[prop_type]


    def _test_case(self, input_kwargs, expected):
        actual_major_premise, actual_minor_premise = prems_given_truth_and_type(**input_kwargs)
        errs = []
        if actual_major_premise != expected['major_premise']:
            errs.append(
                f"Actual major premise: {repr(actual_major_premise)}. "
                f"Expected major premise: {repr(expected['major_premise'])}. "
            )
        if actual_minor_premise != expected['minor_premise']:
            errs.append(
                f"Actual minor premise: {repr(actual_minor_premise)}. "
                f"Expected minor premise: {repr(expected['minor_premise'])}. "
            )
        self.assertEqual(errs, [], '\n'.join(errs))


    def test_finding_all_true(self):
        """
        Test getting with all true premises
        """
        self.mock['truth_list'].side_effect = [
            # Major premise candidates:
            [{'proposition_type': 'E', 'subject': 'mammals', 'predicate': 'reptiles'}],
            # Minor premise candidates:
            [{'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'}],
        ]
        self._test_case(
            input_kwargs={
                'topic': 'topic',
                'prop_truth': {'major': True, 'minor': True},
                'prop_types': {'major': 'E', 'minor': 'A'},
                'term_location': {
                    'middle_term': {'major_premise': 'subject', 'minor_premise': 'subject'},
                    'major_term': {'major_premise': 'predicate'},
                    'minor_term': {'minor_premise': 'predicate'},
                },
            },
            expected={
                'major_premise': {
                    'proposition_type': 'E',
                    'subject': 'mammals',
                    'predicate': 'reptiles',
                },
                'minor_premise': {
                    'proposition_type': 'A',
                    'subject': 'mammals',
                    'predicate': 'animals',
                },
            },
        )
        self.mock['negate_proposition_type'].assert_not_called()


    def test_finding_some_false(self):
        """
        Test getting with some false premises
        """
        self.mock['truth_list'].side_effect = [
            # Major premise candidates:
            [{'proposition_type': 'E', 'subject': 'mammals', 'predicate': 'reptiles'}],
            # Minor premise candidates:
            [{'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'}],
        ]
        self._test_case(
            input_kwargs={
                'topic': 'topic',
                'prop_truth': {'major': False, 'minor': False},
                'prop_types': {'major': 'I', 'minor': 'O'},
                'term_location': {
                    'middle_term': {'major_premise': 'subject', 'minor_premise': 'subject'},
                    'major_term': {'major_premise': 'predicate'},
                    'minor_term': {'minor_premise': 'predicate'},
                },
            },
            expected={
                'major_premise': {
                    'proposition_type': 'I',
                    'subject': 'mammals',
                    'predicate': 'reptiles',
                },
                'minor_premise': {
                    'proposition_type': 'O',
                    'subject': 'mammals',
                    'predicate': 'animals',
                },
            },
        )
        self.mock['negate_proposition_type'].assert_called()


    def test_not_found_major(self):
        """
        Test not finding premises with no major premise candidates
        """
        self.mock['truth_list'].side_effect = [
            # Major premise candidates:
            [],
            # Minor premise candidates:
            [{'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'}],
        ]
        self._test_case(
            input_kwargs={
                'topic': 'topic',
                'prop_truth': {'major': False, 'minor': False},
                'prop_types': {'major': 'I', 'minor': 'O'},
                'term_location': {
                    'middle_term': {'major_premise': 'subject', 'minor_premise': 'subject'},
                    'major_term': {'major_premise': 'predicate'},
                    'minor_term': {'minor_premise': 'predicate'},
                },
            },
            expected={
                'major_premise': None,
                'minor_premise': None,
            },
        )


    def test_not_found_minor(self):
        """
        Test not finding premises with no minor premise candidates
        """
        self.mock['truth_list'].side_effect = [
            # Major premise candidates:
            [{'proposition_type': 'E', 'subject': 'mammals', 'predicate': 'reptiles'}],
            # Minor premise candidates:
            [],
        ]
        self._test_case(
            input_kwargs={
                'topic': 'topic',
                'prop_truth': {'major': False, 'minor': False},
                'prop_types': {'major': 'I', 'minor': 'O'},
                'term_location': {
                    'middle_term': {'major_premise': 'subject', 'minor_premise': 'subject'},
                    'major_term': {'major_premise': 'predicate'},
                    'minor_term': {'minor_premise': 'predicate'},
                },
            },
            expected={
                'major_premise': None,
                'minor_premise': None,
            },
        )


    def test_category_cycle(self):
        """
        Test eliminating a category cycle
        """
        self.mock['truth_list'].side_effect = [
            # Major premise candidates:
            [{'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'}],
            # Minor premise candidates:
            [{'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'}],
        ]
        self._test_case(
            input_kwargs={
                'topic': 'topic',
                'prop_truth': {'major': False, 'minor': False},
                'prop_types': {'major': 'A', 'minor': 'I'},
                'term_location': {
                    'middle_term': {'major_premise': 'subject', 'minor_premise': 'subject'},
                    'major_term': {'major_premise': 'predicate'},
                    'minor_term': {'minor_premise': 'predicate'},
                },
            },
            expected={
                'major_premise': None,
                'minor_premise': None,
            },
        )


class TestFormValiditySoundness(TestUnit):
    """
    Test determining random form, validity, and soundness
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.syllogism_input',
            to_mock=(
                'randbool',
                'random.random',
                'Form.random',
                'Form.is_valid',
            ),
        )


    def _test_case(self, input_kwargs, expected):
        actual = {}
        actual['form'], actual['is_valid'], actual['is_sound'] = \
            get_form_validity_soundness(**input_kwargs)
        errs = []
        for item in actual:
            if not actual[item] == expected[item]:
                errs.append(
                    f"Expected {item} {repr(expected[item])} but got {repr(actual[item])}."
                )
        self.assertEqual(errs, [], '\n'.join(errs))


    def test_valid_form_set_sound(self):
        """
        Test setting sound and valid given a valid form
        """
        self.mock['Form.is_valid'].return_value = True
        self.mock['randbool'].return_value = True
        self._test_case(
            input_kwargs={
                'form': 'AAA-1',
                'is_valid': None,
                'is_sound': None,
            },
            expected={
                'form': 'AAA-1',
                'is_valid': True,
                'is_sound': True,
            }
        )


    def test_valid_form_set_unsound(self):
        """
        Test setting unsound and valid given a valid form
        """
        self.mock['Form.is_valid'].return_value = True
        self.mock['randbool'].return_value = False
        self._test_case(
            input_kwargs={
                'form': 'AAA-1',
                'is_valid': None,
                'is_sound': None,
            },
            expected={
                'form': 'AAA-1',
                'is_valid': True,
                'is_sound': False,
            }
        )


    def test_invalid_form_set_unsound(self):
        """
        Test setting unsound and invalid given an invalid form
        """
        self.mock['Form.is_valid'].return_value = False
        self.mock['randbool'].return_value = False
        self._test_case(
            input_kwargs={
                'form': 'EEE-1',
                'is_valid': None,
                'is_sound': None,
            },
            expected={
                'form': 'EEE-1',
                'is_valid': False,
                'is_sound': False,
            }
        )


    def test_form_soundness_conflict(self):
        """
        Test getting an error with an invalid form and pre-set sound
        """
        self.mock['Form.is_valid'].return_value = False
        self.mock['randbool'].return_value = False
        with self.assertRaises(ValueError):
            dummy1, dummy2, dummy3 = get_form_validity_soundness(
                form='EEE-1',
                is_valid=None,
                is_sound=True,
            )


    def test_valid_sound(self):
        """
        Test setting sound given pre-set valid
        """
        self.mock['Form.random'].return_value = 'AAA-1'
        self.mock['randbool'].return_value = True
        self._test_case(
            input_kwargs={
                'form': None,
                'is_valid': True,
                'is_sound': None,
            },
            expected={
                'form': 'AAA-1',
                'is_valid': True,
                'is_sound': True,
            }
        )


    def test_valid_unsound(self):
        """
        Test setting unsound given pre-set valid
        """
        self.mock['Form.random'].return_value = 'AAA-1'
        self.mock['randbool'].return_value = False
        self._test_case(
            input_kwargs={
                'form': None,
                'is_valid': True,
                'is_sound': None,
            },
            expected={
                'form': 'AAA-1',
                'is_valid': True,
                'is_sound': False,
            }
        )


    def test_invalid(self):
        """
        Test setting unsound given pre-set invalid
        """
        self.mock['Form.random'].return_value = 'EEE-1'
        self._test_case(
            input_kwargs={
                'form': None,
                'is_valid': False,
                'is_sound': None,
            },
            expected={
                'form': 'EEE-1',
                'is_valid': False,
                'is_sound': False,
            }
        )


    def test_random_sound(self):
        """
        Test setting sound valid given nothing
        """
        self.mock['Form.random'].return_value = 'AAA-1'
        self.mock['random.random'].return_value = 0.1
        self._test_case(
            input_kwargs={
                'form': None,
                'is_valid': None,
                'is_sound': None,
            },
            expected={
                'form': 'AAA-1',
                'is_valid': True,
                'is_sound': True,
            }
        )


    def test_random_unsound_valid(self):
        """
        Test setting unsound valid given nothing
        """
        self.mock['Form.random'].return_value = 'AAA-1'
        self.mock['random.random'].return_value = 0.9
        self.mock['randbool'].return_value = True
        self._test_case(
            input_kwargs={
                'form': None,
                'is_valid': None,
                'is_sound': None,
            },
            expected={
                'form': 'AAA-1',
                'is_valid': True,
                'is_sound': False,
            }
        )


    def test_random_unsound_invalid(self):
        """
        Test setting unsound invalid given nothing
        """
        self.mock['Form.random'].return_value = 'EEE-1'
        self.mock['random.random'].return_value = 0.9
        self.mock['randbool'].return_value = False
        self._test_case(
            input_kwargs={
                'form': None,
                'is_valid': None,
                'is_sound': None,
            },
            expected={
                'form': 'EEE-1',
                'is_valid': False,
                'is_sound': False,
            }
        )
