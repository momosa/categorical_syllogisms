"""
test_form.py

Test operations on categorical syllogism forms
"""

from textwrap import dedent
from dataclasses import dataclass

from test_logic.util import TestUnit

from logic.categorical.util.form import Form


@dataclass
class FakeProposition:
    """
    Class representing a categorical proposition.
    """
    proposition_type: str
    subject: str
    predicate: str


class TestForm(TestUnit):
    """
    Base class for testing Form
    """

    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.form',
            to_mock=(
                'get_randseed',
                'randbool',
            ),
        )
        self.mock['get_randseed'].return_value = 1
        self.mock['randbool'].return_value = True


class TestNoInit(TestForm):
    """
    Test that Form objects can't be instantiated
    """

    def test_no_init(self):
        """
        Test that Form objects can't be instantiated
        """
        with self.assertRaises(AttributeError):
            dummy = Form()


class TestValidate(TestForm):
    """
    Test Form.validate(form)
    """


    def test_good_form(self):
        """
        Test that validation passes on good forms
        """
        self.assertIsNone(Form.validate('AAA-1'))
        self.assertIsNone(Form.validate('AEI-2'))
        self.assertIsNone(Form.validate('OEO-3'))


    def test_bad_prop_type(self):
        """
        Test that validation fails on bad proposition types
        """
        with self.assertRaises(ValueError):
            Form.validate('CAT-1')
        with self.assertRaises(ValueError):
            Form.validate('123-1')
        with self.assertRaises(ValueError):
            Form.validate('aei-1')


    def test_bad_figure(self):
        """
        Test that validation fails on bad figure
        """
        with self.assertRaises(ValueError):
            Form.validate('AAA-5')
        with self.assertRaises(ValueError):
            Form.validate('AAA-A')


    def test_short_mood(self):
        """
        Test that validation fails when the mood is short
        """
        with self.assertRaises(ValueError):
            Form.validate('AA-1')


    def test_long_mood(self):
        """
        Test that validation fails when the mood is long
        """
        with self.assertRaises(ValueError):
            Form.validate('AAAA-1')


    def test_bad_dash(self):
        """
        Test that validation fails when the dash is missing or wrong
        """
        with self.assertRaises(ValueError):
            Form.validate('AAA1')
        with self.assertRaises(ValueError):
            Form.validate('AAA_1')
        with self.assertRaises(ValueError):
            Form.validate('AAA 1')


class TestFromPropositions(TestForm):
    """
    Test Form.from_propositions(major_premise, minor_premise, conclusion)
    """


    def _test_case(self, input_kwargs, expected_form):
        actual_form = Form.from_propositions(**input_kwargs)
        self.assertEqual(
            actual_form,
            expected_form,
            dedent(f"""
                Failed to get the correct form from the propositions.
                Actual: {repr(actual_form)}
                Expected: {repr(expected_form)}
                Major premise:
                {repr(input_kwargs['major_premise'])}
                Minor premise:
                {repr(input_kwargs['minor_premise'])}
                Conclusion:
                {repr(input_kwargs['conclusion'])}
            """),
        )


    def test_figure_one(self):
        """
        Test getting figure 1 from propositions
        """
        self._test_case(
            input_kwargs={
                'major_premise': FakeProposition(
                    proposition_type='A',
                    subject='middle term',
                    predicate='major term',
                ),
                'minor_premise': FakeProposition(
                    proposition_type='E',
                    subject='minor term',
                    predicate='middle term',
                ),
                'conclusion': FakeProposition(
                    proposition_type='I',
                    subject='major term',
                    predicate='minor term',
                ),
            },
            expected_form='AEI-1',
        )


    def test_figure_two(self):
        """
        Test getting figure 2 from propositions
        """
        self._test_case(
            input_kwargs={
                'major_premise': FakeProposition(
                    proposition_type='O',
                    subject='major term',
                    predicate='middle term',
                ),
                'minor_premise': FakeProposition(
                    proposition_type='A',
                    subject='minor term',
                    predicate='middle term',
                ),
                'conclusion': FakeProposition(
                    proposition_type='E',
                    subject='minor term',
                    predicate='major term',
                ),
            },
            expected_form='OAE-2',
        )


    def test_figure_three(self):
        """
        Test getting figure 3 from propositions
        """
        self._test_case(
            input_kwargs={
                'major_premise': FakeProposition(
                    proposition_type='I',
                    subject='middle term',
                    predicate='major term',
                ),
                'minor_premise': FakeProposition(
                    proposition_type='E',
                    subject='middle term',
                    predicate='minor term',
                ),
                'conclusion': FakeProposition(
                    proposition_type='O',
                    subject='minor term',
                    predicate='major term',
                ),
            },
            expected_form='IEO-3',
        )


    def test_figure_four(self):
        """
        Test getting figure 4 from propositions
        """
        self._test_case(
            input_kwargs={
                'major_premise': FakeProposition(
                    proposition_type='E',
                    subject='major term',
                    predicate='middle term',
                ),
                'minor_premise': FakeProposition(
                    proposition_type='A',
                    subject='middle term',
                    predicate='minor term',
                ),
                'conclusion': FakeProposition(
                    proposition_type='O',
                    subject='minor term',
                    predicate='major term',
                ),
            },
            expected_form='EAO-4',
        )


    def test_bad_input(self):
        """
        Test getting an error on bad input
        """
        with self.assertRaises(ValueError):
            dummy = Form.from_propositions(
                major_premise=FakeProposition(
                    proposition_type='E',
                    subject='major term',
                    predicate='no middle term',
                ),
                minor_premise=FakeProposition(
                    proposition_type='A',
                    subject='still no middle term',
                    predicate='minor term',
                ),
                conclusion=FakeProposition(
                    proposition_type='O',
                    subject='minor term',
                    predicate='major term',
                ),
            )


class TestRandom(TestForm):
    """
    Test Form.random()
    """


    def test_random_valid(self):
        """
        Test getting a random valid form
        """
        actual_form = Form.random(is_valid=True)
        self.mock['get_randseed'].assert_called()
        expected_form = 'OAO-3'
        self.assertEqual(actual_form, expected_form)


    def test_random_invalid(self):
        """
        Test getting a random invalid form
        """
        actual_form = Form.random(is_valid=False)
        self.mock['get_randseed'].assert_called()
        expected_form = 'AIA-4'
        self.assertEqual(actual_form, expected_form)


    def test_random_seed(self):
        """
        Test getting a random form with a random seed
        """
        actual_form = Form.random(random_seed=1)
        self.mock['get_randseed'].assert_not_called()
        expected_form = 'OAO-3'
        self.assertEqual(actual_form, expected_form)


class TestIsValid(TestForm):
    """
    Test Form.is_valid(form)
    """


    def test_valid_forms(self):
        """
        Test assessing validity of valid forms
        """
        self.assertTrue(Form.is_valid('AAA-1'))
        self.assertTrue(Form.is_valid('AEE-2'))
        self.assertTrue(Form.is_valid('EAE-2'))
        self.assertTrue(Form.is_valid('EIO-4'))


    def test_invalid_forms(self):
        """
        Test assessing validity of invalid forms
        """
        self.assertFalse(Form.is_valid('AAA-2'))
        self.assertFalse(Form.is_valid('IEA-1'))
        self.assertFalse(Form.is_valid('EEA-4'))
        self.assertFalse(Form.is_valid('EAO-3'))


    def test_bad_form(self):
        """
        Test validity assessment yields error on illegit form
        """
        with self.assertRaises(ValueError):
            dummy = Form.is_valid('bad')
