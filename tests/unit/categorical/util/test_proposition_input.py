"""
test_proposition_input.py
"""

from textwrap import dedent

from test_logic.util import TestUnit

from logic.categorical.util.proposition_input import determine_proposition_kwargs


# No need for linguistic elements 'singular' and 'plural'
FAKE_TOPIC = {
    'animals': {'truth': {
        'A': [],
        'E': [],
        'I': ['reptiles', 'mammals'],
        'O': ['reptiles', 'mammals'],
    }},
    'mammals': {'truth': {
        'A': ['animals'],
        'E': ['reptiles'],
        'I': ['animals'],
        'O': ['reptiles'],
    }},
    'reptiles': {'truth': {
        'A': ['animals'],
        'E': ['mammals'],
        'I': ['animals'],
        'O': ['mammals'],
    }},
}

def fake_truth_list(topic, proposition_type, subject, predicate):
    """ Fake generate a truth list """
    dummy = topic
    result_map = {
        # proposition_type, subject, predicate
        (None, None, None): [
            {'proposition_type': 'I', 'subject': 'animals', 'predicate': 'reptiles'},
            {'proposition_type': 'I', 'subject': 'animals', 'predicate': 'mammals'},
            {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'E', 'subject': 'mammals', 'predicate': 'reptiles'},
            {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'O', 'subject': 'mammals', 'predicate': 'reptiles'},
            {'proposition_type': 'A', 'subject': 'reptiles', 'predicate': 'animals'},
            {'proposition_type': 'E', 'subject': 'reptiles', 'predicate': 'mammals'},
            {'proposition_type': 'I', 'subject': 'reptiles', 'predicate': 'animals'},
            {'proposition_type': 'O', 'subject': 'reptiles', 'predicate': 'mammals'},
        ],

        # proposition_type, subject, predicate
        ('A', None, None): [
            {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'A', 'subject': 'reptiles', 'predicate': 'animals'},
        ],

        # proposition_type, subject, predicate
        ('O', None, None): [
            {'proposition_type': 'O', 'subject': 'mammals', 'predicate': 'reptiles'},
            {'proposition_type': 'O', 'subject': 'reptiles', 'predicate': 'mammals'},
        ],

        # proposition_type, subject, predicate
        (None, 'mammals', None): [
            {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'E', 'subject': 'mammals', 'predicate': 'reptiles'},
            {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'O', 'subject': 'mammals', 'predicate': 'reptiles'},
        ],

        # proposition_type, subject, predicate
        (None, None, 'animals'): [
            {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'A', 'subject': 'reptiles', 'predicate': 'animals'},
            {'proposition_type': 'I', 'subject': 'reptiles', 'predicate': 'animals'},
        ],

        # proposition_type, subject, predicate
        ('A', 'mammals', None): [
            {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
        ],

        # proposition_type, subject, predicate
        ('I', None, 'animals'): [
            {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'I', 'subject': 'reptiles', 'predicate': 'animals'},
        ],

        # proposition_type, subject, predicate
        (None, 'mammals', 'animals'): [
            {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
            {'proposition_type': 'I', 'subject': 'mammals', 'predicate': 'animals'},
        ],

        # proposition_type, subject, predicate
        ('A', 'mammals', 'animals'): [
            {'proposition_type': 'A', 'subject': 'mammals', 'predicate': 'animals'},
        ],

        # proposition_type, subject, predicate
        ('A', 'animals', 'mammals'): [],

    }
    return result_map[(proposition_type, subject, predicate)]


class TestDetermineKwargs(TestUnit):
    """
    Base test class for determining kwargs for `CategoricalProposition`
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.proposition_input',
            to_mock=(
                'get_randseed',
                'randbool',
                'negate_proposition_type',
                'random_proposition_type',
                'Topic',
                'Topic.random_topic_name',
                'truth_list',
            ),
        )

        self.mock['get_randseed'].return_value = 1
        self.mock['randbool'].return_value = True
        self.mock['negate_proposition_type'].side_effect = \
            lambda prop_type: {'A': 'O', 'E': 'I', 'I': 'E', 'O': 'A'}[prop_type]
        self.mock['random_proposition_type'].return_value = 'A'
        self.mock['Topic'].return_value = FAKE_TOPIC
        self.mock['Topic.random_topic_name'].return_value = 'topic_name'
        self.mock['truth_list'].side_effect = fake_truth_list


    def _test_case(self, actual_kwargs, expected_kwargs):
        self.assertEqual(
            actual_kwargs,
            expected_kwargs,
            dedent(f"""
                Failed to determine the correct kwargs.
                Actual:
                {repr(actual_kwargs)}
                Expected:
                {repr(expected_kwargs)}
            """),
        )


class TestGivenTruth(TestDetermineKwargs):
    """
    Test determining categorical proposition kwargs given truth value
    """


    def test_true(self):
        """
        Test determining a true categorical proposition's kwargs with no other specs
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'truth_value': True,
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_false(self):
        """
        Test determining a false categorical proposition's kwargs with no other specs
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'truth_value': False,
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'O',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_true_prop_type(self):
        """
        Test determining a true categorical proposition's kwargs with a proposition_type
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'truth_value': True,
                'proposition_type': 'A',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_false_prop_type(self):
        """
        Test determining a false categorical proposition's kwargs with a proposition_type
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'truth_value': False,
                'proposition_type': 'A',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'reptiles',
            },
        )


    def test_true_subject(self):
        """
        Test determining a true categorical proposition's kwargs with a subject
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': True,
                'subject': 'mammals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'E',
                'subject': 'mammals',
                'predicate': 'reptiles',
            },
        )


    def test_false_subject(self):
        """
        Test determining a false categorical proposition's kwargs with a subject
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': False,
                'subject': 'mammals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'I',
                'subject': 'mammals',
                'predicate': 'reptiles',
            },
        )


    def test_true_predicate(self):
        """
        Test determining a true categorical proposition's kwargs with a predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': True,
                'predicate': 'animals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'I',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_false_predicate(self):
        """
        Test determining a false categorical proposition's kwargs with a predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': False,
                'predicate': 'animals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'E',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_true_prop_type_subject(self):
        """
        Test determining a true categorical proposition's kwargs with prop type and subject
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_false_prop_type_subject(self):
        """
        Test determining a false categorical proposition's kwargs with prop type and subject
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': False,
                'proposition_type': 'O',
                'subject': 'mammals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'O',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_true_prop_type_predicate(self):
        """
        Test determining a true categorical proposition's kwargs with prop type and predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': True,
                'proposition_type': 'I',
                'predicate': 'animals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'I',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_false_prop_type_predicate(self):
        """
        Test determining a false categorical proposition's kwargs with prop type and predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': False,
                'proposition_type': 'E',
                'predicate': 'animals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'E',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_true_subject_predicate(self):
        """
        Test determining a true categorical proposition's kwargs with subject and predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': True,
                'subject': 'mammals',
                'predicate': 'animals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_false_subject_predicate(self):
        """
        Test determining a false categorical proposition's kwargs with a subject and predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': False,
                'subject': 'mammals',
                'predicate': 'animals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'O',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_full_spec_true(self):
        """
        Test determining a true categorical proposition's kwargs with a full spec
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_full_spec_false(self):
        """
        Test determining a false categorical proposition's kwargs with a full spec
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'O',
                'subject': 'mammals',
                'predicate': 'animals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'O',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


class TestWithoutTruth(TestDetermineKwargs):
    """
    Test determining categorical proposition kwargs not given truth value
    """


    def test_no_spec(self):
        """
        Test determining categorical proposition kwargs with no input
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_prop_type(self):
        """
        Test determining categorical proposition kwargs with a proposition type
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'proposition_type': 'A',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'A',
                'subject': 'animals',
                'predicate': 'reptiles',
            },
        )


    def test_subject(self):
        """
        Test determining categorical proposition kwargs with a subject
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'subject': 'mammals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_predicate(self):
        """
        Test determining categorical proposition kwargs with a predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'predicate': 'reptiles',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'A',
                'subject': 'animals',
                'predicate': 'reptiles',
            },
        )


    def test_subject_and_predicate(self):
        """
        Test determining categorical proposition kwargs with subject and predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'subject': 'mammals',
                'predicate': 'reptiles',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': False,
                'proposition_type': 'A',
                'subject': 'mammals',
                'predicate': 'reptiles',
            },
        )


    def test_prop_type_and_subject(self):
        """
        Test determining categorical proposition kwargs with proposition type and subject
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'proposition_type': 'I',
                'subject': 'mammals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'I',
                'subject': 'mammals',
                'predicate': 'animals',
            },
        )


    def test_prop_type_and_predicate(self):
        """
        Test determining categorical proposition kwargs with proposition type and predicate
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'proposition_type': 'O',
                'predicate': 'mammals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'O',
                'subject': 'animals',
                'predicate': 'mammals',
            },
        )


    def test_full_spec(self):
        """
        Test determining categorical proposition kwargs with everything except truth value
        """
        self._test_case(
            actual_kwargs=determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'proposition_type': 'O',
                'subject': 'reptiles',
                'predicate': 'mammals',
            }),
            expected_kwargs={
                'topic_name': 'topic_name',
                'random_seed': 1,
                'truth_value': True,
                'proposition_type': 'O',
                'subject': 'reptiles',
                'predicate': 'mammals',
            },
        )


class TestValidation(TestDetermineKwargs):
    """
    Test validation errors are raised when determining a categorical proposition's kwargs
    """


    def test_subject_without_topic(self):
        """
        Test error raised when a subject is specified without a topic name
        """
        with self.assertRaises(AssertionError):
            dummy = determine_proposition_kwargs(**{
                'subject': 'mammals',
            })


    def test_predicate_without_topic(self):
        """
        Test error raised when a subject is specified without a topic name
        """
        with self.assertRaises(AssertionError):
            dummy = determine_proposition_kwargs(**{
                'predicate': 'mammals',
            })


    def test_same_subject_predicate(self):
        """
        Test error raised when the subject and predicate are equal
        """
        with self.assertRaises(ValueError):
            dummy = determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'subject': 'mammals',
                'predicate': 'mammals',
            })


    def test_irresolvable_true(self):
        """
        Test error raised with an irresolvable True truth value
        """
        with self.assertRaises(ValueError):
            dummy = determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': True,
                'proposition_type': 'A',
                'subject': 'animals',
                'predicate': 'mammals',
            })


    def test_irresolvable_false(self):
        """
        Test error raised with an irresolvable False truth value
        """
        with self.assertRaises(ValueError):
            dummy = determine_proposition_kwargs(**{
                'topic_name': 'topic_name',
                'truth_value': False,
                'proposition_type': 'O',
                'subject': 'animals',
                'predicate': 'mammals',
            })
