"""
test_fill_topic.py

Test logic.categorical.util.fill_topic

Currently only testing nouns.
Not yet testing partial truth.
"""

from textwrap import dedent

from test_logic.util import TestUnit

from logic.categorical.util.fill_topic import (
    fill_topic_words,
    fill_topic_truth,
)


class TestFillTopicWords(TestUnit):
    """
    Test fill_topic_words
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.fill_topic',
            to_mock='get_plural_noun',
        )


    def test_fill_topic_words(self):
        """
        Test that fill_topic_words creates plural forms
        """
        self.mock['get_plural_noun'].return_value = 'humans'

        topic = {
            'humans': {'singular': 'human'},
            'deer': {'singular': 'deer', 'plural': 'deer'},
        }
        expected = {
            'humans': {'singular': 'human', 'plural': 'humans'},
            'deer': {'singular': 'deer', 'plural': 'deer'},
        }
        fill_topic_words(topic)
        self.assertEqual(topic, expected)


class TestFillTopicTruth(TestUnit):
    """
    Sets up the test case for fill_topic_truth
    """


    topic = {}
    expected = {}

    def _test_case(self, key, prop_type):
        actual = self.topic[key]['truth'][prop_type]
        expected = self.expected[key]['truth'][prop_type]
        self.assertEqual(
            set(actual),
            set(expected),
            dedent(f"""
                Failed to set prop_type '{prop_type}' for key '{key}'.
                Actual: {actual}"
                Expected: {expected}"
            """),
        )
        self.assertEqual(
            len(actual),
            len(expected),
            dedent(f"""
                Found duplicates in prop_type '{prop_type}' for key '{key}'.
                Actual: {actual}
                Expected: {expected}
            """),
        )


class TestFillTopicTruthExclusive(TestFillTopicTruth):
    """
    Tests for fill_topic_truth using exclusive categories
    """


    topic = {}
    expected = {}


    @classmethod
    def setUpClass(cls):
        """
                     animals
                    /       |
              mammals       reptiles
             /       |         x
        humans       deer
          x            x
        """

        cls.topic = {
            'humans': {
                'truth': {
                    'A': ['mammals'],
                    'E': ['deer'],
                    'I': ['mammals'],
                },
            },
            'deer': {
                'truth': {
                    'A': ['mammals'],
                    'I': ['mammals'],
                },
            },
            'mammals': {
                'truth': {
                    'A': ['animals'],
                    'E': ['reptiles'],
                },
            },
            'reptiles': {
                'truth': {
                    'A': ['animals'],
                    'I': ['animals'],
                },
            },
            'animals': {},
        }

        cls.expected = {
            'humans': {
                'truth': {
                    'A': ['mammals', 'animals'],
                    'E': ['deer', 'reptiles'],
                    'I': ['mammals', 'animals'],
                    'O': ['deer', 'reptiles'],
                },
            },
            'deer': {
                'truth': {
                    'A': ['mammals', 'animals'],
                    'E': ['humans', 'reptiles'],
                    'I': ['mammals', 'animals'],
                    'O': ['humans', 'reptiles'],
                },
            },
            'mammals': {
                'truth': {
                    'A': ['animals'],
                    'E': ['reptiles'],
                    'I': ['humans', 'deer', 'animals'],
                    'O': ['humans', 'deer', 'reptiles'],
                },
            },
            'reptiles': {
                'truth': {
                    'A': ['animals'],
                    'E': ['mammals', 'humans', 'deer'],
                    'I': ['animals'],
                    'O': ['mammals', 'humans', 'deer'],
                },
            },
            'animals': {
                'truth': {
                    'A': [],
                    'E': [],
                    'I': ['mammals', 'humans', 'deer', 'reptiles'],
                    'O': ['mammals', 'humans', 'deer', 'reptiles'],
                },
            },
        }

        fill_topic_truth(cls.topic)
        super(TestFillTopicTruthExclusive, cls).setUpClass()


    def test_fill_a(self):
        """
        Test filling truth in topics for A's
        """
        self._test_case('humans', 'A')
        self._test_case('deer', 'A')
        self._test_case('mammals', 'A')
        self._test_case('reptiles', 'A')
        self._test_case('animals', 'A')


    def test_fill_e(self):
        """
        Test filling truth in topics for E's
        """
        self._test_case('humans', 'E')
        self._test_case('deer', 'E')
        self._test_case('mammals', 'E')
        self._test_case('reptiles', 'E')
        self._test_case('animals', 'E')


    def test_fill_i(self):
        """
        Test filling truth in topics for I's
        """
        self._test_case('humans', 'I')
        self._test_case('deer', 'I')
        self._test_case('mammals', 'I')
        self._test_case('reptiles', 'I')
        self._test_case('animals', 'I')


    def test_fill_o(self):
        """
        Test filling truth in topics for O's
        """
        self._test_case('humans', 'O')
        self._test_case('deer', 'O')
        self._test_case('mammals', 'O')
        self._test_case('reptiles', 'O')
        self._test_case('animals', 'O')


class TestFillTopicTruthNonExclusive(TestFillTopicTruth):
    """
    Tests for fill_topic_truth using non-exclusive categories

    This is used to test the last O rule: (rAs & tOs) -> tOr
    """


    topic = []
    expected = []


    @classmethod
    def setUpClass(cls):
        """
                    cool_people      tall_people
                   /                    x
        glasses_wearers
        """

        cls.topic = {
            'cool_people': {},
            'glasses_wearers': {
                'truth': {
                    'A': ['cool_people'],
                },
            },
            'tall_people': {
                'truth': {
                    'O': ['cool_people'],
                },
            },
        }

        cls.expected = {
            'cool_people': {
                'truth': {
                    'A': [],
                    'E': [],
                    'I': [],
                    'O': [],
                },
            },
            'glasses_wearers': {
                'truth': {
                    'A': ['cool_people'],
                    'E': [],
                    'I': [],
                    'O': [],
                },
            },
            'tall_people': {
                'truth': {
                    'A': [],
                    'E': [],
                    'I': [],
                    'O': ['cool_people', 'glasses_wearers'],
                },
            },
        }

        fill_topic_truth(cls.topic)
        super(TestFillTopicTruthNonExclusive, cls).setUpClass()


    def test_fill_o(self):
        """
        Test filling truth in topics for O's
        """
        self._test_case('cool_people', 'O')
        self._test_case('glasses_wearers', 'O')
        self._test_case('tall_people', 'O')
