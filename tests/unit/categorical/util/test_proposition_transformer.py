"""
test_proposition_transformer.py
"""

from dataclasses import dataclass

from test_logic.util import TestUnit

from logic.categorical.util.proposition_transformer import (
    contradictory,
    contrary,
    subalternate,
    converse,
)


@dataclass
class FakeProposition:
    """
    Class representing a categorical proposition
    """
    proposition_type: str
    subject: str = 'subject'
    predicate: str = 'predicate'
    topic_name: str = 'topic name'


class TestPropositionTransformer(TestUnit):
    """
    Base class for testing transforming propositions
    """


    def setUp(self):
        self.setup_mocks(
            namespace='logic.categorical.util.proposition_transformer',
            to_mock='CategoricalProposition',
        )
        self.mock['CategoricalProposition'].side_effect = FakeProposition


class TestContradictory(TestPropositionTransformer):
    """
    Tests for contradictory(proposition)
    """


    def setUp(self):
        super(TestContradictory, self).setUp()
        self.setup_mocks(
            namespace='logic.categorical.util.proposition_transformer',
            to_mock='negate_proposition_type',
        )


    def test_contradictory_a(self):
        """
        Test getting the contradictory of a proposition with type A
        """
        self.mock['negate_proposition_type'].return_value = 'O'
        actual_contradictory = contradictory(FakeProposition('A'))
        expected_contradictory = FakeProposition('O')
        self.assertEqual(actual_contradictory, expected_contradictory)



    def test_contradictory_e(self):
        """
        Test getting the contradictory of a proposition with type E
        """
        self.mock['negate_proposition_type'].return_value = 'I'
        actual_contradictory = contradictory(FakeProposition('E'))
        expected_contradictory = FakeProposition('I')
        self.assertEqual(actual_contradictory, expected_contradictory)


    def test_contradictory_i(self):
        """
        Test getting the contradictory of a proposition with type I
        """
        self.mock['negate_proposition_type'].return_value = 'E'
        actual_contradictory = contradictory(FakeProposition('I'))
        expected_contradictory = FakeProposition('E')
        self.assertEqual(actual_contradictory, expected_contradictory)


    def test_contradictory_o(self):
        """
        Test getting the contradictory of a proposition with type O
        """
        self.mock['negate_proposition_type'].return_value = 'A'
        actual_contradictory = contradictory(FakeProposition('O'))
        expected_contradictory = FakeProposition('A')
        self.assertEqual(actual_contradictory, expected_contradictory)


    def test_bad_proposition_type(self):
        """
        Test getting an error for a bad proposition type
        """
        self.mock['negate_proposition_type'].side_effect = KeyError
        with self.assertRaises(KeyError):
            dummy = contradictory(FakeProposition('bad'))


class TestContrary(TestPropositionTransformer):
    """
    Tests for contrary(proposition)
    """


    def test_contrary_a(self):
        """
        Test getting the contrary of a proposition with type A
        """
        actual_contrary = contrary(FakeProposition('A'))
        expected_contrary = FakeProposition('E')
        self.assertEqual(actual_contrary, expected_contrary)


    def test_contrary_e(self):
        """
        Test getting the contrary of a proposition with type E
        """
        actual_contrary = contrary(FakeProposition('E'))
        expected_contrary = FakeProposition('A')
        self.assertEqual(actual_contrary, expected_contrary)


    def test_contrary_i(self):
        """
        Test getting the contrary of a proposition with type I
        """
        actual_contrary = contrary(FakeProposition('I'))
        expected_contrary = FakeProposition('O')
        self.assertEqual(actual_contrary, expected_contrary)


    def test_contrary_o(self):
        """
        Test getting the contrary of a proposition with type O
        """
        actual_contrary = contrary(FakeProposition('O'))
        expected_contrary = FakeProposition('I')
        self.assertEqual(actual_contrary, expected_contrary)


    def test_bad_proposition_type(self):
        """
        Test getting an error for a bad proposition type
        """
        with self.assertRaises(KeyError):
            dummy = contrary(FakeProposition('bad'))


class TestSubalternate(TestPropositionTransformer):
    """
    Tests for subalternate(proposition)
    """


    def test_subalternate_a(self):
        """
        Test getting the subalternate of a proposition with type A
        """
        actual_subalternate = subalternate(FakeProposition('A'))
        expected_subalternate = FakeProposition('I')
        self.assertEqual(actual_subalternate, expected_subalternate)


    def test_subalternate_e(self):
        """
        Test getting the subalternate of a proposition with type E
        """
        actual_subalternate = subalternate(FakeProposition('E'))
        expected_subalternate = FakeProposition('O')
        self.assertEqual(actual_subalternate, expected_subalternate)


    def test_subalternate_i(self):
        """
        Test getting the subalternate of a proposition with type I
        """
        actual_subalternate = subalternate(FakeProposition('I'))
        expected_subalternate = FakeProposition('A')
        self.assertEqual(actual_subalternate, expected_subalternate)


    def test_subalternate_o(self):
        """
        Test getting the subalternate of a proposition with type O
        """
        actual_subalternate = subalternate(FakeProposition('O'))
        expected_subalternate = FakeProposition('E')
        self.assertEqual(actual_subalternate, expected_subalternate)


    def test_bad_proposition_type(self):
        """
        Test getting an error for a bad proposition type
        """
        with self.assertRaises(KeyError):
            dummy = subalternate(FakeProposition('bad'))


class TestConverse(TestPropositionTransformer):
    """
    Tests for converse(proposition)
    """


    def test_converse_a(self):
        """
        Test getting the converse of a proposition with type A
        """
        actual_converse = converse(FakeProposition('A'))
        expected_converse = FakeProposition('A', 'predicate', 'subject')
        self.assertEqual(actual_converse, expected_converse)


    def test_converse_e(self):
        """
        Test getting the converse of a proposition with type E
        """
        actual_converse = converse(FakeProposition('E'))
        expected_converse = FakeProposition('E', 'predicate', 'subject')
        self.assertEqual(actual_converse, expected_converse)


    def test_converse_i(self):
        """
        Test getting the converse of a proposition with type I
        """
        actual_converse = converse(FakeProposition('I'))
        expected_converse = FakeProposition('I', 'predicate', 'subject')
        self.assertEqual(actual_converse, expected_converse)


    def test_converse_o(self):
        """
        Test getting the converse of a proposition with type O
        """
        actual_converse = converse(FakeProposition('O'))
        expected_converse = FakeProposition('O', 'predicate', 'subject')
        self.assertEqual(actual_converse, expected_converse)
