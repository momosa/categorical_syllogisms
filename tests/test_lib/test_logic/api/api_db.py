"""
api_db.py

ApiDB
A database for handling request data and expected responses
"""

import os
import copy

from logic.util import Database


class ApiDB:
    """
    A database for handling request data and expected repsonses

    :param str test_type: name of the database to initialize.
        Corresponds to the input filename
    """

    DB_SUBDIR = 'api_tests'
    # We directly hit the container 'test.server'
    # within a local docker network
    # To run locally, start a dev server at port 8888
    # and run tests with environment variable CS_LOCAL=true
    # CS_LOCAL=true pytest -v tests/api/
    base_url = 'http://localhost:8888' \
        if os.environ.get('CS_LOCAL') == 'true' \
        else 'http://test.server'


    def __init__(self, test_type):
        self._db_full_name = ApiDB._get_db_full_name(test_type)
        self._database = Database(self._db_full_name, read_only=True)


    def get_test_names(self):
        """
        Get the list of tests

        :return: the list of tests
        :rtype: list
        """
        return list(self._database.keys())


    def get_url(self, test_name):
        """
        Get the full url for the test

        :param str test_name: key of the test we want to kwargs for
        :return: the full url for the test
        :rtype: str
        """
        endpoint = self._database[test_name]['request_kwargs']['url']
        url = os.path.join(ApiDB.base_url, endpoint)
        return url


    def get_request_method_name(self, test_name):
        """
        Get the name of the request method for the test

        :param str test_name: key of the test we want to kwargs for
        :return: the name of the request method (lower-case)
        :rtype: str
        """
        method_name = self._database[test_name].get('method', 'get')
        return method_name.lower()


    def get_request_kwargs(self, test_name):
        """
        Get the kwargs for the request method

        :param str test_name: key of the test we want to kwargs for
        :return: the kwargs for the request method
        :rtype: dict
        """
        kwargs = self._database[test_name].get('request_kwargs')
        if kwargs:
            kwargs = copy.deepcopy(kwargs)
            kwargs['url'] = self.get_url(test_name)
        return kwargs


    def get_expected(self, test_name):
        """
        Get the expected response data for the test

        :param str test_name: key of the test we want to kwargs for
        :return: the expected response data
        :rtype: dict
        """
        expected = self._database[test_name]['expected']
        return expected


    @staticmethod
    def _get_db_full_name(test_type):
        """
        Get the full name of the database

        Includes the db type given by the subdir

        :param str test_name: basename of the DB
        :return: str full DB name, like 'my_db_type/my_db'
        """
        return os.path.join(ApiDB.DB_SUBDIR, test_type)
