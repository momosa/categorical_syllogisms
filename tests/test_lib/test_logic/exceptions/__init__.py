"""
test_exceptions

A module for custom exceptions used in integration testing
"""


from test_logic.exceptions.api_data import ApiDataError
from test_logic.exceptions.api_status import ApiStatusError


__all__ = [
    'ApiDataError',
    'ApiStatusError',
]
