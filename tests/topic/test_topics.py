"""
test_topics.py

Backend integration tests for validating topics
"""

from textwrap import dedent
import re

from unittest import TestCase

from logic.categorical import Topic
from logic.categorical.util.proposition_type import PROPOSITION_TYPES
from logic.categorical.util.form import VALID_FORMS
from logic.categorical.syllogism import CategoricalSyllogism


MAX_TOPIC_NAME_LEN = 30
EXPECTED_N_TERMS = 10


class TestTopic(TestCase):
    """
    Validate all topics
    """


    def _test_topic_name_length(self, topic, max_length):
        """
        Test that the topic name isn't too long
        """
        actual_length = len(topic.topic_name)
        self.assertLessEqual(
            actual_length,
            max_length,
            dedent(f"""
                Topic {topic.topic_name} has a name that's too long.
                It has {actual_length} characters,
                but should have no more than {max_length} characters.
            """),
        )


    def _test_topic_slug(self, topic):
        """
        Test that the topic name follows the established format
        """
        is_legit_slug = lambda name: re.match(r'^[a-z][a-z_]*[a-z]$', name) is not None
        self.assertTrue(
            is_legit_slug(topic.topic_name),
            dedent(f"""
                Topic {topic.topic_name} is not allowed.
                Topic names must contain only lower-case letters and '_',
                and must begin and end with a letter.
            """),
        )


    def _test_term_keys(self, topic):
        """
        Test that the term keys are the plural form
        """
        for term_key, term_info in topic.items():
            self.assertEqual(
                term_key,
                term_info['plural'],
                dedent(f"""
                    Topic {topic.topic_name} has illegit keys.
                    Keys should be the plural form of the term.
                """),
            )


    def _test_n_terms(self, topic):
        """
        Test that the number of terms of the given type is correct
        """
        actual_n_terms = len(topic.keys())
        self.assertEqual(
            actual_n_terms,
            EXPECTED_N_TERMS,
            dedent(f"""
                Topic {topic.topic_name} has {actual_n_terms},
                but it should have {EXPECTED_N_TERMS}
            """),
        )


    def _test_contains_true_prop(self, topic, proposition_type):
        """
        Test that the topic contains at least one true proposition of the the given type
        """
        n_true_props = lambda term: len(topic[term]['truth'][proposition_type])
        found = next((
            term
            for term in topic
            if n_true_props(term) > 0
        ), None)
        self.assertIsNotNone(
            found,
            dedent(f"""
                Topics should contain a true proposition of every type.
                Topic {topic.topic_name} lacks a true proposition of type {repr(proposition_type)}.
            """),
        )


    def _test_truth_keys(self, topic):
        """
        Test that the topic's truth keys are correct for each term
        """
        expected_truth_keys = set(PROPOSITION_TYPES)
        for term_info in topic.values():
            actual_truth_keys = set(term_info['truth'].keys())
            self.assertSetEqual(actual_truth_keys, expected_truth_keys)


    def _test_consistency(self, topic):
        """
        Test the consistency of the topic
        """
        for term_info in topic.values():
            a_predicates = set(term_info['truth']['A'])
            e_predicates = set(term_info['truth']['E'])
            i_predicates = set(term_info['truth']['I'])
            o_predicates = set(term_info['truth']['O'])
            self.assertTrue(a_predicates.isdisjoint(o_predicates))
            self.assertTrue(e_predicates.isdisjoint(i_predicates))


    def _test_sound_forms(self, topic_name):
        """
        Test that the topic has sound syllogisms of every valid form
        """
        missing_sound_forms = []
        for valid_form in VALID_FORMS:
            dummy = CategoricalSyllogism(
                topic_name=topic_name,
                is_sound=True,
                form=valid_form,
            )
            try:
                dummy = CategoricalSyllogism(
                    topic_name=topic_name,
                    is_sound=True,
                    form=valid_form,
                )
            except:
                missing_sound_forms.append(valid_form)
        self.assertEqual(
            missing_sound_forms,
            [],
            dedent(f"""
                Required: a sound categorical syllogism for every valid form.
                Topic: {repr(topic_name)}
                Valid forms without a sound syllogism:
                {repr(missing_sound_forms)}
                Topic details:
                {repr(Topic(topic_name).as_dict())}
            """),
        )



    def test_topic_name(self):
        """
        Validate the names of all topics
        """
        available_topics = Topic.get_available()
        for topic_name in available_topics:
            topic = Topic(topic_name=topic_name)
            self._test_topic_name_length(topic, MAX_TOPIC_NAME_LEN)
            self._test_topic_slug(topic)


    def test_n_terms(self):
        """
        Validate that each topic has the correct number of terms
        """
        available_topics = Topic.get_available()
        for topic_name in available_topics:
            topic = Topic(topic_name=topic_name)
            self._test_n_terms(topic)


    def test_term_keys(self):
        """
        Validate term keys for all topics
        """
        available_topics = Topic.get_available()
        for topic_name in available_topics:
            topic = Topic(topic_name=topic_name)
            self._test_term_keys(topic)


    def test_truth_values(self):
        """
        Validate that each topic has at least one true proposition of each type
        """
        available_topics = Topic.get_available()
        for topic_name in available_topics:
            topic = Topic(topic_name=topic_name)
            for proposition_type in PROPOSITION_TYPES:
                self._test_contains_true_prop(topic, proposition_type)


    def test_truth_keys(self):
        """
        Validate that each topic has the right set of truth keys
        """
        available_topics = Topic.get_available()
        for topic_name in available_topics:
            topic = Topic(topic_name=topic_name)
            self._test_truth_keys(topic)


    def test_consistency(self):
        """
        Validate that each topic is consistent
        """
        available_topics = Topic.get_available()
        for topic_name in available_topics:
            topic = Topic(topic_name=topic_name)
            self._test_consistency(topic)


    def test_sound_forms(self):
        """
        Validate that each topic has sound syllogisms of every valid form
        """
        available_topics = Topic.get_available()
        for topic_name in available_topics:
            self._test_sound_forms(topic_name)
