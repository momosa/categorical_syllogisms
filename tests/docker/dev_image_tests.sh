test_container=$(cat <<-'EOF'

source /categorical_syllogisms/tests/docker/test_functions.sh

test_command_available python
test_cmd 'Test that python is version 3.7' \
    'python --version | grep "Python 3.7"'

test_command_available pip
test_cmd 'Test that pip is for python 3.7' \
    'pip --version | grep "python 3.7"'

test_directory_exists /categorical_syllogisms/
test_file_exists /categorical_syllogisms/requirements-dev.txt
test_file_exists /categorical_syllogisms/requirements-shared.txt

test_python_package_installed requests
test_python_package_version requests 2.21

test_python_package_installed enum
test_python_package_version enum 1.1.6 

test_python_package_installed bleach
test_python_package_version bleach 3.1 

test_python_package_installed retrying
test_python_package_version retrying 1.3.3

# Test python dev toys installed via pip
test_command_available pytest
test_python_package_version pytest 4.4.0
test_python_package_installed pytest_cov
test_python_package_version pytest-cov 2.6.1
test_command_available pylint
test_python_package_version pylint 2.3.1

test_directory_exists /categorical_syllogisms/logic
test_directory_exists /categorical_syllogisms/logic_server
test_directory_exists /categorical_syllogisms/tests

test_environment_var DJANGO_SETTINGS_MODULE logic_server.settings_dev

EOF
)
