#!/bin/bash -e

# USAGE: run.sh image_name
# E.g., run.sh prod

declare -r CATEGORICAL_SYLLOGISMS="$(cd "${BASH_SOURCE%/*}/../.."; pwd; cd - &> /dev/null)"
[[ -d "$CATEGORICAL_SYLLOGISMS" ]] || \
    { echo "Failed to find the root dir. Tried '$CATEGORICAL_SYLLOGISMS'" 1>&2 ; exit 1; }
source "$CATEGORICAL_SYLLOGISMS/bin/errmsgs"

declare -r IMAGE=$1
[[ $IMAGE ]] || echoexit "Please specify an image"
declare -r CONTAINER=$IMAGE

declare -r TEST_FILE=$CATEGORICAL_SYLLOGISMS/tests/docker/${IMAGE}_image_tests.sh
[[ -f $TEST_FILE ]] || echoexit "Cannot find tests for image '$IMAGE'"

declare test_passed=true

# Get the test_container variable to execute tests
declare test_container
source $TEST_FILE

echo
echo ========================================
echo "Testing image: $IMAGE"
echo ----------------------------------------
docker run $CONTAINER bash -ce "$test_container" \
    || test_passed=false
echo ========================================
echo

$CATEGORICAL_SYLLOGISMS/bin/clean.sh

if $test_passed; then
    echo "Test successful for image '$IMAGE'"
else
    echoexit "Test failed for image '$IMAGE'"
fi
