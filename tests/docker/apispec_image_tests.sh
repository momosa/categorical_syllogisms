test_container=$(cat <<-'EOF'

source /categorical_syllogisms/tests/docker/test_functions.sh

# Test apt-get installs
test_command_available npm
test_command_available wget
test_command_available swagger-merger
test_command_available java

test_directory_exists /swagger-codegen
test_file_exists /swagger-codegen/swagger-codegen-cli.jar
test_directory_exists /swagger-codegen/in/
test_directory_exists /swagger-codegen/out/

# Test that the root spec file exists
test_file_exists /categorical_syllogisms/apispec/index.yaml

# Test yaml merging
test_file_exists /swagger-codegen/in/index.yaml

EOF
)
