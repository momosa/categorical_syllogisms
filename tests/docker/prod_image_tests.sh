test_container=$(cat <<-'EOF'

source /categorical_syllogisms/tests/docker/test_functions.sh

test_command_available python
test_cmd 'Test that python is version 3.7' \
    'python --version | grep "Python 3.7"'

test_command_available pip
test_cmd 'Test that pip is for python 3.7' \
    'pip --version | grep "python 3.7"'

test_directory_exists /categorical_syllogisms/
test_file_exists /categorical_syllogisms/requirements-prod.txt
test_file_exists /categorical_syllogisms/requirements-shared.txt

test_python_package_installed requests
test_python_package_version requests 2.21

test_python_package_installed enum
test_python_package_version enum 1.1.6 

test_python_package_installed bleach
test_python_package_version bleach 3.1 

test_python_package_installed gunicorn
test_command_available gunicorn
test_python_package_version gunicorn 19.9.0

test_directory_exists /categorical_syllogisms/logic
test_directory_exists /categorical_syllogisms/logic_server

test_environment_var DJANGO_SETTINGS_MODULE logic_server.settings

EOF
)
