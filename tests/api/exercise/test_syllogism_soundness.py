"""
test_syllogism_soundness.py

API integration tests against proposition type exercise
"""

from test_logic.api import ApiTestCase


class TestPropositionType(ApiTestCase):
    """
    Tests for the soundness exercise API endpoint
    """


    @classmethod
    def setUpClass(cls):
        cls.init_db('exercise/syllogism_soundness')
        super(TestPropositionType, cls).setUpClass()


    def test_soundness(self):
        """
        Run API tests for the soundness exercise endpoint
        """
        self.run_all()
