"""
test_proposition_type.py

API integration tests against proposition type exercise
"""

from test_logic.api import ApiTestCase


class TestPropositionType(ApiTestCase):
    """
    Tests for the proposition type exercise API endpoint
    """


    @classmethod
    def setUpClass(cls):
        cls.init_db('exercise/proposition_type')
        super(TestPropositionType, cls).setUpClass()


    def test_prop_type(self):
        """
        Run API tests for the proposition type exercise endpoint
        """
        self.run_all()
