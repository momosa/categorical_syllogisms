"""
test_proposition_contradictory.py

API integration tests against proposition contradictory exercise
"""

from test_logic.api import ApiTestCase


class TestPropositionContradictory(ApiTestCase):
    """
    Tests for the proposition contradictory exercise API endpoint
    """


    @classmethod
    def setUpClass(cls):
        cls.init_db('exercise/proposition_contradictory')
        super(TestPropositionContradictory, cls).setUpClass()


    def test_prop_type(self):
        """
        Run API tests for the proposition contradictory exercise endpoint
        """
        self.run_all()
