"""
test_list.py

API integration tests against the topic list endpoint
"""

from test_logic.api import ApiTestCase


class TestList(ApiTestCase):
    """
    Tests for the topic list API endpoint
    """


    @classmethod
    def setUpClass(cls):
        cls.init_db('topic/list')
        super(TestList, cls).setUpClass()


    def test_list(self):
        """
        Run API tests for the topic list endpoint
        """
        self.run_all()
