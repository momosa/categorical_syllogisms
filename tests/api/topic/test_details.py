"""
test_details.py

API integration tests against the topic details endpoint
"""

from test_logic.api import ApiTestCase


class TestDetails(ApiTestCase):
    """
    Tests for the topic details API endpoint
    """


    @classmethod
    def setUpClass(cls):
        cls.init_db('topic/details')
        super(TestDetails, cls).setUpClass()


    def test_details(self):
        """
        Run API tests for the topic details endpoint
        """
        self.run_all()
