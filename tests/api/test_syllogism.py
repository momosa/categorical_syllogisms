"""
test_syllogism.py

API integration tests against syllogism
"""

from test_logic.api import ApiTestCase


class TestSyllogism(ApiTestCase):
    """
    Tests for the proposition API endpoints
    """


    @classmethod
    def setUpClass(cls):
        cls.init_db('syllogism')
        super(TestSyllogism, cls).setUpClass()


    def test_syllogism(self):
        """
        Run API tests for the syllogism endpoints
        """
        self.run_all()
