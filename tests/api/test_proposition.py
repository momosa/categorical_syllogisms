"""
test_proposition.py

API integration tests against proposition
"""

from test_logic.api import ApiTestCase


class TestProposition(ApiTestCase):
    """
    Tests for the proposition API endpoints
    """


    @classmethod
    def setUpClass(cls):
        cls.init_db('proposition')
        super(TestProposition, cls).setUpClass()


    def test_proposition(self):
        """
        Run API tests for the proposition endpoints
        """
        self.run_all()
