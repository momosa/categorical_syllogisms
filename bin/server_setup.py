#!/usr/bin/env python3

import string, random, inspect, os

SECREY_KEY_FILE = ''

def _get_project_dir():
    """
    Get the project directory
    """
    bin_dir = os.path.dirname(
        os.path.abspath(
            inspect.getfile(inspect.currentframe())
        )
    )
    project_dir = os.path.join(
        bin_dir,
        os.pardir,
    )
    return project_dir

def _get_secret_key_filename():
    """
    Get the abs path of the secret key file
    """
    project_dir = _get_project_dir()
    key_filename = os.path.join(
        project_dir,
        'logic_server',
        'logic_server',
        'secret_key.py',
    )
    return key_filename

def _get_new_secret_key():
    chars = string.ascii_letters+string.digits+string.punctuation
    new_key = ''.join((
        random.SystemRandom().choice(chars)
        for i in range(random.randint(45,50))
    ))
    return new_key

def create_secret_key_file():
    new_key = _get_new_secret_key()
    key_filename = _get_secret_key_filename()
    with open(key_filename, 'w+') as key_file:
        key_file.write(
            f"SECRET_KEY = {repr(new_key)}"
        )

if __name__ == '__main__':
    print(f"Creating secret key file {_get_secret_key_filename()}")
    create_secret_key_file()
