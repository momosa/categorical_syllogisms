#!/bin/bash -e

declare -r CATEGORICAL_SYLLOGISMS="$(cd "${BASH_SOURCE%/*}/.."; pwd; cd - &> /dev/null)"
[[ -d "$CATEGORICAL_SYLLOGISMS" ]] || \
    { echo "Failed to find the root dir. Tried '$CATEGORICAL_SYLLOGISMS'" 1>&2 ; exit 1; }
source "$CATEGORICAL_SYLLOGISMS/bin/errmsgs"

print_help() {
cat << END_HELP
USAGE

    run.sh  [-v|--verbose] [-l LOG_DIR] [-h|--help] [target]

DESCRIPTION

    Runs a designated docker container.

    Any logs placed into the container's /var/log will be retained,
    stored on the host machine in /var/log/categorical_syllogisms.

TARGET CONTAINERS

    unit_tests
        Run the python unit tests

    lint_tests
        Run the python lint tests

    topic_tests
        Validate topics

    ui_unit_tests
        Run the javascript unit tests

    ui_lint_tests
        Run the javascript lint tests

    api_tests
        The api tests container.
        Also starts a test server.
        The test server is stopped when tests are complete.

    apispec_validate
        Validate the apispec using swagger-codegen

    apispec_generate
        Generate the apispec using swagger-codegen

    dev_server
        The development server container.
        Attaches to port $DEFAULT_DEV_PORT by default.

    prod_server
        The production server container.
        Attaches to port $PROD_SERVER_PORT.

END_HELP
}

create_network() {
    docker network inspect $CS_NETWORK &> /dev/null || \
        docker network create $CS_NETWORK
}

remove_old_container() {
    local container=$1
    local running_ids="$(docker ps --quiet --filter name=$container)"
    [[ "$running_ids" ]] && docker stop $running_ids || true
    local ids="$(docker ps --all --quiet --filter name=$container)"
    [[ "$ids" ]] && docker rm $ids || true
}

run_container() {
    local image="$1"
    local container_name="$2"
    local options="$3"
    shift 3
    local args="$@"
    local log_dir="$LOG_DIR/$container_name"
    [[ -d $log_dir ]] || mkdir -p $log_dir
    options+=" --name $container_name"
    options+=" --volume '$log_dir:/var/log'"
    remove_old_container $container_name
    eval "$(echo docker run $options $image $args)" \
        || echoexit "Execution failed: '$container_name'"
}

stop_container() {
    local container=$1
    local container_is_running
    container_is_running="$(docker ps | grep $container || true)"
    if [[ "$container_is_running" ]]; then
        docker stop $container
    fi
}

run_tests_with_runner() {
    local test_runner='/categorical_syllogisms/bin/test.sh'
    local test_type=$1
    local test_cmd="$test_runner $test_type"
    local container_name="${test_type}_tests"
    run_container dev $container_name \
        "--rm --entrypoint /bin/bash" -ce "'$test_cmd'"
}

run_dev_server(){
    local host_port="${1:-$DEFAULT_DEV_PORT}"
    local options=""
    options+=" --detach=true"
    options+=" --expose $CONTAINER_SERVER_PORT"
    options+=" --publish $host_port:$CONTAINER_SERVER_PORT"
    options+=" --network $CS_NETWORK"
    options+=" --entrypoint /bin/bash"
    local -r server_setup='/categorical_syllogisms/bin/server_setup.py'
    local server_cmd="$server_setup && /categorical_syllogisms/logic_server/manage.py runserver"
    server_cmd+=" 0.0.0.0:$CONTAINER_SERVER_PORT"
    server_cmd+=" &> /var/log/django.log"
    create_network
    run_container dev dev_server "$options" \
        -ce "'$server_cmd'"
}

run_cs_server(){
    # Runs a server with gunicorn
    # Does not publish a port on the host
    # The container is expected to be connected to nginx
    local -r CONTAINER_NAME=backend.logicforall.org
    local docker_options=""
    docker_options+=" --detach=true"
    docker_options+=" --restart=on-failure"
    docker_options+=" --network $CS_NETWORK"
    docker_options+=" --entrypoint /bin/bash"

    local -r server_setup='/categorical_syllogisms/bin/server_setup.py'
    local -r server_dir='/categorical_syllogisms/logic_server'
    local gunicorn_cmd='gunicorn'
    gunicorn_cmd+=" --config gunicorn_config.py"
    gunicorn_cmd+=" --bind 0.0.0.0:80"
    gunicorn_cmd+=" logic_server.wsgi:application"
    local server_cmd="$server_setup && cd $server_dir && $gunicorn_cmd &> /var/log/gunicorn.log"

    run_container prod $CONTAINER_NAME "$docker_options" \
        -ce "'$server_cmd'"
}

run_nginx(){
    local host_port=${1:-$PROD_SERVER_PORT}
    local is_test
    [[ "$2" ]] && is_test=true || is_test=false
    local container_name=nginx
    if $is_test; then
        container_name=test.server
    fi
    local docker_options=""
    docker_options+=" --detach=true"
    docker_options+=" --name=$container_name"
    docker_options+=" --network $CS_NETWORK"
    docker_options+=" --restart=on-failure"
    if ! $is_test; then
        docker_options+=" --publish $host_port:80"
    fi

    remove_old_container $container_name
    eval "$(echo docker run $docker_options nginx)"
}

run_api_tests(){
    local tests_passed=true

    create_network
    echo "Starting the backend and test servers..."
    run_cs_server
    run_nginx 80 is_test

    local options=""
    options+=" --net $CS_NETWORK"
    options+=" --entrypoint /bin/bash"
    local test_runner='/categorical_syllogisms/bin/test.sh'
    local test_args='api'
    local test_cmd="$test_runner $test_args"
    run_container dev api_tests \
        "$options" -ce "'$test_cmd'" \
        || tests_passed=false

    echo "Cleaning up..."
    stop_container test.server
    stop_container backend.logicforall.org
    remove_old_container test.server
    remove_old_container backend.logicforall.org
    $CATEGORICAL_SYLLOGISMS/bin/clean.sh docker
    $tests_passed || echoexit "API tests failed"
}

run_apispec_validate() {
    local validate_cmd="java -jar /swagger-codegen/swagger-codegen-cli.jar validate"
    validate_cmd+=" --input-spec /swagger-codegen/in/index.yaml"
    run_container apispec apispec_validate \
        "--rm --entrypoint /bin/bash" -ce "'$validate_cmd'"
}

run_apispec_generate() {
    local apispec_out_dir="$OUT_DIR/apispec"
    [[ -d "$apispec_out_dir" ]] || mkdir -p "$apispec_out_dir"
    local mount_opts="type=bind"
    mount_opts+=",source=$apispec_out_dir"
    mount_opts+=",destination=/swagger-codegen/out"
    local options="--mount $mount_opts"
    options+=" --rm"
    options+=" --entrypoint /bin/bash"

    local generate_cmd="java -jar /swagger-codegen/swagger-codegen-cli.jar generate"
    generate_cmd+=" --input-spec /swagger-codegen/in/index.yaml"
    generate_cmd+=" --lang html2"
    generate_cmd+=" --output /swagger-codegen/out"
    run_container apispec apispec_generate \
        "$options" -ce "'$generate_cmd'"

    echo "The apispec can be found in $apispec_out_dir"
}

run_prod_server(){
    local -r HOST_PORT=${1:-$PROD_SERVER_PORT}
    (( $HOST_PORT > 0 )) || echoexit "Illegit host port $HOST_PORT"
    create_network
    run_cs_server
    run_nginx $HOST_PORT
}

declare opt
while getopts ':-hl:v' opt; do
    case $opt in
        h|-help)
            print_help
            exit
            ;;
        l)
            CATEGORICAL_SYLLOGISMS_LOGS=$OPTARG
            ;;
        v|-verbose)
            set -x
            ;;
        \?)
            echo "Invalid option -$OPTARG" 1>&2
            print_help
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument"
            print_help
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

[[ "$@" ]] || { print_help; exit 1; }

if [[ "$GITLAB_CI" ]]; then
    # In gitlab CI, dind and job containers only share certain folders
    # $CI_PROJECT_DIR is one such folder
    # See https://gitlab.com/gitlab-org/gitlab-ce/issues/41227
    declare -r OUT_DIR=${CI_PROJECT_DIR}/.tmp
else
    declare -r OUT_DIR=${HOME}/.categorical_syllogisms
fi

declare -r LOG_DIR=${CATEGORICAL_SYLLOGISMS_LOGS:-$OUT_DIR/logs}
declare -r CS_NETWORK=categorical_syllogisms_network
declare -r DEFAULT_DEV_PORT=8888
declare -r CONTAINER_SERVER_PORT=8888
declare -r TEST_SERVER_PORT=9001
declare -r PROD_SERVER_PORT=80

[[ -d $OUT_DIR ]] || mkdir -p ${OUT_DIR}
[[ -d $LOG_DIR ]] || mkdir -p ${LOG_DIR}

declare container=$1
shift

case $container in
    unit_tests) run_tests_with_runner unit ;;
    lint_tests) run_tests_with_runner lint ;;
    topic_tests) run_tests_with_runner topic ;;
    ui_unit_tests) run_tests_with_runner ui_unit ;;
    ui_lint_tests) run_tests_with_runner ui_lint ;;
    api_tests) run_api_tests ;;
    apispec_validate) run_apispec_validate ;;
    apispec_generate) run_apispec_generate ;;
    prod_server) run_prod_server $@;;
    dev_server) run_dev_server $@ ;;
    tests)
        run_unit_tests
        run_lint_tests
        run_topic_tests
        run_api_tests
        run_apispec_validate
        ;;
    *)
        echoerr "Unrecognized container '$container'"
        print_help
        exit 1
        ;;
esac

echo "Logs will be stored in $LOG_DIR"

