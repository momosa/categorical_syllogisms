#!/bin/bash -ex

declare -r DEFAULT_HOST=logicforall-server
declare -r REMOTE_NGINX_CONF='https://gitlab.com/momosa/categorical_syllogisms/raw/master/misc/nginx.conf'

declare -r CATEGORICAL_SYLLOGISMS="$(cd "${BASH_SOURCE%/*}/.."; pwd; cd - &> /dev/null)"
[[ -d "$CATEGORICAL_SYLLOGISMS" ]] || \
    { echo "Failed to find the root dir. Tried '$CATEGORICAL_SYLLOGISMS'" 1>&2 ; exit 1; }
declare -r BIN="${CATEGORICAL_SYLLOGISMS}/bin"
source "${BIN}/errmsgs"

function print_help() {
cat << END_HELP

USAGE

    deploy.sh [-h|--help]
    deploy.sh local
    deploy.sh [host_name]

If not specified, the host_name defaults to $DEFAULT_HOST

For remote deployments, put something like this in your .ssh/config:

    Host logicforall-server
        User ubuntu
        HostName 18.216.56.239
        IdentityFile ~/.ssh/logicforall-server.pem

Ensure you can ssh onto the host using 'ssh host_name'
Ensure docker and wget are installed on the host.

END_HELP
}

helpexit() { echoerr "$@"; print_help 1>&2; exit 1; }

declare -r PROD_IMAGE=registry.gitlab.com/momosa/categorical_syllogisms
declare -r NGINX_IMAGE=registry.gitlab.com/momosa/categorical_syllogisms/nginx

get_run_django_server_cmd() {
    local run_server_cmd=''
    run_server_cmd+=' /categorical_syllogisms/bin/server_setup.py'
    run_server_cmd+=' && cd /categorical_syllogisms/logic_server'
    run_server_cmd+=' && gunicorn'
    run_server_cmd+=' --config gunicorn_config.py'
    run_server_cmd+=' --bind 0.0.0.0:80'
    run_server_cmd+=' logic_server.wsgi:application'
    run_server_cmd+=' &> /var/log/gunicorn.log'
    echo $run_server_cmd
}

get_host_cmds() {
cat << END_HOST_CMDS
set -x
# Ensure prerequisites
which docker || { echo "Please install docker"; exit 1; }
which wget || { echo "Please install wget"; exit 1; }
[[ -d \$HOME/.categorical_syllogisms ]] || \
    mkdir -p \$HOME/.categorical_syllogisms
wget --output-document \$HOME/.categorical_syllogisms/nginx.conf $REMOTE_NGINX_CONF
[[ -f \$HOME/.categorical_syllogisms/nginx.conf ]] || \
    { echo "Failed to obtain the nginx config"; exit 1; }
[[ -d /var/log/categorical_syllogisms ]] || sudo mkdir -p /var/log/categorical_syllogisms


# Remove old images
images="$(docker images --quiet --filter dangling=true)" 2>&1 | grep -v 'must be forced' || true
if [[ "$images" ]]; then
    docker rmi $images
fi

# Remove stale containers
echo "Removing stale containers (all of them)"
containers="$(docker ps --quiet --filter status=exited)"
if [[ "$containers" ]]; then
    docker rm $containers
fi

docker pull $PROD_IMAGE
docker pull $NGINX_IMAGE

# Turn things off
docker ps | grep nginx && docker stop nginx
docker ps | grep backend.logicforall.org && docker stop backend.logicforall.org
docker rm -f nginx || true
docker rm -f backend.logicforall.org || true
docker ps | grep nginx && \
    { echo "Failed to remove nginx container"; exit 1; }
docker ps | grep backend.logicforall.org && \
    { echo "Failed to remove backend.logicforall.org container"; exit 1; }

docker network ls | grep categorical_syllogisms_network || \
    docker network create categorical_syllogisms_network

# Run the backend django server
sudo docker run \
    --detach=true \
    --network categorical_syllogisms_network \
    --entrypoint /bin/bash \
    --name backend.logicforall.org \
    --restart=on-failure \
    --volume /var/log/categorical_syllogisms:/var/log \
    $PROD_IMAGE \
    -ce '$(get_run_django_server_cmd)'

# Run nginx
docker run \
    --detach=true \
    --network categorical_syllogisms_network \
    --publish 80:80 \
    --name nginx \
    --restart=on-failure \
    $NGINX_IMAGE
END_HOST_CMDS
}


update_server() {
    local -r HOST=${1:-$DEFAULT_HOST}
    ssh $HOST -o ConnectTimeout=5 'echo testing server access' \
        || helpexit "Failed to connect to $HOST"
    ssh ${HOST} "$(get_host_cmds)"
}

deploy_local() {
    source <(get_host_cmds)
}

case $1 in
    -h|--help) print_help ;;
    *) update_server $1 ;;
esac
