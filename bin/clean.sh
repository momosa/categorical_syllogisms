#!/bin/bash -e

declare -r CATEGORICAL_SYLLOGISMS="$(cd "${BASH_SOURCE%/*}/.."; pwd; cd - &> /dev/null)"
[[ -d "$CATEGORICAL_SYLLOGISMS" ]] || \
    { echo "Failed to find the root dir. Tried '$CATEGORICAL_SYLLOGISMS'" 1>&2 ; exit 1; }
source "$CATEGORICAL_SYLLOGISMS/bin/errmsgs"

print_help() {
cat << END_HELP
USAGE

    clean.sh  [-v|--verbose] [-h|--help]

DESCRIPTION

    Cleans up old images, containers, and python files.

END_HELP
}

declare opt
while getopts ':-hv' opt; do
    case $opt in
        h|-help)
            print_help
            exit
            ;;
        v|-verbose)
            set -x
            ;;
        \?)
            echo "Invalid option -$OPTARG" 1>&2
            print_help
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument"
            print_help
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

remove_stale_containers() {
    echo "Removing stale containers (all of them)"
    local containers="$(docker ps --quiet --filter status=exited)"
    if [[ "$containers" ]]; then
        docker rm $containers
    fi
}

remove_unused_images() {
    echo "Removing unused images"
    local images="$(docker images --quiet --filter dangling=true)" 2>&1 | grep -v 'must be forced' || true

    if [[ "$images" ]]; then
        docker rmi $images
    fi
}

remove_pyc_files() {
    echo "Removing .pyc files and __pycache__ dirs"
    if [[ -d "$CATEGORICAL_SYLLOGISMS" ]]; then
        find "$CATEGORICAL_SYLLOGISMS" -type f -name "*.pyc" -delete
        find "$CATEGORICAL_SYLLOGISMS" -type d -name "__pycache__" -delete
    fi
}

git_clean() {
    echo "git clean -xdf"
    git clean -xdf
}

declare cleaner=$1
[[ "$cleaner" ]] || cleaner='all'

case $cleaner in
    stale_containers) remove_stale_containers ;;
    unused_images) remove_unused_images ;;
    pyc_files) remove_pyc_files ;;
    git) git_clean ;;
    docker)
        remove_stale_containers
        remove_unused_images
        ;;
    all)
        remove_stale_containers
        remove_unused_images
        remove_pyc_files
        #git_clean
        ;;
    *) echoexit "Unrecognized cleaner '$cleaner'" ;;
esac
