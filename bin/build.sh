#!/bin/bash -e

declare -r CATEGORICAL_SYLLOGISMS="$(cd "${BASH_SOURCE%/*}/.."; pwd; cd - &> /dev/null)"
[[ -d "$CATEGORICAL_SYLLOGISMS" ]] || \
    { echo "Failed to find the root dir. Tried '$CATEGORICAL_SYLLOGISMS'" 1>&2 ; exit 1; }
source "$CATEGORICAL_SYLLOGISMS/bin/errmsgs"

function print_help() {
cat << END_HELP
USAGE

    build.sh  [-v|--verbose] [-h|--help] [target]

DESCRIPTION

    Builds the designated docker image(s).

BUILD TARGETS

    all
        All images

    prod
        The production image, used for deployment

    dev
        The development image, used for development, debugging, and testing

    apispec
        The apispec image, used to generate and validate the apispec

END_HELP
}

function build_image() {
    local image=$1
    local dockerfile="$CATEGORICAL_SYLLOGISMS"/docker/${image}.dockerfile
    if [[ "$image" == prod ]] || [[ "$image" == dev ]]; then
        $CATEGORICAL_SYLLOGISMS/bin/clean.sh pyc_files
    fi
    docker build \
        --tag $image \
        --file $dockerfile \
        "$CATEGORICAL_SYLLOGISMS" \
        || echoexit "Failed to build image '$image'"
}

declare opt
while getopts ':-hv' opt; do
    case $opt in
        h|-help)
            print_help
            exit
            ;;
        v|-verbose)
            set -x
            ;;
        \?)
            echo "Invalid option -$OPTARG" 1>&2
            print_help
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument"
            print_help
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

[[ "$@" ]] || { print_help; exit 1; }

declare image
for image in $@; do
    case $image in
        prod|dev|apispec|nginx) build_image $image ;;
        all) # All the things!
            build_image prod
            build_image dev
            build_image apispec
            build_image nginx
            ;;
        *) echoexit "Unrecognized build target '$image'" ;;
    esac
done
