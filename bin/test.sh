#!/bin/bash -e

declare -r CATEGORICAL_SYLLOGISMS="$(cd "${BASH_SOURCE%/*}/.."; pwd; cd - &> /dev/null)"
[[ -d "$CATEGORICAL_SYLLOGISMS" ]] || \
    { echo "Failed to find the root dir. Tried '$CATEGORICAL_SYLLOGISMS'" 1>&2 ; exit 1; }
source "$CATEGORICAL_SYLLOGISMS/bin/errmsgs"

declare -r BIN="$CATEGORICAL_SYLLOGISMS/bin"
declare -r ALL_IMAGES="prod dev apispec"

print_help() {
cat << END_HELP
USAGE

    test.sh  [-v|--verbose] [-h|--help] [test_type]

DESCRIPTION

    Runs the desigated tests.

    NOTE: This will run the tests locally. The intended purpose of this
    script is to be used by run.sh to run tests in containers. However,
    this will work anywhere provided that the prerequisites are met.

TEST TYPES

    unit|unit_tests
        The unit tests with test coverage

    lint|lint_tests [logic|tests]
        The lint tests
        No argument will run all available tests.
        Arguments logic or tests will run the linter against
        the logic package or tests, respectively.

    api|api_tests
        The api tests.

    docker|docker_tests
        The docker tests

END_HELP
}

run_unit_tests() {
    local exit_status
    local output
    local test_cmd='pytest'
    test_cmd+=" --cov=$CATEGORICAL_SYLLOGISMS/logic/logic"
    test_cmd+=" --cov-report term-missing"
    test_cmd+=" --cov-report term:skip-covered"
    test_cmd+=" $CATEGORICAL_SYLLOGISMS/tests/unit/"
    test_cmd+=" 2>&1"
    set +e
    output=$(eval "$test_cmd")
    exit_status=$?
    set -e
    echo "$output"
    local coverage=$(echo "$output" | grep TOTAL | awk '{print $4}')
    coverage=${coverage%\%}
    $BIN/clean.sh pyc_files
    (( exit_status == 0 )) || echoexit "Unit tests failed"
    (( coverage == 100 )) || echoexit "Test coverage is ${coverage}%, but should be 100%"
}

run_api_tests() {
    local exit_status
    set +e
    pytest $CATEGORICAL_SYLLOGISMS/tests/api
    exit_status=$?
    set -e
    which docker 2>/dev/null && $BIN/clean.sh docker || true
    (( exit_status == 0 )) || echoexit "API tests failed"
}

lint_test_passed() {
    local SUMMARY_LINE=$1
    local SCORE_STR=$(echo $SUMMARY_LINE | cut -d' ' -f7)
    python -c "assert $SCORE_STR == 1" &> /dev/null
}

run_lint_tests() {
    which pylint &> /dev/null || echoexit "Running lint tests requires pylint"
    local -r LINT_RULES="$CATEGORICAL_SYLLOGISMS/tests/lint_rules"
    local test_target="$1"
    [[ "$test_target" ]] || test_target='all'
    if [[ "$test_target" == 'all' ]]; then
        local logic_result="$("$BIN/test.sh" lint logic 2>&1 || true &)"
        local tests_dir_result="$("$BIN/test.sh" lint tests 2>&1 || true &)"
        wait
        local tests_dir_summary="$(echo "$tests_dir_result" | grep 'Your code has been rated at')"

        echo "$logic_result"
        echo "$tests_dir_result"
        echo ""
        echo "==============================================================================="
        echo "Summary"
        echo "==============================================================================="
        local logic_summary="$(echo "$logic_result" | grep 'Your code has been rated at')"
        echo "Main code base: $logic_summary"
        echo "Unit tests: $tests_dir_summary"

        lint_test_passed "$logic_summary"
        lint_test_passed "$tests_dir_summary"

    elif [[ "$test_target" == 'tests' ]]; then
        PYTHONPATH=$PYTHONPATH:$CATEGORICAL_SYLLOGISMS/tests/unit \
            pylint \
                --rcfile="$LINT_RULES/pylintrc_test" \
                $CATEGORICAL_SYLLOGISMS/tests/unit/* \
                $CATEGORICAL_SYLLOGISMS/tests/api/* \
                $CATEGORICAL_SYLLOGISMS/tests/test_lib/* \
                2>&1
    elif [[ "$test_target" == 'logic' ]]; then
        pylint \
            --rcfile="$LINT_RULES/pylintrc" \
            $CATEGORICAL_SYLLOGISMS/logic/* \
            2>&1
    else
        echoexit "Unrecognized lint test target '$test_target'"
    fi
}

run_topic_tests() {
    local exit_status
    local test_cmd='pytest'
    test_cmd+=" --verbose"
    test_cmd+=" $CATEGORICAL_SYLLOGISMS/tests/topic/"
    test_cmd+=" 2>&1"
    set +e
    eval "$test_cmd"
    exit_status=$?
    set -e
    $BIN/clean.sh pyc_files
    (( exit_status == 0 )) || echoexit "Topic validation failed"
}

run_docker_tests() {
    local test_images="$@"
    local test_runner="$CATEGORICAL_SYLLOGISMS/tests/docker/run.sh"
    local test_image
    if [[ -z "$test_images" ]] || [[ "$test_images" == 'all' ]]; then
        test_images="$ALL_IMAGES"
    fi
    for test_image in $test_images; do
        $test_runner $test_image
    done
}

_npm_run() {
    local test_command="$1"
    local exit_status
    cd $CATEGORICAL_SYLLOGISMS/ui
    set +e
    npm run $test_command
    exit_status=$?
    set -e
    cd - &> /dev/null
    (( exit_status == 0 )) || echoexit "UI test failed: $test_command"

}

run_ui_lint_tests() {
    _npm_run lint
}

run_ui_unit_tests() {
    _npm_run test
}

declare opt
while getopts ':-hv' opt; do
    case $opt in
        h|-help)
            print_help
            exit
            ;;
        v|-verbose)
            set -x
            ;;
        \?)
            echo "Invalid option -$OPTARG" 1>&2
            print_help
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument"
            print_help
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

[[ "$@" ]] || { print_help; exit 1; }

declare test_type="$1"
shift

case $test_type in
    unit|unit_tests) run_unit_tests $@ ;;
    lint|lint_tests) run_lint_tests $@ ;;
    topic|topic_tests) run_topic_tests $@ ;;
    api|api_tests) run_api_tests $@ ;;
    docker|docker_tests) run_docker_tests $@ ;;
    ui_lint|ui_lint_tests) run_ui_lint_tests $@ ;;
    ui_unit|ui_unit_tests) run_ui_unit_tests $@ ;;
    *) echoexit "Unrecognized test type '$test_type'" ;;
esac
